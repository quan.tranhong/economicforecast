package it.thoson.flutter_base.application

class NetworkEvent {
    var action = Action.UNKNOWN

    enum class Action {
        UNKNOWN,
        NETWORK_CONNECTED,
        NETWORK_DISCONNECTED
    }
}