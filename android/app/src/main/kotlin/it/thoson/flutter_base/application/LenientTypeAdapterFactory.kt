package it.thoson.flutter_base.application

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.TypeAdapterFactory
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.io.IOException

class LenientTypeAdapterFactory : TypeAdapterFactory {

    override fun <T> create(gson: Gson, type: TypeToken<T>): TypeAdapter<T> {

        val delegate = gson.getDelegateAdapter(this, type)

        return object : TypeAdapter<T>() {

            @Throws(IOException::class)
            override fun write(output: JsonWriter, value: T) {
                delegate.write(output, value)
            }

            @Throws(IOException::class)
            override fun read(input: JsonReader): T? {
                return try { //Here is the magic
                    //Try to read value using default TypeAdapter
                    delegate.read(input)
                } catch (e: Exception) {
                    //Utils.recordLogs("read", "fail to read input on lenient type")
                    //Timber.e("*** PARSE JSON ERROR *** - $input in $type")
                    //If we can't in case when we expecting to have an object but array is received (or some other unexpected stuff), we just skip this value in reader and return null
                    input.skipValue()
                    null
                }

            }
        }
    }
}