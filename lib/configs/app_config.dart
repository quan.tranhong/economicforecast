import 'package:flutter/services.dart';

class AppConfig {
  static const String appId = 'it.quanth';
  static const String appName = 'Ecomonic Forecast';
  static const String version = '1.0.0';

  ///Network
  static const baseUrl = "https://aic-group.bike/api/v1/dong-nai";//"http://18.140.50.86/api/v1/dong-nai";

  ///Paging
  static const pageSize = 40;
  static const pageSizeMax = 1000;
}

class FirebaseConfig {
  //Todo
}

class DatabaseConfig {
  //Todo
  static const int version = 1;
}

class MovieAPIConfig {
  static const String APIKey = '26763d7bf2e94098192e629eb975dab0';
}

class FlutterBridge {
  static String CHANNEL = "com.aic.sso";
  static String KEY_CHECK_TOKEN = "sso_token";
  static String KEY_OPEN_SSO_CENTER = "open_sso_center";
  static String KEY_LOGOUT = "logout";

  static var platform = MethodChannel(CHANNEL);

  static Future<Null> checkToken() async {
    await FlutterBridge.platform.invokeMethod(KEY_CHECK_TOKEN);
  }

  static Future<Null> openSSOCenter() async {
    await FlutterBridge.platform.invokeMethod(KEY_OPEN_SSO_CENTER);
  }

  static Future<Null> logout() async {
    await FlutterBridge.platform.invokeMethod(KEY_LOGOUT);
  }
}
