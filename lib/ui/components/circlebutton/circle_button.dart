import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';

class CircleButton extends StatelessWidget {
  Function onTapListenter;
  Widget icon;
  ForecastDurationType? forecastType;

  CircleButton({
    required this.onTapListenter,
    required this.icon,
    this.forecastType,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTapListenter();
      },
      child: Container(
        width: 30.0,
        height: 30.0,
        decoration: new BoxDecoration(
            shape: BoxShape.circle,
            // You can use like this way or like the below line
            //borderRadius: new BorderRadius.circular(30.0),
            color: Colors.white,
            boxShadow: AppShadow.boxDarkShadow),
        padding: EdgeInsets.all(5),
        child: Stack(
          children: [
            Visibility(
              visible: forecastType != null,
              child: Center(
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  margin: EdgeInsets.only(top: 5),
                  child: Center(
                      child: Text(
                    forecastType?.number ?? "",
                    style: TextStyle(fontSize: 8, fontWeight: FontWeight.bold),
                  )),
                ),
              ),
            ),
            SizedBox.fromSize(
              child: FittedBox(
                child: icon,
              ),
            ),
          ],
        ), // You can add a Icon instead of text also, like below.
      ),
    );
  }
}
