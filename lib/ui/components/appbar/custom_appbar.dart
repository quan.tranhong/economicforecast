import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';

// ignore: must_be_immutable
class CustomAppbar extends StatelessWidget {
  String title;
  Function onMenuCLickListener;

  CustomAppbar({required this.title, required this.onMenuCLickListener})
      /*: assert(title != null && onPressMenuListener != null)*/ {
    this.title = title;
  }

  double marginTopToHeader = 10;
  double marginHorizontalSearch = 10;
  double marginHaftHorizontalSearch = 5;
  double headerHeight = 50;
  double circleAvatarSize = 35;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppDimens.appBarHeight,
      decoration: BoxDecoration(
          color: Colors.transparent
      ),
      padding: EdgeInsets.symmetric(horizontal: AppDimens.statusMarginHorizontal),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              onMenuCLickListener();
            },
            child: SizedBox.fromSize(
              size: Size.fromRadius(AppDimens.statusBarHeight - 5),
              child: FittedBox(
                child: Icon(
                  Icons.menu,
                  color: AppColors.backgroundBlueLight,
                ),
              ),
            ),
          ),
          Expanded(
            child: Text(
              title,
              style: TextStyle(fontSize: 17, color: AppColors.backgroundBlueLight, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            width: AppDimens.statusBarHeight,
            height: AppDimens.statusBarHeight,
          )
        ],
      ),
    );
  }
}
