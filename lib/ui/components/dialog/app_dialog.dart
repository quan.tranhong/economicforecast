import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_images.dart';

enum DialogType { INFO, SUCCESS, ERROR }

class AppDialog {
  final BuildContext context;
  final DialogType? type;
  final String? title;
  final String? description;
  final String? okText;
  final VoidCallback? onOkPressed;
  final String? cancelText;
  final VoidCallback? onCancelPressed;
  final bool? dismissible;
  final VoidCallback? onDismissed;
  final bool? autoDismiss;
  final VoidCallback? overrideDismiss;

  AppDialog( {
    required this.context,
    this.type = DialogType.INFO,
    this.title,
    this.description,
    this.okText = '',
    this.onOkPressed,
    this.cancelText = '',
    this.onCancelPressed,
    this.onDismissed,
    this.dismissible = false,
    this.autoDismiss = false,
    this.overrideDismiss,
  }) : assert(
          context != null,
        );

  void show() {
    //Auto dismiss after 2 seconds
    if (autoDismiss ?? false) {
      Future.delayed(Duration(seconds: 2), () {
        dismiss();
      });
    }
    showDialog(
        useRootNavigator: true,
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return buildDialog;
        });
  }

  Widget get buildDialog {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: WillPopScope(
        onWillPop: _onWillPop,
        child: GestureDetector(
          onTap: () {
            if (dismissible == true) {
              dismiss();
            }
          },
          child: Container(
            color: Colors.transparent,
            height: double.infinity,
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  padding: EdgeInsets.only(
                    top: 10,
                    left: 5,
                    right: 5,
                  ),
                  child: Column(
                    children: [
                      _buildHeaderIcon,
                      _buildTitleText,
                      _buildDescriptionText,
                      _buildActions,
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget get _buildHeaderIcon {
    switch (type) {
      case DialogType.INFO:
        return Container();
      case DialogType.SUCCESS:
        return Container(
          margin: EdgeInsets.only(top: 10),
          child: Image.asset(AppImages.icSuccess),
        );
      case DialogType.ERROR:
        return Container(
          margin: EdgeInsets.only(top: 10),
          child: Image.asset(AppImages.icErrorNetwork),
        );
    }
    return Container();
  }

  Widget get _buildTitleText {
    if ((title ?? '').isEmpty) return Container();
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Text(
        title ?? '',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Color(0xFF005CF7)),
      ),
    );
  }

  Widget get _buildDescriptionText {
    if ((description ?? '').isEmpty) return Container();
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Text(
        description ?? '',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: Colors.black),
      ),
    );
  }

  Widget get _buildActions {
    bool showOkButton = (okText ?? '').isNotEmpty;
    bool showCancelButton = (cancelText ?? '').isNotEmpty;

    return Container(
      child: Row(
        children: [
          showOkButton
              ? Expanded(
                  child: Container(
                  child: _buildOkButton,
                ))

              /// check xem nút cancel có còn dùng ko? nếu ko dùng thì padding button 1 đoạn
              : showCancelButton
                  ? Container()
                  : Container(
                      height: 15,
                    ),
          showCancelButton
              ? Expanded(
                  child: Container(
                  child: _buildCancelButton,
                ))

              /// check xem nút ok có còn dùng ko? nếu ko dùng thì padding button 1 đoạn
              : showOkButton
                  ? Container()
                  : Container(
                      height: 15,
                    ),
        ],
      ),
    );
  }

  Widget get _buildOkButton {
    return FlatButton(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onPressed: () {
        dismiss();
        onOkPressed?.call();
      },
      child: Text(
        okText ?? "ok",
        style: TextStyle(
          fontSize: 14,
          color: Color(0xFF005CF7),
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget get _buildCancelButton => FlatButton(
        onPressed: () {
          dismiss();
          onCancelPressed?.call();
        },
        child: Text(
          cancelText ?? 'Cancel',
          style: TextStyle(
            fontSize: 14,
            color: Color(0xFF005CF7),
            fontWeight: FontWeight.bold,
          ),
        ),
        color: Colors.transparent,
      );

  dismiss() {
    if(overrideDismiss == null) {
      Navigator.of(context).pop();
    } else {
      overrideDismiss!();
    }
    onDismissed?.call();
  }

  Future<bool> _onWillPop() async {
    return (dismissible == null);
  }
}
