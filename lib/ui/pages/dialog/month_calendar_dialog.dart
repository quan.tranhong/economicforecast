import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';

import 'item/month_calendar_item.dart';

class MonthCalendarDialog {
  final BuildContext context;
  List<GlobalKey<MonthCalendarItemState>> itemGlobalKeys = [];
  ForecastDurationType selectedType;
  Function itemSelected;
  bool canShowAll;

  MonthCalendarDialog({
    required this.context,
    required this.selectedType,
    required this.itemSelected,
    required this.canShowAll,
  }) : assert(
          context != null,
        );

  void show() {
    showDialog(
        useRootNavigator: true,
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return _buildDialog;
        });
  }

  Widget get _buildDialog {
    GlobalKey<MonthCalendarItemState> keySixMonth = GlobalKey();
    GlobalKey<MonthCalendarItemState> keyAYear = GlobalKey();
    GlobalKey<MonthCalendarItemState> keyAllYear = GlobalKey();

    itemGlobalKeys.add(keySixMonth);
    itemGlobalKeys.add(keyAYear);
    itemGlobalKeys.add(keyAllYear);

    List<Widget> items = [];
    items.add(
      MonthCalendarItem(
          key: itemGlobalKeys[0],
          type: ForecastDurationType.SIX_MONTH,
          isSelected: selectedType == ForecastDurationType.SIX_MONTH ||
              selectedType == ForecastDurationType.NINE_MONTH,
          selectItemListener: _selectItemListener),
    );

    items.add(
      MonthCalendarItem(
          key: itemGlobalKeys[1],
          type: ForecastDurationType.A_YEAR,
          isSelected: selectedType == ForecastDurationType.A_YEAR ||
              selectedType == ForecastDurationType.OVER_A_YEAR,
          selectItemListener: _selectItemListener),
    );

    if(canShowAll)
      items.add(
        MonthCalendarItem(
            key: itemGlobalKeys[2],
            type: ForecastDurationType.ALL_YEAR,
            isSelected: selectedType == ForecastDurationType.ALL_YEAR ||
                selectedType == ForecastDurationType.OVER_ALL_YEAR,
            selectItemListener: _selectItemListener),
      );

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        color: Colors.transparent,
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: ScreenSize.of(context).width * 0.8,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    height: 50,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                            vertical: 10,
                            horizontal: 5,
                          ),
                          child: Text(
                            "Lựa chọn kiểu hiển thị",
                            style: AppTextStyle.blueDarkS15Bold,
                          ),
                        ),
                        Spacer(),
                        CircleButton(
                          icon: Icon(
                            Icons.close,
                            color: AppColors.backgroundBlueDark,
                          ),
                          onTapListenter: _closeClickedListener,
                        ),
                      ],
                    ),
                  ),
                  Container(height: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: items,
                  ),
                  Container(height: 20),
                  GestureDetector(
                    onTap: () {
                      itemSelected(type: selectedType);
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.backgroundBlueDark,
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        boxShadow: AppShadow.boxShadow,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Center(
                        child: Text(
                          "Áp dụng",
                          style: AppTextStyle.whiteS16Bold,
                        ),
                      ),
                    ),
                  ),
                  Container(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _selectItemListener(
      {required ForecastDurationType type, required bool isSelected}) {
    if (type == ForecastDurationType.SIX_MONTH ||
        type == ForecastDurationType.NINE_MONTH) {
      itemGlobalKeys[1].currentState?.updateChoiceMatter(type);
      itemGlobalKeys[2].currentState?.updateChoiceMatter(type);
    } else if (type == ForecastDurationType.A_YEAR ||
        type == ForecastDurationType.OVER_A_YEAR) {
      itemGlobalKeys[0].currentState?.updateChoiceMatter(type);
      itemGlobalKeys[2].currentState?.updateChoiceMatter(type);
    } else {
      itemGlobalKeys[0].currentState?.updateChoiceMatter(type);
      itemGlobalKeys[1].currentState?.updateChoiceMatter(type);
    }
    selectedType = type;
  }

  void _closeClickedListener() {
    Navigator.of(context).pop();
  }
}
