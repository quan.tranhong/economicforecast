import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';

class MonthCalendarItem extends StatefulWidget {
  ForecastDurationType? type;
  bool? isSelected;
  Function selectItemListener;

  MonthCalendarItem(
      {Key? key, this.type, required this.selectItemListener, this.isSelected})
      : super(key: key);

  @override
  MonthCalendarItemState createState() => MonthCalendarItemState();
}

class MonthCalendarItemState extends State<MonthCalendarItem> {
  late bool _isSelected;

  @override
  void initState() {
    super.initState();
    _isSelected = widget.isSelected ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (!_isSelected) {
            _isSelected = true;
            widget.selectItemListener(
                type: widget.type, isSelected: _isSelected);
          }
        });
      },
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: !_isSelected ? Colors.white : AppColors.greenForecastHex,
            borderRadius: BorderRadius.circular(12),
            boxShadow: AppShadow.boxLightShadow),
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.symmetric(vertical: 5),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: 25,
              height: 25,
              // decoration: BoxDecoration(
              //   color: Colors.white,
              //   borderRadius: BorderRadius.circular(4),
              // ),
              child: SizedBox.fromSize(
                child: FittedBox(
                    child: Image.asset(
                      widget.type == ForecastDurationType.SIX_MONTH ||
                        widget.type == ForecastDurationType.A_YEAR
                        ? AppImages.ic6Month
                        : AppImages.icAllMonth,)
                    // Image.asset(
                    //     widget.type == ForecastDurationType.SIX_MONTH
                    //         ? AppImages.icSixMonth
                    //         : AppImages.icAYear),
                    ),
              ), // You can add a Icon instead of text also, like below.
            ),
            Container(height: 10),
            Container(
              padding: EdgeInsets.all(5),
              child: Center(
                child: Text(
                  widget.type?.name ?? "",
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: AppColors.backgroundBlueDark),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void updateChoiceMatter(ForecastDurationType type) {
    setState(() {
      if (type != widget.type) {
        _isSelected = false;
      } else {
        _isSelected = true;
      }
    });
  }
}
