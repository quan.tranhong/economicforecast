part of 'index_cubit.dart';

@immutable
class IndexState extends Equatable {
  final bool? select;

  IndexState({this.select});

  IndexState copyWith({
    bool? select,
  }) {
    return IndexState(
      select: select ?? this.select,
    );
  }

  @override
  List<Object?> get props => [select];
}
