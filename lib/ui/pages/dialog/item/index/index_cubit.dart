import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'index_state.dart';

class IndexCubit extends Cubit<IndexState> {
  IndexCubit() : super(IndexState());

  /// quanth: statistical
  void update({bool? select}) async {
    emit(state.copyWith(
      select: select,
    ));
  }
}
