part of 'select_index_cubit.dart';

@immutable
class SelectIndexState extends Equatable {
  ChartPresenter? presenter;
  LoadStatus? presenterStatus;

  SelectIndexState({this.presenter, this.presenterStatus});

  SelectIndexState copyWith({
    ChartPresenter? presenter,
    LoadStatus? presenterStatus,
  }) {
    return SelectIndexState(
      presenter: presenter ?? this.presenter,
      presenterStatus: presenterStatus ?? this.presenterStatus,
    );
  }

  @override
  List<Object?> get props => [
    this.presenter,
    this.presenterStatus,
  ];

}
