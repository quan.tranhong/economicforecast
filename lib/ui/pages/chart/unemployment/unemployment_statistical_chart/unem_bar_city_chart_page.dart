import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/unem_type.dart';
import 'package:flutter_base/ui/pages/home/unemployment/unemployment_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';

class UnemBarCityChartPage extends StatefulWidget {
  UnemType type;
  Function onTouchDotListener;

  UnemBarCityChartPage({
    required this.type,
    required this.onTouchDotListener,
  });

  @override
  _UnemBarCityChartPageState createState() => _UnemBarCityChartPageState();
}

class _UnemBarCityChartPageState extends State<UnemBarCityChartPage> {
  late UnemploymentCubit _cubit;
  final Color allColor = AppColors.unemRedDark;
  final Color femaleColor = AppColors.unemGreenDark;
  final Color maleColor = AppColors.unemBlueDark;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<UnemploymentCubit>(context);
    _cubit.initBarChartPresenter(
        chartGroupPresenter: _cubit.state.barCityFilterParam?.chartPresenter);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<UnemploymentCubit, UnemploymentState>(
                  buildWhen: (prev, current) =>
                      prev.barCityFilterStatus != current.barCityFilterStatus ||
                      prev.barCityType != current.barCityType,
                  builder: (context, state) {
                    if (state.barCityFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.barCityFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return BarChart(
                      getBarChartData(
                          maxYvalue: _cubit.state.barCityType?.statValue ??
                              ForecastDurationType.SIX_MONTH.statValue),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  BarChartData getBarChartData({double? maxYvalue}) {
    return BarChartData(
      alignment: BarChartAlignment.center,
      barTouchData: BarTouchData(
        enabled: true,
        touchCallback: (BarTouchResponse touchResponse) {
          if (touchResponse != null) {
            BarTouchedSpot? spot = touchResponse.spot;
            widget.onTouchDotListener(barTouchedSpot: spot);
          }
        },
        touchTooltipData: BarTouchTooltipData(
          tooltipBgColor: Colors.transparent,
          tooltipMargin: -10,
          getTooltipItem: (
            BarChartGroupData group,
            int groupIndex,
            BarChartRodData rod,
            int rodIndex,
          ) {
            Color color = Colors.transparent;
            return BarTooltipItem(
              "${rod.y}",
              TextStyle(
                  fontSize: 12, color: color, fontWeight: FontWeight.bold),
            );
          },
        ),
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          rotateAngle: 60,
          margin: 10,
          getTitles: (double value) {
            double a = _cubit.state.cityTimeLines?.length.toDouble() ?? 0;
            double? length = (a >= (maxYvalue ?? 0))
                ? maxYvalue
                : a;
            if (value >= 0 && value < (length ?? 0).toInt())
              return _cubit.state.cityTimeLines![(value).toInt()].toString();
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
              color: AppColors.backgroundBlueDark,
              fontWeight: FontWeight.normal,
              fontSize: 10),
          margin: 5,
        ),
      ),
      gridData: FlGridData(
        show: true,
        checkToShowHorizontalLine: (value) => value % 20 == 0,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: (value == 0) ? AppColors.backgroundBlueDark : AppColors.gray,
            strokeWidth: (value == 0) ? 1 : 0.5,
          );
        },
      ),
      borderData: FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(
            color: AppColors.gray,
          ),
          left: BorderSide(
            color: AppColors.gray,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      barGroups: getBarGroups(maxYvalue: maxYvalue),
    );
  }

  List<BarChartGroupData> getBarGroups({double? maxYvalue}) {
    List<BarChartGroupData> results = [];

    ChartGroupPresenter? chartGroupPresenter =
        _cubit.state.barCityFilterParam?.chartPresenter;
    ChartPresenter? all = chartGroupPresenter?.presenters?[0];
    ChartPresenter? female = chartGroupPresenter?.presenters?[1];
    ChartPresenter? male = chartGroupPresenter?.presenters?[2];

    if (all != null && female != null && male != null) {
      List<double>? allValues = _createRealValue(all.value ?? []);
      List<double>? femaleValues = _createRealValue(female.value ?? []);
      List<double>? maleValues = _createRealValue(male.value ?? []);
      for (int i = 0; i < allValues!.length; i++) {
        double a = allValues[i];
        double b = (allValues[i]) + (maleValues?[i] ?? 0);
        double c =
            (allValues[i]) + (femaleValues?[i] ?? 0) + (maleValues?[i] ?? 0);

        BarChartGroupData barChartGroupData =  BarChartGroupData(
          x: i,
          barsSpace: 1,
          barRods: [
            BarChartRodData(
                width: 17,
                y: ((allValues[i]) +
                    (femaleValues?[i] ?? 0) +
                    (maleValues?[i] ?? 0)),
                rodStackItems: [
                  BarChartRodStackItem(0, a, allColor),
                  BarChartRodStackItem(a, b, femaleColor),
                  BarChartRodStackItem(b, c, maleColor),
                ],
                borderRadius: const BorderRadius.all(Radius.zero)),
          ],
        );
        results.add(barChartGroupData);
      }
    }
    return results;
  }

  double getRealLength() {
    ChartGroupPresenter? chartGroupPresenter =
        _cubit.state.barCityFilterParam!.chartPresenter;
    List<ChartPresenter> chartPresenters =
        chartGroupPresenter?.presenters ?? [];
    List<double>? list = chartPresenters[0].value;
    double length = (_cubit.state.barCityType?.statValue ??
        ForecastDurationType.SIX_MONTH.statValue);
    double currLength = list?.length.toDouble() ?? 0;
    if (length > currLength) length = currLength;
    return length;
  }

  List<double>? _createRealValue(List<double>? oldValue) {
    List<double>? realValues = [];
    realValues.addAll(oldValue ?? []);
    if (realValues.isNotEmpty) {
      switch (_cubit.state.barCityType) {
        case ForecastDurationType.SIX_MONTH:
          return realValues.sublist(
              realValues.length <= 6
                  ? 0
                  : (realValues.length - 6),
              realValues.length);
        case ForecastDurationType.A_YEAR:
          return realValues.sublist(
              realValues.length <= 12
                  ? 0
                  : (realValues.length - 12),
              realValues.length);
        default:
          return realValues.sublist(
              realValues.length <= 6
                  ? 0
                  : (realValues.length - 6),
              realValues.length);
      }
    }
    return realValues;
  }
}
