import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/chart_group_reg_presenter.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/unem_type.dart';
import 'package:flutter_base/ui/pages/home/iip/iip_cubit.dart';
import 'package:flutter_base/ui/pages/home/unemployment/unemployment_cubit.dart';
import 'package:flutter_base/ui/pages/home/xnk/xnk_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UnemLineCityChartPage extends StatefulWidget {
  UnemDataEntity? unem;
  Function onTouchDotListener;
  UnemType type;

  UnemLineCityChartPage(
      {this.unem, required this.type, required this.onTouchDotListener});

  @override
  _UnemLineCityChartPageState createState() => _UnemLineCityChartPageState();
}

class _UnemLineCityChartPageState extends State<UnemLineCityChartPage> {
  late UnemploymentCubit _cubit;
  List<int> _timeline = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<UnemploymentCubit>(context);
    createChartPresents(unem: widget.unem);
    _cubit.initLineCityPresenter(
      chartGroupPresenter: _cubit.state.lineCityFilterParam?.chartPresenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<UnemploymentCubit, UnemploymentState>(
                  buildWhen: (prev, current) =>
                      prev.lineCityFilterStatus !=
                          current.lineCityFilterStatus ||
                      prev.lineCityType != current.lineCityType,
                  builder: (context, state) {
                    if (state.lineCityFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.lineCityFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return LineChart(
                      createChartData(
                          maxYvalue: ForecastDurationType.SIX_MONTH.value),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  LineChartData createChartData({double? maxYvalue}) {
    double length = getRealLength();
    List<LineChartBarData> lineBarsDatas = linesBarData();
    return LineChartData(
      lineTouchData: LineTouchData(
        enabled: true,
        touchCallback: (LineTouchResponse? touchResponse) {
          if (touchResponse != null) {
            List<LineBarSpot>? lineBarSpots = touchResponse.lineBarSpots;
            widget.onTouchDotListener(lineBarSpots: lineBarSpots);
          }
        },
        getTouchedSpotIndicator:
            (LineChartBarData barData, List<int> spotIndexes) {
          return spotIndexes.map((index) {
            return TouchedSpotIndicatorData(
              FlLine(
                color: Colors.pink,
              ),
              FlDotData(
                show: true,
                getDotPainter: (spot, percent, barData, index) =>
                    FlDotCirclePainter(
                  radius: 8,
                  color: Colors.red,
                  strokeWidth: 2,
                  strokeColor: Colors.black,
                ),
              ),
            );
          }).toList();
        },
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.pink,
          tooltipRoundedRadius: 8,
          getTooltipItems: (List<LineBarSpot> lineBarsSpot) {
            return lineBarsSpot.map((lineBarSpot) {
              return null;
            }).toList();
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
      ),
      titlesData: FlTitlesData(
        /// quanth: danh sách giá trị trục hoành
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          rotateAngle: 60,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          margin: 10,
          getTitles: (value) {
            double length = getRealLength();
            if (value > 0 && value < length )
              return _timeline[(value - 1).toInt()].toString();
            return '';
          },
        ),

        /// quanth: danh sách giá trị trục tung
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          getTitles: (value) {
            return '${value.toInt()}';
          },
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: AppColors.gray,
          ),
          left: BorderSide(
            color: AppColors.gray,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: maxYvalue,
      lineBarsData: lineBarsDatas,
    );
  }

  List<LineChartBarData> linesBarData() {
    if (_cubit.state.lineCityFilterParam!.chartPresenter == null) {
      return [];
    }
    List<LineChartBarData> result = [];
    ChartGroupPresenter? chartGroupPresenter =
        _cubit.state.lineCityFilterParam!.chartPresenter;

    List<ChartPresenter> chartPresenters =
        chartGroupPresenter?.presenters ?? [];

    result.add(
      createLineChartData(list: chartPresenters[0].value, colors: [
        AppColors.unemBlueDark,
      ]),
    );
    result.add(
      createLineChartData(list: chartPresenters[1].value, colors: [
        AppColors.unemGreenDark,
      ]),
    );
    result.add(
      createLineChartData(list: chartPresenters[2].value, colors: [
        AppColors.unemRedDark,
      ]),
    );

    return result;
  }

  LineChartBarData createLineChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];
    double length = getRealLength();
    for (int i = 0; i < length - 1; i++) {
      flSpots.add(FlSpot(i + 1, list![i]));
    }
    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
    );
  }

  /// Lerps between a [LinearGradient] colors, based on [t]
  Color? lerpGradient(List<Color> colors, List<double> stops, double t) {
    if (stops.length != colors.length) {
      stops = [];

      /// provided gradientColorStops is invalid and we calculate it here
      colors.asMap().forEach((index, color) {
        final percent = 1.0 / colors.length;
        stops.add(percent * index);
      });
    }

    for (var s = 0; s < stops.length - 1; s++) {
      final leftStop = stops[s], rightStop = stops[s + 1];
      final leftColor = colors[s], rightColor = colors[s + 1];
      if (t <= leftStop) {
        return leftColor;
      } else if (t < rightStop) {
        final sectionT = (t - leftStop) / (rightStop - leftStop);
        return Color.lerp(leftColor, rightColor, sectionT);
      }
    }
    return colors.last;
  }

  void createChartPresents({UnemDataEntity? unem}) {
    _timeline = [];
    _timeline.addAll(unem?.timeline ?? []);
  }

  double getRealLength(){
    ChartGroupPresenter? chartGroupPresenter =
        _cubit.state.lineCityFilterParam!.chartPresenter;
    List<ChartPresenter> chartPresenters =
        chartGroupPresenter?.presenters ?? [];
    List<double>? list = chartPresenters[0].value;
    double length = (_cubit.state.lineCityType?.value ??
        ForecastDurationType.SIX_MONTH.value);
    double currLength = list?.length.toDouble() ?? 0;
    if (length > currLength) length = currLength;
    return length;
  }
}
