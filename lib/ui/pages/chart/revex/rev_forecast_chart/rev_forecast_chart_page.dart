import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_base/ui/pages/home/revenue/revenue_cubit.dart';
import 'package:flutter_base/utils/extensions.dart';
import 'package:flutter_base/utils/utils.dart';

class RevForecastChartPage extends StatefulWidget {
  Function onTouchDotListener;
  List<String>? timeline;
  List<ChartPresenter>? chartPresenters;
  ForecastDurationType? forecastType;

  RevForecastChartPage(
      {required this.onTouchDotListener,
      this.timeline,
      this.chartPresenters,
      this.forecastType});

  @override
  _RevForecastChartPageState createState() => _RevForecastChartPageState();
}

class _RevForecastChartPageState extends State<RevForecastChartPage> {
  late RevCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<RevCubit>(context);
    _cubit.initForecastChartPresenter(
        chartPresenter: _cubit.state.forecastFilterParams!.presenterList![0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                  child: BlocBuilder<RevCubit, RevState>(
                      buildWhen: (prev, current) =>
                          prev.forecastFilterStatus !=
                              current.forecastFilterStatus ||
                          prev.forecastType != current.forecastType,
                      builder: (context, state) {
                        if (state.forecastFilterStatus == LoadStatus.LOADING) {
                          return Container();
                        } else if (state.forecastFilterStatus ==
                            LoadStatus.FAILURE) {
                          return Container();
                        }
                        double? maxYvalue = widget.forecastType?.value ??
                            ForecastDurationType.SIX_MONTH.value;
                        if (widget.forecastType ==
                            ForecastDurationType.ALL_YEAR ||
                            widget.forecastType ==
                                ForecastDurationType.OVER_ALL_YEAR)
                          maxYvalue = Utils.checkDouble(
                              state.forecastTimeLines!.length + 1);
                        return LineChart(
                          createChartData(
                              maxYvalue: maxYvalue),
                          swapAnimationDuration:
                              const Duration(milliseconds: 250),
                        );
                      }),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ],
      ),
    );
  }

  LineChartData createChartData({double? maxYvalue}) {
    List<LineChartBarData> lineBarsDatas = linesBarData(maxYValue: maxYvalue!);
    return LineChartData(
      lineTouchData: LineTouchData(
        enabled: true,
        touchCallback: (LineTouchResponse touchResponse) {
          if (touchResponse != null) {
            List<LineBarSpot>? lineBarSpots = touchResponse.lineBarSpots;
            widget.onTouchDotListener(lineBarSpots: lineBarSpots);
          }
        },
        getTouchedSpotIndicator:
            (LineChartBarData barData, List<int> spotIndexes) {
          return spotIndexes.map((index) {
            return TouchedSpotIndicatorData(
              FlLine(
                color: Colors.pink,
              ),
              FlDotData(
                show: true,
                getDotPainter: (spot, percent, barData, index) =>
                    FlDotCirclePainter(
                  radius: 8,
                  color: Colors.red,
                  strokeWidth: 2,
                  strokeColor: Colors.black,
                ),
              ),
            );
          }).toList();
        },
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.pink,
          tooltipRoundedRadius: 8,
          getTooltipItems: (List<LineBarSpot> lineBarsSpot) {
            return lineBarsSpot.map((lineBarSpot) {
              return null;
            }).toList();
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
      ),
      titlesData: FlTitlesData(
        /// quanth: danh sách giá trị trục hoành
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          rotateAngle: 60,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          margin: 10,
          interval: !Utils.isEnableAllModeExceptOver(_cubit.state.forecastType) ? 1 : Utils.createDivRange(
              _cubit.state.forecastTimeLines?.length ?? 0),
          getTitles: (value) {
            if (value > 0 && (widget.timeline?.length ?? 0) > (value -1))
              return widget.timeline![(value-1).toInt()];
            return '';
          },
        ),

        /// quanth: danh sách giá trị trục tung
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 8,
          ),
          getTitles: (value) {
            return '${value.toStringAsFixed(2)}';
          },
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: AppColors.gray,
          ),
          left: BorderSide(
            color: AppColors.gray,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: maxYvalue,
      lineBarsData: lineBarsDatas,
      betweenBarsData: isEnableForecast()
          ? getBetweenBarData(lineBarsDatas.length)
          : [],
    );
  }

  List<BetweenBarsData> getBetweenBarData(int length){
    BetweenBarsData area1 = BetweenBarsData(
      fromIndex: 1,
      toIndex: 2,
      colors: [AppColors.greenForecast],
    );
    return [area1];
  }

  List<LineChartBarData> linesBarData({double? maxYValue}) {
    if (_cubit.state.forecastFilterParams!.presenterList!.isEmpty) {
      return [];
    }
    List<LineChartBarData> result = [];
    for (int i = 0;
    i < _cubit.state.forecastFilterParams!.presenterList!.length;
    i++) {
      if (_cubit.state.forecastType == ForecastDurationType.NINE_MONTH &&
          maxYValue! > ForecastDurationType.SIX_MONTH.value) {
        result = createLCDWithCond(
            chartPresenter:
            _cubit.state.forecastFilterParams!.presenterList![i],
            type: ForecastDurationType.NINE_MONTH);
      } else if (_cubit.state.forecastType ==
          ForecastDurationType.OVER_A_YEAR &&
          maxYValue! > ForecastDurationType.A_YEAR.value) {
        result = createLCDWithCond(
            chartPresenter:
            _cubit.state.forecastFilterParams!.presenterList![i],
            type: ForecastDurationType.OVER_A_YEAR);
      } else if (_cubit.state.forecastType ==
          ForecastDurationType.OVER_ALL_YEAR &&
          maxYValue! > _cubit.state.forecastTimeLines!.length - 3) {
        result = createLCDWithCond(
            chartPresenter:
            _cubit.state.forecastFilterParams!.presenterList![i],
            type: ForecastDurationType.OVER_ALL_YEAR);
      } else {
        List<double>? realValues = _createRealValue(
            _cubit.state.forecastFilterParams!.presenterList![i].value ?? []);
        result.add(
          createLineChartData(list: realValues, colors: [
            AppColors.chartColors[
            _cubit.state.forecastFilterParams!.presenterList![i].key!]
          ]),
        );
      }
    }

    return result;
  }

  LineChartBarData createLineChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];

    /// quanth: ko dc de flspot empty
    ForecastDurationType type =
        _cubit.state.forecastType ?? ForecastDurationType.SIX_MONTH;

    if (type == ForecastDurationType.NINE_MONTH) {
      for (int i = 0; i < (ForecastDurationType.SIX_MONTH.statValue); i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else if (type == ForecastDurationType.OVER_A_YEAR) {
      for (int i = 0; i < (ForecastDurationType.A_YEAR.statValue); i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else if (type == ForecastDurationType.OVER_ALL_YEAR) {
      for (int i = 0; i < (list?.length ?? 3) - 3; i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else if (type == ForecastDurationType.ALL_YEAR) {
      for (int i = 0; i < (list?.length ?? 0); i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else {
      for (int i = 0; i < ((type.value) - 1); i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    }

    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(
          show: !Utils.isEnableAllModeExceptOver(_cubit.state.forecastType)),
      belowBarData: BarAreaData(show: false),
    );
  }

  LineChartBarData createDashLineChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];
    if (_cubit.state.forecastType == ForecastDurationType.NINE_MONTH) {
      for (int i = ForecastDurationType.SIX_MONTH.value.toInt() - 2;
      i < (ForecastDurationType.NINE_MONTH.value.toInt() - 1);
      i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else if (_cubit.state.forecastType == ForecastDurationType.OVER_A_YEAR) {
      for (int i = ForecastDurationType.A_YEAR.value.toInt() - 2;
      i < (ForecastDurationType.OVER_A_YEAR.value.toInt() - 1);
      i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else if (_cubit.state.forecastType ==
        ForecastDurationType.OVER_ALL_YEAR) {
      for (int i = (list?.length ?? 4) - 4; i < (list?.length ?? 0); i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    }

    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      dashArray: [2, 4],
      isStrokeCapRound: true,
      dotData:
      FlDotData(show: !Utils.isEnableAllModeExceptOver(_cubit.state.forecastType)),
      belowBarData: BarAreaData(show: false),
    );
  }

  LineChartBarData createDashLineNoDotChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];
    if (_cubit.state.forecastType == ForecastDurationType.NINE_MONTH) {
      for (int i = ForecastDurationType.SIX_MONTH.value.toInt() - 2;
      i < (ForecastDurationType.NINE_MONTH.value.toInt() - 1);
      i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else if (_cubit.state.forecastType == ForecastDurationType.OVER_A_YEAR) {
      for (int i = ForecastDurationType.A_YEAR.value.toInt() - 2;
      i < (ForecastDurationType.OVER_A_YEAR.value.toInt() - 1);
      i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    } else if (_cubit.state.forecastType ==
        ForecastDurationType.OVER_ALL_YEAR) {
      for (int i = (list?.length ?? 4) - 4; i < (list?.length ?? 0); i++) {
        flSpots.add(FlSpot(i + 1, list![i]));
      }
    }

    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      dashArray: [2, 4],
      isStrokeCapRound: true,
      dotData: FlDotData(show: false),
      belowBarData: BarAreaData(show: false),
    );
  }

  List<double>? _createRealValue(List<double>? oldValue) {
    List<double>? realValues = [];
    realValues.addAll(oldValue ?? []);
    int range = Utils.checkInt(_cubit.state.forecastType?.statValue ?? 0)!;
    if (realValues.isNotEmpty) {
      switch (_cubit.state.forecastType) {
        case ForecastDurationType.SIX_MONTH:
          realValues = realValues.sublist(
              (realValues.length - 3) - range, realValues.length - 3);
          break;
        case ForecastDurationType.A_YEAR:
          realValues = realValues.sublist(
              (realValues.length - 3) - range, realValues.length - 3);
          break;
        case ForecastDurationType.NINE_MONTH:
          realValues =
              realValues.sublist(realValues.length - range, realValues.length);
          break;
        case ForecastDurationType.OVER_A_YEAR:
          realValues =
              realValues.sublist(realValues.length - range, realValues.length);
          break;
        case ForecastDurationType.ALL_YEAR:
          realValues = realValues.sublist(0, realValues.length - 3);
          break;
        case ForecastDurationType.OVER_ALL_YEAR:
          realValues =
              realValues.sublist(realValues.length - range, realValues.length);
          break;
        default:
          realValues = realValues.sublist(6, 12);
      }
    }
    return realValues;
  }

  bool isEnableForecast() {
    return _cubit.state.forecastType == ForecastDurationType.NINE_MONTH ||
        _cubit.state.forecastType == ForecastDurationType.OVER_A_YEAR ||
        _cubit.state.forecastType == ForecastDurationType.OVER_ALL_YEAR;
  }

  List<LineChartBarData> createLCDWithCond({
    required ChartPresenter chartPresenter,
    required ForecastDurationType type,
  }) {
    /// quanth: khai báo kết quả
    List<LineChartBarData> result = [];

    /// quanth: khai báo danh sách cận trên, dưới, giữa
    List<double>? realValuesBot = [];
    List<double>? realValuesTop = [];
    List<double>? realValuesMid = [];

    /// quanth: khai báo bộ đếm của cận trên, dưới, giữa
    int botIndex = 0;
    int topIndex = 0;
    int midIndex = 0;

    List<double>? realValues = _createRealValue(chartPresenter.value ?? []);

    double startIndex = (type.statValue - 1) - 3;
    if(type == ForecastDurationType.OVER_ALL_YEAR)
      startIndex = _cubit.state.forecastTimeLines!.length - 4;

    /// quanth: tạo danh sách cận dưới
    for (int i = 0; i < (realValues?.length ?? 0); i++) {
      double value = realValues![i];
      if (i > startIndex) {
        value = double.parse(
            _cubit.state.forecastLOWRevenues![botIndex].toStringAsFixed(2));
        botIndex++;
        realValuesBot.add(value);
      } else {
        realValuesBot.add(realValues[i]);
      }
    }

    /// quanth: tạo danh sách cận trên
    for (int i = 0; i < (realValues?.length ?? 0); i++) {
      double value = realValues![i];
      if (i > startIndex) {
        value = double.parse(
            _cubit.state.forecastUPRevenues![topIndex].toStringAsFixed(2));
        topIndex++;
        realValuesTop.add(value);
      } else {
        realValuesTop.add(realValues[i]);
      }
    }

    /// quanth: tạo danh sách dự báo
    for (int i = 0; i < (realValues?.length ?? 0); i++) {
      double value = realValues![i];
      if (i > startIndex) {
        value = double.parse(
            _cubit.state.forecastBETRevenues![midIndex].toStringAsFixed(2));
        midIndex++;
        realValuesMid.add(value);
      } else {
        realValuesMid.add(realValues[i]);
      }
    }

    /// quanth: đối với 3 tháng thông thường, push các giá trị mặc định
    result.add(
      createLineChartData(
          list: realValues,
          colors: [AppColors.chartColors[chartPresenter.key!]]),
    );

    /// quanth: đối với 3 tháng dự báo push các giá trị từ api dự báo
    result.add(
      createDashLineNoDotChartData(
          list: realValuesTop,
          colors: [AppColors.chartColors[chartPresenter.key!]]),
    );
    result.add(
      createDashLineNoDotChartData(
          list: realValuesBot,
          colors: [AppColors.chartColors[chartPresenter.key!]]),
    );
    result.add(
      createDashLineChartData(
          list: realValuesMid,
          colors: [AppColors.chartColors[chartPresenter.key!]]),
    );
    return result;
  }
}
