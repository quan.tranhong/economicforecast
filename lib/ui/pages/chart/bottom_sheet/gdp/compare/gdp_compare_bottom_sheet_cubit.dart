import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/sync_chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/line_chart_filter_param.dart';
import 'package:flutter_base/models/params/sync_chart_filter_param.dart';
import 'package:meta/meta.dart';

part 'gdp_compare_bottom_sheet_state.dart';

class GdpCompareBottomSheetCubit extends Cubit<GdpCompareBottomSheetState> {
  GdpCompareBottomSheetCubit() : super(GdpCompareBottomSheetState());

  void filterIndexChart({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(filterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          filterStatus: LoadStatus.SUCCESS,
          filterParams: LineChartFilterParam(chartPresenter: chartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(filterStatus: LoadStatus.FAILURE));
    }
  }
}
