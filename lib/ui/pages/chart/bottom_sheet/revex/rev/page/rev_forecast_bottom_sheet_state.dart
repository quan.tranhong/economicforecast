part of 'rev_forecast_bottom_sheet_cubit.dart';

@immutable
class RevForecastBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  LineChartFilterParam? filterParams;

  RevForecastBottomSheetState(
      {this.filterStatus, this.filterParams});

  RevForecastBottomSheetState copyWith({
    LoadStatus? filterStatus,
    LineChartFilterParam? filterParams,
  }) {
    return new RevForecastBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
    this.filterStatus ?? LoadStatus.LOADING,
    this.filterParams ?? LineChartFilterParam()
  ];
}
