import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/revex/rev/item/rev_forecast_bottom_sheet_item.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/revex/rev/page/rev_forecast_bottom_sheet_cubit.dart';
import 'package:flutter_base/utils/extensions.dart';

class RevForecastBottomSheetPage extends StatefulWidget {
  List<RevenueDataDetailEntity>? revenues;
  List<ChartPresenter>? selectedPresenter;
  Function? selectItemListener;

  RevForecastBottomSheetPage(
      {required this.revenues,
      required this.selectItemListener,
      required this.selectedPresenter});

  @override
  _RevForecastBottomSheetPageState createState() =>
      _RevForecastBottomSheetPageState();
}

class _RevForecastBottomSheetPageState
    extends State<RevForecastBottomSheetPage> {
  List<ChartPresenter> _chartPresenters = [];
  late RevForecastBottomSheetCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<RevForecastBottomSheetCubit>(context);
    createChartPresents();
    _cubit.filterIndexChart(chartPresenters: widget.selectedPresenter);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints:
          BoxConstraints(maxHeight: ScreenSize.of(context).height * 0.8),
      padding: EdgeInsets.only(bottom: 10),
      width: ScreenSize.of(context).width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(19.0),
          topLeft: Radius.circular(19.0),
        ),
      ),
      // height: ScreenSize.of(context).height /3,
      child: Container(
          margin: EdgeInsets.only(top: 10, left: 10, right: 10),
          child: _buildGridView()),
    );
  }

  Widget _buildGridView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  top: 10,
                  left: 5,
                  bottom: 10,
                  right: 5,
                ),
                child: Text(
                  "Các chỉ tiêu",
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),
            ),
            BlocBuilder<RevForecastBottomSheetCubit,
                    RevForecastBottomSheetState>(
                buildWhen: (prev, current) =>
                    prev.filterStatus != current.filterStatus,
                builder: (context, state) {
                  if (state.filterStatus == LoadStatus.LOADING) {
                    return Container();
                  } else if (state.filterStatus == LoadStatus.FAILURE) {
                    return Container();
                  }
                  return Visibility(
                    visible:
                        (state.filterParams?.chartPresenters?.length ?? 0) > 4,
                    child: Container(
                      constraints: BoxConstraints(maxWidth: 200),
                      padding: EdgeInsets.only(
                        top: 10,
                        left: 10,
                        bottom: 10,
                        right: 5,
                      ),
                      child: Text(
                        "Chỉ được chọn tối đa 4 chỉ tiêu khác nhau.",
                        style: AppTextStyle.redS9,
                        maxLines: 2,
                      ),
                    ),
                  );
                }),
            Container(width: 10),
            CircleButton(
              icon: Icon(
                Icons.close,
                color: AppColors.backgroundBlueDark,
              ),
              onTapListenter: _closeClickedListener,
            ),
          ],
        ),
        Expanded(
          child: GridView.builder(
            physics: ScrollPhysics(),
            itemCount: _chartPresenters.length,
            shrinkWrap: true,
            primary: false,
            itemBuilder: (context, index) {
              return _buildItemGrid(chartPresenter: _chartPresenters[index]);
            },
            // padding: EdgeInsets.all(largeMargin),
            // controller: _scrollController,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.4, crossAxisCount: 3),
          ),
        ),
        BlocBuilder<RevForecastBottomSheetCubit, RevForecastBottomSheetState>(
            buildWhen: (prev, current) =>
                prev.filterStatus != current.filterStatus,
            builder: (context, state) {
              if (state.filterStatus == LoadStatus.LOADING) {
                return Container();
              } else if (state.filterStatus == LoadStatus.FAILURE) {
                return Container();
              }
              return GestureDetector(
                onTap: () {
                  if ((state.filterParams?.chartPresenters?.length ?? 0) < 5) {
                    widget.selectItemListener!(
                        chartPresenters:
                            _cubit.state.filterParams?.chartPresenters);
                    Navigator.of(context).pop();
                  }
                },
                child: SafeArea(
                  child: Container(
                    decoration: BoxDecoration(
                      color:
                          (state.filterParams?.chartPresenters?.length ?? 0) < 5
                              ? AppColors.backgroundBlueDark
                              : Colors.grey,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      boxShadow: AppShadow.boxShadow,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Center(
                      child: Text(
                        "Áp dụng",
                        style: AppTextStyle.whiteS16Bold,
                      ),
                    ),
                  ),
                ),
              );
            }),
      ],
    );
  }

  void _closeClickedListener() {
    Navigator.of(context).pop();
  }

  Widget _buildItemGrid({required ChartPresenter chartPresenter}) {
    bool isSelected = false;
    List<ChartPresenter> containsItem = widget.selectedPresenter?.containChartPresenter(chartPresenter) ?? [];
    if (containsItem.isNotEmpty)
      isSelected = true;
    return RevForecastBottomSheetItem(
        chartPresenter: chartPresenter,
        selectItemListener: _selectItemListener,
        isSelected: isSelected);
  }

  void _selectItemListener(
      {required ChartPresenter chartPresenter, required bool isSelected}) {
    bool containItem = _cubit.state.filterParams?.chartPresenters
        ?.containChartPresenter(chartPresenter)?.isNotEmpty ?? false;
    List<ChartPresenter>? chartPresenters = _cubit.state.filterParams?.chartPresenters;
    if (isSelected) {
      if (!containItem) {
        chartPresenters?.add(chartPresenter);
        _cubit.filterIndexChart(
            chartPresenters: chartPresenters);
      }
    } else {
      if (containItem) {
        List<ChartPresenter> item = chartPresenters?.containChartPresenter(chartPresenter) ?? [];
        if(item.isNotEmpty){
          chartPresenters?.remove(item[0]);
          _cubit.filterIndexChart(
              chartPresenters: chartPresenters);
        }
      }
    }
  }

  void createChartPresents() {
    _chartPresenters = [];
    for (int i = 0; i < (widget.revenues?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: widget.revenues?[i].value,
        name: widget.revenues?[i].name,
      );
      _chartPresenters.add(chartPresenter);
    }
  }
}
