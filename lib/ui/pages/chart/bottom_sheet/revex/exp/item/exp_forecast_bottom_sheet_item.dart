import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';

class ExpForecastBottomSheetItem extends StatefulWidget {
  ChartPresenter? chartPresenter;
  bool? isSelected;
  Function? selectItemListener;

  ExpForecastBottomSheetItem({Key? key, this.chartPresenter, this.selectItemListener, this.isSelected}) : super(key: key);

  @override
  ExpForecastBottomSheetItemState createState() => ExpForecastBottomSheetItemState();
}

class ExpForecastBottomSheetItemState extends State<ExpForecastBottomSheetItem> {
  late bool _isSelected;

  @override
  void initState() {
    super.initState();
    _isSelected = widget.isSelected ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        setState(() {
          if(_isSelected){
            _isSelected = false;
          } else {
            _isSelected = true;
          }
          widget.selectItemListener!(chartPresenter: widget.chartPresenter, isSelected: _isSelected);
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: !_isSelected ? Colors.white : AppColors.greenForecastHex,
            borderRadius: BorderRadius.circular(12),
            boxShadow: AppShadow.boxLightShadow),
        margin: EdgeInsets.all(5),
        child: Column(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.all(5),
                child: Center(
                  child: Text(
                    widget.chartPresenter?.name ?? "",
                    style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                        color: AppColors.backgroundBlueDark),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: AppColors.chartColors[widget.chartPresenter!.key!], boxShadow: AppShadow.boxShadow),
              margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
              height: 5,
            ),
          ],
        ),
      ),
    );
  }

  void updateChoiceMatter(){
    setState(() {
      if(_isSelected){
        _isSelected = false;
      } else {
        _isSelected = true;
      }
    });
  }
}
