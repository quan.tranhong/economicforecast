import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/ui/components/appbar/custom_appbar.dart';

class StatisticalBottomSheetItem extends StatefulWidget {
  ChartPresenter? chartPresenter;
  bool? isSelected;
  Function? selectItemListener;

  StatisticalBottomSheetItem({Key? key, this.chartPresenter, this.selectItemListener, this.isSelected}) : super(key: key);

  @override
  StatisticalBottomSheetItemState createState() => StatisticalBottomSheetItemState();
}

class StatisticalBottomSheetItemState extends State<StatisticalBottomSheetItem> {
  late bool _isSelected;

  @override
  void initState() {
    super.initState();
    _isSelected = widget.isSelected ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        setState(() {
          if(!_isSelected){
            _isSelected = true;
            widget.selectItemListener!(chartPresenter: widget.chartPresenter, isSelected: _isSelected);
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: !_isSelected ? Colors.white : AppColors.greenForecastHex,
            borderRadius: BorderRadius.circular(12),
            boxShadow: AppShadow.boxLightShadow),
        margin: EdgeInsets.all(5),
        child: Column(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.all(5),
                child: Center(
                  child: Text(
                    widget.chartPresenter?.name ?? "",
                    style: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.bold,
                        color: AppColors.backgroundBlueDark),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  color: AppColors.chartColors[widget.chartPresenter!.key!], boxShadow: AppShadow.boxShadow),
              margin: EdgeInsets.only(bottom: 10, left: 10, right: 10),
              height: 5,
            ),
          ],
        ),
      ),
    );
  }

  void updateChoiceMatter(){
    setState(() {
      if(_isSelected){
        _isSelected = false;
      } else {
        _isSelected = true;
      }
    });
  }
}
