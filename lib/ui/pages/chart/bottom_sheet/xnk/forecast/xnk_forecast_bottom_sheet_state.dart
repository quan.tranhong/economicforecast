part of 'xnk_forecast_bottom_sheet_cubit.dart';

@immutable
class XnkForeBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  TwiceChartFilterParam? filterParams;

  XnkForeBottomSheetState(
      {this.filterStatus, TwiceChartFilterParam? filterParams}) {
    this.filterParams = filterParams ?? TwiceChartFilterParam();
  }

  XnkForeBottomSheetState copyWith({
    LoadStatus? filterStatus,
    TwiceChartFilterParam? filterParams,
  }) {
    return new XnkForeBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
        this.filterStatus ?? LoadStatus.LOADING,
        this.filterParams ?? TwiceChartFilterParam()
      ];
}
