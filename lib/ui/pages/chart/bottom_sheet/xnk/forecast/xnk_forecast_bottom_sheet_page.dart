import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/xnk/forecast/xnk_forecast_bottom_sheet_cubit.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/xnk/item/xnk_forecast_bottom_sheet_item.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class XNKForeBottomSheetPage extends StatefulWidget {
  XNKDataEntity? xnk;
  ChartPresenter? imSelectedPresenter;
  ChartPresenter? exSelectedPresenter;
  Function? selectItemListener;

  XNKForeBottomSheetPage({
    this.xnk,
    this.selectItemListener,
    this.imSelectedPresenter,
    this.exSelectedPresenter,
  });

  @override
  _XNKForeBottomSheetPageState createState() =>
      _XNKForeBottomSheetPageState();
}

class _XNKForeBottomSheetPageState extends State<XNKForeBottomSheetPage> {
  List<ChartPresenter> _imChartPresenters = [];
  List<ChartPresenter> _exChartPresenters = [];
  late XnkForeBottomSheetCubit _cubit;

  List<GlobalKey<XNKForeBottomSheetItemState>> itemGlobalKeys = [];

  @override
  void initState() {
    super.initState();
    _cubit = context.read<XnkForeBottomSheetCubit>();
    createChartPresents(xnk: widget.xnk);
    _cubit.filterIndexChart(
      imChartPresenter: widget.imSelectedPresenter,
      exChartPresenter: widget.exSelectedPresenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints:
          BoxConstraints(maxHeight: ScreenSize.of(context).height * 0.8),
      padding: EdgeInsets.only(bottom: 10),
      width: ScreenSize.of(context).width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(19.0),
          topLeft: Radius.circular(19.0),
        ),
      ),
      // height: ScreenSize.of(context).height /3,
      child: Container(
          margin: EdgeInsets.only(top: 10, left: 10, right: 10),
          child: _buildGridView()),
    );
  }

  Widget _buildGridView() {
    itemGlobalKeys.clear();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  top: 10,
                  left: 5,
                  bottom: 10,
                  right: 5,
                ),
                child: Text(
                  "Các chỉ tiêu",
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),
            ),
            Container(width: 10),
            CircleButton(
              icon: Icon(
                Icons.close,
                color: AppColors.backgroundBlueDark,
              ),
              onTapListenter: _closeClickedListener,
            ),
          ],
        ),
        Expanded(
          child: GridView.builder(
            physics: ScrollPhysics(),
            itemCount: _imChartPresenters.length,
            shrinkWrap: true,
            primary: false,
            itemBuilder: (context, index) {
              GlobalKey<XNKForeBottomSheetItemState> key = GlobalKey();
              itemGlobalKeys.add(key);
              return _buildItemGrid(
                imChartPresenter: _imChartPresenters[index],
                exChartPresenter: _exChartPresenters[index],
                key: key,
              );
            },
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.4, crossAxisCount: 3),
          ),
        ),
        BlocBuilder<XnkForeBottomSheetCubit, XnkForeBottomSheetState>(
            buildWhen: (prev, current) =>
                prev.filterStatus != current.filterStatus,
            builder: (context, state) {
              if (state.filterStatus == LoadStatus.LOADING) {
                return Container();
              } else if (state.filterStatus == LoadStatus.FAILURE) {
                return Container();
              }
              return GestureDetector(
                onTap: () {
                  widget.selectItemListener!(
                    imChartPresenter:
                        _cubit.state.filterParams?.imChartPresenter,
                    exChartPresenter:
                        _cubit.state.filterParams?.exChartPresenter,
                  );
                  Navigator.of(context).pop();
                },
                child: SafeArea(
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.backgroundBlueDark,
                      borderRadius: BorderRadius.all(Radius.circular(8)),
                      boxShadow: AppShadow.boxShadow,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Center(
                      child: Text(
                        "Áp dụng",
                        style: AppTextStyle.whiteS16Bold,
                      ),
                    ),
                  ),
                ),
              );
            }),
      ],
    );
  }

  void _selectItemListener({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
    bool? isSelected,
  }) {
    if (isSelected ?? false) {
      int index = _cubit.state.filterParams?.imChartPresenter?.key ?? -1;
      if (index != -1) {
        itemGlobalKeys[index].currentState?.updateChoiceMatter();
      }
      _cubit.filterIndexChart(
        imChartPresenter: imChartPresenter,
        exChartPresenter: exChartPresenter,
      );
    }
  }

  Widget _buildItemGrid({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
    GlobalKey<XNKForeBottomSheetItemState>? key,
  }) {
    bool isSelected = false;
    if (widget.imSelectedPresenter == imChartPresenter) isSelected = true;
    return XNKForeBottomSheetItem(
      imChartPresenter: imChartPresenter,
      exChartPresenter: exChartPresenter,
      selectItemListener: _selectItemListener,
      isSelected: isSelected,
      key: key,
    );
  }

  void createChartPresents({XNKDataEntity? xnk}) {
    _imChartPresenters = [];
    for (int i = 0; i < (xnk?.nhapkhau?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: xnk?.nhapkhau?[i].rate,
        name: xnk?.nhapkhau?[i].name,
      );
      _imChartPresenters.add(chartPresenter);
    }
    _exChartPresenters = [];
    for (int i = 0; i < (xnk?.xuatkhau?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: xnk?.xuatkhau?[i].rate,
        name: xnk?.xuatkhau?[i].name,
      );
      _exChartPresenters.add(chartPresenter);
    }
  }

  void _closeClickedListener() {
    Navigator.of(context).pop();
  }
}
