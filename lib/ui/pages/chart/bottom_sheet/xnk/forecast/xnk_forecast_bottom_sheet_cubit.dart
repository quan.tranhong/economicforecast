import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:meta/meta.dart';

import '../../../../../../models/params/twice_chart_filter_param.dart';

part 'xnk_forecast_bottom_sheet_state.dart';

class XnkForeBottomSheetCubit extends Cubit<XnkForeBottomSheetState> {
  XnkForeBottomSheetCubit() : super(XnkForeBottomSheetState());

  void filterIndexChart({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
  }) async {
    emit(state.copyWith(filterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          filterStatus: LoadStatus.SUCCESS,
          filterParams: TwiceChartFilterParam(
            imChartPresenter: imChartPresenter,
            exChartPresenter: exChartPresenter,
          ),
        ),
      );
    } catch (error) {
      emit(state.copyWith(filterStatus: LoadStatus.FAILURE));
    }
  }
}
