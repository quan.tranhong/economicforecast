import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/line_chart_filter_param.dart';
import 'package:meta/meta.dart';

part 'iip_statistical_bottom_sheet_state.dart';

class IIPStatBottomSheetCubit extends Cubit<IIPStatBottomSheetState> {
  IIPStatBottomSheetCubit() : super(IIPStatBottomSheetState());

  void filterIndexChart({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(filterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          filterStatus: LoadStatus.SUCCESS,
          filterParams: LineChartFilterParam(chartPresenter: chartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(filterStatus: LoadStatus.FAILURE));
    }
  }
}
