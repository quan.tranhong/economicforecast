part of 'iip_forecast_bottom_sheet_cubit.dart';

@immutable
class IIPForeBottomSheetState extends Equatable {

  final LoadStatus? filterStatus;
  LineChartFilterParam? filterParams;

  IIPForeBottomSheetState({
    this.filterStatus,
    LineChartFilterParam? filterParams
  }) {
    this.filterParams = filterParams ?? LineChartFilterParam();
  }

  IIPForeBottomSheetState copyWith({
    LoadStatus? filterStatus,
    LineChartFilterParam? filterParams,
  }) {
    return new IIPForeBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [this.filterStatus ?? LoadStatus.LOADING, this.filterParams ?? LineChartFilterParam()];

}
