import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:meta/meta.dart';

import '../../../../../../models/params/twice_chart_filter_param.dart';

part 'imex_statistical_bottom_sheet_state.dart';

class ImexStatBottomSheetCubit extends Cubit<ImexStatBottomSheetState> {
  ImexStatBottomSheetCubit() : super(ImexStatBottomSheetState());

  void filterIndexChart({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
    ChartPresenter? balChartPresenter,
  }) async {
    emit(state.copyWith(filterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          filterStatus: LoadStatus.SUCCESS,
          filterParams: TwiceChartFilterParam(
            imChartPresenter: imChartPresenter,
            exChartPresenter: exChartPresenter,
            balChartPresenter: balChartPresenter,
          ),
        ),
      );
    } catch (error) {
      emit(state.copyWith(filterStatus: LoadStatus.FAILURE));
    }
  }
}
