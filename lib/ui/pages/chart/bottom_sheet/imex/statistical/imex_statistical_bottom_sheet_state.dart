part of 'imex_statistical_bottom_sheet_cubit.dart';

@immutable
class ImexStatBottomSheetState extends Equatable {
  final LoadStatus? filterStatus;
  TwiceChartFilterParam? filterParams;

  ImexStatBottomSheetState(
      {this.filterStatus, TwiceChartFilterParam? filterParams}) {
    this.filterParams = filterParams ?? TwiceChartFilterParam();
  }

  ImexStatBottomSheetState copyWith({
    LoadStatus? filterStatus,
    TwiceChartFilterParam? filterParams,
  }) {
    return new ImexStatBottomSheetState(
      filterStatus: filterStatus ?? this.filterStatus,
      filterParams: filterParams ?? this.filterParams,
    );
  }

  @override
  List<Object> get props => [
        this.filterStatus ?? LoadStatus.LOADING,
        this.filterParams ?? TwiceChartFilterParam()
      ];
}
