import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/entities/pie_chart_presenter.dart';
import 'package:flutter_base/models/enums/imex_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/home/imex/imex_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ExStatChartPage extends StatefulWidget {

  ImexDataEntity? imex;
  Function onTouchDotListener;
  ImexType type;

  ExStatChartPage({
    this.imex,
    required this.onTouchDotListener,
    required this.type,
  });

  @override
  _ExStatChartPageState createState() => _ExStatChartPageState();
}

class _ExStatChartPageState extends State<ExStatChartPage> {
  late ImexCubit _cubit;
  int _touchedIndex = 0;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ImexCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<ImexCubit, ImexState>(
                  buildWhen: (prev, current) =>
                  prev.exChartFilterStatus != current.exChartFilterStatus,
                  builder: (context, state) {
                    if (state.exChartFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.exChartFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return _buildChart();
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _buildChart() {
    return PieChart(
      PieChartData(
          pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
            setState(() {
              final desiredTouch =
                  pieTouchResponse.touchInput is! PointerExitEvent &&
                      pieTouchResponse.touchInput is! PointerUpEvent;
              if (desiredTouch && pieTouchResponse.touchedSection != null) {
                _touchedIndex =
                    pieTouchResponse.touchedSection!.touchedSectionIndex;
              } else {
                _touchedIndex = -1;
              }
            });
          }),
          borderData: FlBorderData(
            show: false,
          ),
          sectionsSpace: 0,
          centerSpaceRadius: 0,
          sections: showingSections()),
    );
  }

  PieChartSectionData getPieChartSecData({
    double? radius,
    double? fontSize,
    double? value,
    String? name,
    Color? color,
  }) {
    return PieChartSectionData(
      color: color,
      value: value,
      title: name,
      radius: radius,
      titleStyle: TextStyle(
          fontSize: fontSize,
          fontWeight: FontWeight.bold,
          color: const Color(0xffffffff)),
      badgePositionPercentageOffset: .98,
    );
  }

  List<PieChartSectionData> showingSections() {
    List<PieChartPresenter> pieChartPresenters =
        _cubit.state.exChartPresenters ?? [];
    List<PieChartSectionData> results = [];
    for (int i = 0; i < pieChartPresenters.length; i++) {
      final isTouched = i == _touchedIndex;
      final fontSize = isTouched ? 20.0 : 16.0;
      final radius = isTouched ? 110.0 : 100.0;

      PieChartSectionData pie = getPieChartSecData(
          radius: radius,
          fontSize: fontSize,
          name: pieChartPresenters[i].name,
          value: pieChartPresenters[i].value,
          color: (i == 0 ? Color(0xffBB5A25) : Color(0xff809F44)));
      results.add(pie);
    }

    return results;
  }
}
