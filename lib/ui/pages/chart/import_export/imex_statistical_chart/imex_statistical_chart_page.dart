import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/screen_size.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/home/imex/imex_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ImexStatChartPage extends StatefulWidget {
  Function onTouchDotListener;

  ImexStatChartPage({required this.onTouchDotListener});

  @override
  _ImexStatChartPageState createState() => _ImexStatChartPageState();
}

class _ImexStatChartPageState extends State<ImexStatChartPage> {
  late ImexCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ImexCubit>(context);
    _cubit.initStatisticalChartPresenter(
        imChartPresenter:
            _cubit.state.statisticalFilterParams?.imChartPresenter,
        exChartPresenter:
            _cubit.state.statisticalFilterParams?.exChartPresenter,
        balChartPresenter:
            _cubit.state.statisticalFilterParams?.balChartPresenter);
  }

  double getMinY({double? maxYvalue}) {
    double min = 0;
    ChartPresenter? chartPresenter =
        _cubit.state.statisticalFilterParams?.balChartPresenter;
    List<double>? list = _createRealValue(chartPresenter?.value ?? []);
    if (list?.isNotEmpty ?? false) {
      for (int i = 0; i < (maxYvalue ?? 0); i++) {
        if ((list?[i] ?? 0) < min) min = list?[i] ?? 0.0;
      }
    }
    return min;
  }

  double getMaxY({double? maxYvalue}) {
    double? max = 0.0;
    ChartPresenter imChartPresenter =
        _cubit.state.statisticalFilterParams?.imChartPresenter ??
            ChartPresenter();
    List<double>? imList = _createRealValue(imChartPresenter.value ?? []);
    if (imList?.isNotEmpty ?? false) {
      for (int i = 0; i < (maxYvalue ?? 0); i++) {
        if ((imList?[i] ?? 0.0) > max!) max = imList?[i] ?? 0.0;
      }
    }
    ChartPresenter exChartPresenter =
        _cubit.state.statisticalFilterParams?.exChartPresenter ??
            ChartPresenter();
    List<double>? exList = _createRealValue(exChartPresenter.value ?? []);
    if (exList?.isNotEmpty ?? false) {
      for (int i = 0; i < (maxYvalue ?? 0); i++) {
        if ((exList?[i] ?? 0.0) > max!) max = exList?[i] ?? 0.0;
      }
    }
    return max!;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<ImexCubit, ImexState>(
                  buildWhen: (prev, current) =>
                      prev.statisticalFilterStatus !=
                          current.statisticalFilterStatus ||
                      prev.statisticalType != current.statisticalType,
                  builder: (context, state) {
                    if (state.statisticalFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.statisticalFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return BarChart(
                      getBarChartData(maxYvalue: state.statisticalType?.statValue ?? ForecastDurationType.SIX_MONTH.statValue),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  BarChartData getBarChartData({double? maxYvalue}) {
    double? minY =
        double.parse(getMinY(maxYvalue: maxYvalue).toStringAsFixed(0));
    double? maxY =
        double.parse(getMaxY(maxYvalue: maxYvalue).toStringAsFixed(0));
    double? max = (minY.abs() > maxY.abs()) ? minY.abs() : maxY.abs();
    double keyIndex = getCS10(max);
    double? i = double.parse((max / keyIndex).toStringAsFixed(0));
    if (max % keyIndex != 0) {
      max = (i + 1) * keyIndex;
    }
    List<BarChartGroupData> barGroups = getBarGroups(maxYvalue: maxYvalue);
    return BarChartData(
      alignment: BarChartAlignment.spaceAround,
      barTouchData: BarTouchData(
        enabled: true,
        touchCallback: (BarTouchResponse touchResponse) {
          if (touchResponse != null) {
            BarTouchedSpot? spot = touchResponse.spot;
            widget.onTouchDotListener(barTouchedSpot: spot);
          }
        },
        touchTooltipData: BarTouchTooltipData(
          tooltipBgColor: Colors.transparent,
          tooltipMargin: -10,
          getTooltipItem: (
            BarChartGroupData group,
            int groupIndex,
            BarChartRodData rod,
            int rodIndex,
          ) {
            Color color = Colors.transparent;
            return BarTooltipItem(
              "${rod.y}",
              TextStyle(
                  fontSize: 12, color: color, fontWeight: FontWeight.bold),
            );
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
        checkToShowHorizontalLine: (value) => value % keyIndex == 0,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: (value == 0) ? AppColors.backgroundBlueDark : AppColors.gray,
            strokeWidth: (value == 0) ? 1 : 0.5,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          rotateAngle: 60,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          getTitles: (value) {
            if (value >= 0 && value < maxYvalue!)
              return _cubit.state.statisticalTimeLines?[(value).toInt()] ?? "";
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          getTitles: (value) {
            if (value == 0) {
              return '0';
            }
            return '${value.toInt() / keyIndex}';
          },
          checkToShowTitle:
              (minValue, maxValue, sideTitles, appliedInterval, value) =>
                  value % keyIndex == 0,
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(color: Colors.grey, width: 0.5),
          left: BorderSide(
            color: AppColors.backgroundBlueDark,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minY: -1 * max,
      maxY: max,
      barGroups: barGroups,
    );
  }

  BarChartGroupData createBarChartData({
    int? x,
    double? imY,
    List<Color>? imColors,
    double? exY,
    List<Color>? exColors,
    double? balY,
    List<Color>? balColors,
  }) {
    double width = ScreenSize.of(context).width / 40;
    switch (_cubit.state.statisticalType) {
      case ForecastDurationType.SIX_MONTH:
        width = ScreenSize.of(context).width / 40;
        break;
      case ForecastDurationType.A_YEAR:
        width = ScreenSize.of(context).width / 80;
        break;
    }
    return BarChartGroupData(
      x: x ?? 0,
      barRods: [
        BarChartRodData(
          width: width,
          y: imY ?? 0,
          colors: imColors,
          borderRadius: BorderRadius.all(Radius.zero),
        ),
        BarChartRodData(
          width: width,
          y: exY ?? 0,
          colors: exColors,
          borderRadius: BorderRadius.all(Radius.zero),
        ),
        BarChartRodData(
          width: width,
          y: balY ?? 0,
          colors: balColors,
          borderRadius: BorderRadius.all(Radius.zero),
        )
      ],
    );
  }

  List<BarChartGroupData> getBarGroups({double? maxYvalue}) {
    ChartPresenter imChartPresenter =
        _cubit.state.statisticalFilterParams?.imChartPresenter ??
            ChartPresenter();
    ChartPresenter exChartPresenter =
        _cubit.state.statisticalFilterParams?.exChartPresenter ??
            ChartPresenter();
    ChartPresenter balChartPresenter =
        _cubit.state.statisticalFilterParams?.balChartPresenter ??
            ChartPresenter();
    if (imChartPresenter == null ||
        exChartPresenter == null ||
        balChartPresenter == null) {
      return [];
    }
    List<double>? realImValues = _createRealValue(imChartPresenter.value ?? []);
    List<double>? realExValues = _createRealValue(exChartPresenter.value ?? []);
    List<double>? realBalValues = _createRealValue(balChartPresenter.value ?? []);

    List<BarChartGroupData> result = [];
    for (int i = 0; i < maxYvalue!; i++) {
      double imY = realImValues![i];
      double exY = realExValues![i];
      double balY = realBalValues![i];

      result.add(
        createBarChartData(
          x: i,
          imY: imY,
          imColors: [AppColors.imexBlueDark],
          exY: exY,
          exColors: [AppColors.imexRedDark],
          balY: balY,
          balColors: [AppColors.imexGreenDark],
        ),
      );
    }
    return result;
  }

  // quanth: phân tích a = b * 10^n, sô cần tìm là 10^n
  double getCS10(double a) {
    double count = 0;
    while (a > 10) {
      a = a / 10;
      count++;
    }
    if (count == 0) return 1;
    // tính lũy thừa mũ count
    double result = 1;
    for (int i = 0; i < count; i++) {
      result *= 10;
    }
    return result;
  }

  List<double>? _createRealValue(List<double>? oldValue) {
    List<double>? realValues = [];
    realValues.addAll(oldValue ?? []);
    if (realValues.isNotEmpty) {
      switch (_cubit.state.statisticalType) {
        case ForecastDurationType.SIX_MONTH:
          realValues = realValues.sublist(6, 12);
          break;
        case ForecastDurationType.A_YEAR:
          realValues = realValues.sublist(0, 12);
          break;
        case ForecastDurationType.NINE_MONTH:
          realValues = realValues.sublist(6, 15);
          break;
        case ForecastDurationType.OVER_A_YEAR:
          realValues = realValues.sublist(0, 15);
          break;
        default:
          realValues = realValues.sublist(6, 12);
      }
    }
    return realValues;
  }
}
