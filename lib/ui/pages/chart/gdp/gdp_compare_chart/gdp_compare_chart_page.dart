import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/home/gdp/gdp_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GDPCompareChartPage extends StatefulWidget {
  Function onTouchDotListener;
  List<int>? timeline;
  ChartPresenter? chartPresenter;

  GDPCompareChartPage({
    required this.onTouchDotListener,
    this.timeline,
    this.chartPresenter,
  });

  @override
  _GDPCompareChartState createState() => _GDPCompareChartState();
}

class _GDPCompareChartState extends State<GDPCompareChartPage> {
  late GdpCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<GdpCubit>(context);
    _cubit.initCompareChartPresenter(
        chartPresenter: _cubit.state.compareFilterParams!.chartPresenter!);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<GdpCubit, GdpState>(
                  buildWhen: (prev, current) =>
                      prev.compareFilterStatus != current.compareFilterStatus,
                  builder: (context, state) {
                    if (state.compareFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.compareFilterStatus ==
                        LoadStatus.FAILURE) {
                      return Container();
                    }
                    return LineChart(
                      createChartData(
                          maxYvalue: ForecastDurationType.SIX_MONTH.value),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  LineChartData createChartData({double? maxYvalue}) {
    List<LineChartBarData> lineBarsDatas = linesBarData(maxYValue: maxYvalue!);
    double? minY =
        double.parse(getMinY(maxYvalue: maxYvalue).toStringAsFixed(0));
    double keyIndex = getCS10(minY);

    return LineChartData(
      lineTouchData: LineTouchData(
        enabled: true,
        touchCallback: (LineTouchResponse? touchResponse) {
          if (touchResponse != null) {
            List<LineBarSpot>? lineBarSpots = touchResponse.lineBarSpots;
            widget.onTouchDotListener(lineBarSpots: lineBarSpots);
          }
        },
        getTouchedSpotIndicator:
            (LineChartBarData barData, List<int> spotIndexes) {
          return spotIndexes.map((index) {
            return TouchedSpotIndicatorData(
              FlLine(
                color: Colors.pink,
              ),
              FlDotData(
                show: true,
                getDotPainter: (spot, percent, barData, index) =>
                    FlDotCirclePainter(
                  radius: 8,
                  color: Colors.red,
                  strokeWidth: 2,
                  strokeColor: Colors.black,
                ),
              ),
            );
          }).toList();
        },
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.pink,
          tooltipRoundedRadius: 8,
          getTooltipItems: (List<LineBarSpot> lineBarsSpot) {
            return lineBarsSpot.map((lineBarSpot) {
              return null;
            }).toList();
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
      ),
      titlesData: FlTitlesData(
        /// quanth: danh sách giá trị trục hoành
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          rotateAngle: 60,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.bold,
            fontSize: 10,
          ),
          margin: 10,
          getTitles: (value) {
            if (value > 0 && value < maxYvalue)
              return widget.timeline![(value - 1).toInt()].toString();
            return '';
          },
        ),

        /// quanth: danh sách giá trị trục tung
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.bold,
            fontSize: 10,
          ),
          getTitles: (value) {
            return '${value / keyIndex * 10}';
          },
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: AppColors.gray,
          ),
          left: BorderSide(
            color: AppColors.gray,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      lineBarsData: lineBarsDatas,
    );
  }

  List<LineChartBarData> linesBarData({double? maxYValue}) {
    if (_cubit.state.compareFilterParams!.chartPresenter == null) {
      return [];
    }
    List<LineChartBarData> result = [];
    List<double> currentList =
        _cubit.state.compareFilterParams!.chartPresenter!.value!;
    List<double> countryList = [];
    for (int i = 0; i < currentList.length; i++) {
      if (i % 3 == 0) {
        countryList.add(currentList[i] + 30000);
      } else if (i % 3 == 1)
        countryList.add(currentList[i] - 10000);
      else
        countryList.add(currentList[i] - 20000);
    }

    result.add(
      createLineChartData(
          list: _cubit.state.compareFilterParams!.chartPresenter!.value!,
          colors: [
            Colors.red
          ]),
    );
    result.add(
      createDashLineChartData(list: countryList, colors: [
        Colors.green
      ]),
    );

    return result;
  }

  LineChartBarData createLineChartData(
      {List<double>? list, List<Color>? colors}) {
    return LineChartBarData(
      spots: [
        FlSpot(1, list![0]),
        FlSpot(2, list[1]),
        FlSpot(3, list[2]),
        FlSpot(4, list[3]),
        FlSpot(5, list[4]),
        FlSpot(6, list[5]),
      ],
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
    );
  }

  LineChartBarData createDashLineChartData(
      {List<double>? list, List<Color>? colors}) {
    return LineChartBarData(
      spots: [
        FlSpot(1, list![0]),
        FlSpot(2, list[1]),
        FlSpot(3, list[2]),
        FlSpot(4, list[3]),
        FlSpot(5, list[4]),
        FlSpot(6, list[5]),
      ],
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      // dashArray: [2, 4],
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
    );
  }

  // quanth: phân tích a = b * 10^n, sô cần tìm là 10^n
  double getCS10(double a) {
    double count = 0;
    while (a > 10) {
      a = a / 10;
      count++;
    }
    if (count == 0) return 1;
    // tính lũy thừa mũ count
    double result = 1;
    for (int i = 0; i < count; i++) {
      result *= 10;
    }
    return result;
  }

  double getMinY({double? maxYvalue}) {
    double? min;
    ChartPresenter? chartPresenter =
        _cubit.state.compareFilterParams!.chartPresenter!;
    List<double>? list = chartPresenter.value;
    if (list?.isNotEmpty ?? false) {
      for (int i = 0; i < (maxYvalue ?? 0); i++) {
        if(min == null) min = list?[i] ?? 0.0;
        else if ((list?[i] ?? 0) < min) min = list?[i] ?? 0.0;
      }
    }
    return min ?? 0;
  }
}
