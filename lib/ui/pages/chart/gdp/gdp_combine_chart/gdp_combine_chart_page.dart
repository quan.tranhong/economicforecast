import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/models/entities/item_sync_chart_presenter.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/ui/pages/home/gdp/gdp_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GDPCombineChartPage extends StatefulWidget {
  ForecastDurationType combineType;
  List<ItemSyncChartPresenter>? statPresenters = [];
  List<ItemSyncChartPresenter>? ratePresenters = [];
  Function onTouchDotListener;

  GDPCombineChartPage(
      {this.statPresenters,
      this.ratePresenters,
      required this.combineType,
      required this.onTouchDotListener});

  @override
  _GDPCombineChartState createState() => _GDPCombineChartState();
}

class _GDPCombineChartState extends State<GDPCombineChartPage> {
  late List<charts.Series<ItemSyncChartPresenter, String>> seriesList;
  late bool animate;
  late GdpCubit _cubit;

  @override
  void initState() {
    super.initState();
    animate = true;
    _cubit = BlocProvider.of<GdpCubit>(context);
    _cubit.updateCombineType(combineType: widget.combineType);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GdpCubit, GdpState>(
      buildWhen: (prev, current) => prev.combineType != current.combineType,
      builder: (context, state) {
        seriesList = _createMultipleData(
            maxYValue: state.combineType?.statValue.toInt() ??
                ForecastDurationType.SIX_MONTH.statValue.toInt());
        return Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(18)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                  child: _buildChart(),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildChart() {
    return charts.OrdinalComboChart(
      seriesList,
      animate: animate,
      domainAxis: charts.OrdinalAxisSpec(
        renderSpec: charts.SmallTickRendererSpec(
          labelRotation: 60,
          labelStyle: TextStyleSpec(
            fontSize: 9,
            color: charts.ColorUtil.fromDartColor(AppColors.backgroundBlueDark),
          ),
        ),
      ),
      // Configure the default renderer as a bar renderer.
      defaultRenderer: new charts.BarRendererConfig(
        groupingType: charts.BarGroupingType.grouped,
      ),
      primaryMeasureAxis: new charts.NumericAxisSpec(
        tickProviderSpec:
            new charts.BasicNumericTickProviderSpec(desiredTickCount: 5),
        renderSpec: charts.SmallTickRendererSpec(
          labelStyle: TextStyleSpec(
            fontSize: 9,
            color: charts.ColorUtil.fromDartColor(AppColors.backgroundBlueDark),
          ),
        ),
        showAxisLine: true,
      ),
      secondaryMeasureAxis: new charts.NumericAxisSpec(
        tickProviderSpec:
            new charts.BasicNumericTickProviderSpec(desiredTickCount: 5),
        renderSpec: charts.SmallTickRendererSpec(
          labelStyle: TextStyleSpec(
            fontSize: 9,
            color: charts.ColorUtil.fromDartColor(AppColors.backgroundBlueDark),
          ),
        ),
        showAxisLine: true,
      ),
      // Custom renderer configuration for the line series. This will be used for
      // any series that does not define a rendererIdKey.
      customSeriesRenderers: [
        new charts.LineRendererConfig(
          // ID used to link series to this renderer.
          customRendererId: 'customLine',
          roundEndCaps: true,
          includePoints: true,
          radiusPx: 5,
        )
      ],
      selectionModels: [
        new charts.SelectionModelConfig(
          type: charts.SelectionModelType.info,
          updatedListener: _onSelectionChanged,
        )
      ],
    );
  }

  _onSelectionChanged(charts.SelectionModel model) {
    final selectedDatum = model.selectedDatum;
    if (selectedDatum.isNotEmpty) {
      List<ItemSyncChartPresenter> results = [];
      for (int i = 0; i < selectedDatum.length; i++) {
        ItemSyncChartPresenter presenter = ItemSyncChartPresenter(
          year: selectedDatum[i].datum.year ?? "",
          name: selectedDatum[i].datum.name ?? "",
          value: selectedDatum[i].datum.value,
          key: selectedDatum[i].datum.key,
        );
        results.add(presenter);
      }
      widget.onTouchDotListener(lineBarSpots: results);
    }
  }

  /// Create series list with multiple series
  List<charts.Series<ItemSyncChartPresenter, String>> _createMultipleData(
      {required int maxYValue}) {
    int currLength = widget.statPresenters?.length ?? 0;
    int max = currLength;
    if(max >= maxYValue) max = maxYValue;
    List<ItemSyncChartPresenter>? realValues = _createRealPresenter(widget.statPresenters);
    List<ItemSyncChartPresenter>? ratePresenters = _createRealPresenter(widget.ratePresenters);
    return [
      new charts.Series<ItemSyncChartPresenter, String>(
          id: 'stat',
          colorFn: (_, __) =>
              charts.ColorUtil.fromDartColor(AppColors.gdpBlueDark),
          domainFn: (ItemSyncChartPresenter sales, _) => sales.year ?? "",
          measureFn: (ItemSyncChartPresenter sales, _) => sales.value,
          data: realValues ?? []),
      new charts.Series<ItemSyncChartPresenter, String>(
          id: 'rate ',
          colorFn: (_, __) =>
              charts.ColorUtil.fromDartColor(AppColors.gdpOrangeDark),
          domainFn: (ItemSyncChartPresenter sales, _) => sales.year ?? "",
          measureFn: (ItemSyncChartPresenter sales, _) => sales.value,
          data: ratePresenters ?? [])
        // Configure our custom line renderer for this series.
        ..setAttribute(charts.rendererIdKey, 'customLine')
        ..setAttribute(charts.measureAxisIdKey, 'secondaryMeasureAxisId'),
    ];
  }

  List<ItemSyncChartPresenter>? _createRealPresenter(List<ItemSyncChartPresenter>? oldPresenters) {
    List<ItemSyncChartPresenter>? realValues = [];
    realValues.addAll(oldPresenters ?? []);
    if (realValues.isNotEmpty) {
      switch (_cubit.state.combineType) {
        case ForecastDurationType.SIX_MONTH:
          realValues = realValues.sublist((oldPresenters?.length ?? 6) - 6, oldPresenters?.length);
          break;
        case ForecastDurationType.A_YEAR:
          realValues = realValues.sublist(0, oldPresenters?.length);
          break;
        default:
          realValues = realValues.sublist((oldPresenters?.length ?? 6) - 6, oldPresenters?.length);
      }
    }
    return realValues;
  }

}
