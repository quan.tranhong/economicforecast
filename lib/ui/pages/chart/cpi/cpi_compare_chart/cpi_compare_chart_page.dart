import 'dart:collection';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/home/cpi/cpi_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CPICompareChartPage extends StatefulWidget {
  Function onTouchDotListener;
  ChartPresenter? chartPresenter;
  ForecastDurationType? compareType;

  CPICompareChartPage(
      {required this.onTouchDotListener,
      this.chartPresenter,
      this.compareType});

  @override
  _CPICompareChartPageState createState() => _CPICompareChartPageState();
}

class _CPICompareChartPageState extends State<CPICompareChartPage> {
  late CpiCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<CpiCubit>(context);
    _cubit.initCompareChartPresenter(
        chartPresenter: _cubit.state.compareFilterParams!.chartPresenter!);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 16.0, left: 6.0),
              child: BlocBuilder<CpiCubit, CpiState>(
                  buildWhen: (prev, current) =>
                      prev.compareFilterStatus != current.compareFilterStatus,
                  builder: (context, state) {
                    if (state.compareFilterStatus == LoadStatus.LOADING) {
                      return Container();
                    } else if (state.compareFilterStatus == LoadStatus.FAILURE) {
                      return Container();
                    }
                    return LineChart(
                      createChartData(
                          maxYvalue: widget.compareType?.value ??
                              ForecastDurationType.SIX_MONTH.value),
                      swapAnimationDuration:
                          const Duration(milliseconds: 250),
                    );
                  }),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  LineChartData createChartData({double? maxYvalue}) {
    List<LineChartBarData> lineBarsDatas = linesBarData(maxYValue: maxYvalue!);
    return LineChartData(
      lineTouchData: LineTouchData(
        enabled: true,
        touchCallback: (LineTouchResponse? touchResponse) {
          if(touchResponse != null){
            List<LineBarSpot>? lineBarSpots = touchResponse.lineBarSpots;
            widget.onTouchDotListener(lineBarSpots: lineBarSpots);
          }
        },
        getTouchedSpotIndicator: (LineChartBarData barData, List<int> spotIndexes) {
          return spotIndexes.map((index) {
            return TouchedSpotIndicatorData(
              FlLine(
                color: Colors.pink,
              ),
              FlDotData(
                show: true,
                getDotPainter: (spot, percent, barData, index) => FlDotCirclePainter(
                  radius: 8,
                  color: Colors.red,
                  strokeWidth: 2,
                  strokeColor: Colors.black,
                ),
              ),
            );
          }).toList();
        },
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.pink,
          tooltipRoundedRadius: 8,
          getTooltipItems: (List<LineBarSpot> lineBarsSpot) {
            return lineBarsSpot.map((lineBarSpot) {
              return null;
            }).toList();
          },
        ),
      ),
      gridData: FlGridData(
        show: true,
      ),
      titlesData: FlTitlesData(
        /// quanth: danh sách giá trị trục hoành
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          rotateAngle: 60,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          margin: 10,
          getTitles: (value) {
            if (value > 0 && value < maxYvalue)
              return _cubit.state.compareTimeLines![(value - 1).toInt()];
            return '';
          },
        ),

        /// quanth: danh sách giá trị trục tung
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: AppColors.backgroundBlueDark,
            fontWeight: FontWeight.normal,
            fontSize: 10,
          ),
          getTitles: (value) {
            if (value >= 90 && value % 5 == 0) return '$value';
            return '';
          },
          margin: 8,
          reservedSize: 25,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: AppColors.gray,
          ),
          left: BorderSide(
            color: AppColors.gray,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: maxYvalue,
      minY: 90,
      lineBarsData: lineBarsDatas,
    );
  }

  List<LineChartBarData> linesBarData({double? maxYValue}) {
    if (_cubit.state.compareFilterParams!.chartPresenter == null) {
      return [];
    }
    List<LineChartBarData> result = [];
    List<double> currentList = _cubit.state.compareFilterParams!.chartPresenter!.value!;
    List<double> countryList = [];
    for(int i = 0; i < currentList.length; i++){
      if(i % 3 == 0){
        countryList.add(currentList[i] + 3);
      } else if(i % 3 == 1)
        countryList.add(currentList[i] -1);
      else
        countryList.add(currentList[i] - 2);
    }

    List<double>? realCrrValues = _createRealValue(currentList);
    List<double>? realCounValues = _createRealValue(countryList);

    result.add(
      createLineChartData(
          list: realCrrValues,
          colors: [
            AppColors
                .chartColors[_cubit.state.compareFilterParams!.chartPresenter!.key!]
          ]),
    );
    result.add(
      createDashLineChartData(
          list: realCounValues,
          colors: [
            AppColors.chartColors[
            _cubit.state.compareFilterParams!.chartPresenter!.key!]
          ]),
    );

    return result;
  }

  LineChartBarData createLineChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];
    ForecastDurationType type = _cubit.state.compareType ?? ForecastDurationType.SIX_MONTH;
    for (int i = 0; i < ((type.value) - 1); i++) {
      flSpots.add(FlSpot(i + 1, list![i]));
    }
    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
    );
  }

  LineChartBarData createDashLineChartData(
      {List<double>? list, List<Color>? colors}) {
    List<FlSpot> flSpots = [];
    ForecastDurationType type = _cubit.state.compareType ?? ForecastDurationType.SIX_MONTH;
    for (int i = 0; i < ((type.value) - 1); i++) {
      flSpots.add(FlSpot(i + 1, list![i]));
    }
    return LineChartBarData(
      spots: flSpots,
      isCurved: true,
      curveSmoothness: 0,
      colors: colors,
      barWidth: 2,
      dashArray: [2, 4],
      isStrokeCapRound: true,
      dotData: FlDotData(show: true),
      belowBarData: BarAreaData(show: false),
    );
  }

  List<double>? _createRealValue(List<double>? oldValue) {
    List<double>? realValues = [];
    realValues.addAll(oldValue ?? []);
    if (realValues.isNotEmpty) {
      switch (_cubit.state.compareType) {
        case ForecastDurationType.SIX_MONTH:
          realValues = realValues.sublist(6, 12);
          break;
        case ForecastDurationType.A_YEAR:
          realValues = realValues.sublist(0, 12);
          break;
        case ForecastDurationType.NINE_MONTH:
          realValues = realValues.sublist(6, 15);
          break;
        case ForecastDurationType.OVER_A_YEAR:
          realValues = realValues.sublist(0, 15);
          break;
        default:
          realValues = realValues.sublist(6, 12);
      }
    }
    return realValues;
  }

}
