import 'dart:ffi';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/configs/app_config.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/repositories/auth_repository.dart';
import 'package:flutter_base/router/application.dart';
import 'package:flutter_base/router/routers.dart';
import 'package:flutter_base/ui/components/app_button.dart';
import 'package:flutter_base/ui/pages/sign_in/sign_in_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignInPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SignInPageState();
  }
}

class _SignInPageState extends State<SignInPage> {
  final _usernameController = TextEditingController(text: 'quanth');
  final _passwordController = TextEditingController(text: '12345678');

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  late SignInCubit _cubit;

  @override
  void initState() {
    _cubit = context.read<SignInCubit>();
    super.initState();
    initPlatformMethod();
    _cubit.listen((state) {
      if (state.signInStatus == SignInStatus.FAILURE) {
        _showMessage('Login failure');
      } else if (state.signInStatus == SignInStatus.SUCCESS) {
        Navigator.pop(context);
      } else if (state.signInStatus == SignInStatus.USERNAME_PASSWORD_INVALID) {
        _showMessage('Wrong Username or Password');
      }
    });
  }

  @override
  void dispose() {
    _cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      // backgroundColor: AppColors.main,
      body: buildBodyWidget(),
    );
  }

  Widget buildBodyWidget() {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [
          Color(0xff3a6186),
          Color(0xff89253e),
        ],
      )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 100,
                width: 100,
                margin:
                    EdgeInsets.only(bottom: AppDimens.formSigninButtonSpace),
                child: Image.asset(AppImages.icSplashLogo, fit: BoxFit.cover),
              ),
              Container(
                height: AppDimens.formHeight,
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: _usernameController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          hintText: S.of(context).username,
                          hintStyle: AppTextStyle.greyS16,
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: AppDimens.formInnerHorizontalMargin,
                              vertical: 0),
                        ),
                      ),
                    ),
                    Container(
                      width: AppDimens.formIconSize,
                      height: AppDimens.formIconSize,
                      margin: EdgeInsets.only(
                          right: AppDimens.formInnerHorizontalMargin),
                      child: Icon(Icons.person_sharp,
                          color: AppColors.backgroundGray),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ],
          ),
          SizedBox(height: 12),
          Container(
            height: AppDimens.formHeight,
            margin: EdgeInsets.symmetric(
                horizontal: AppDimens.formOuterHorizontalMargin),
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    controller: _passwordController,
                    keyboardType: TextInputType.visiblePassword,
                    decoration: InputDecoration(
                      hintText: S.of(context).password,
                      hintStyle: AppTextStyle.greyS16,
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                    ),
                    obscureText: true,
                  ),
                ),
                Container(
                  width: AppDimens.formIconSize,
                  height: AppDimens.formIconSize,
                  margin: EdgeInsets.only(
                      right: AppDimens.formInnerHorizontalMargin),
                  child: Icon(
                    Icons.lock,
                    color: AppColors.backgroundGray,
                  ),
                )
              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12),
            ),
          ),
          SizedBox(height: 12),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: AppDimens.formOuterHorizontalMargin),
            child: Text(
              S.of(context).register,
              style: TextStyle(color: AppColors.backgroundBlueLight),
              textAlign: TextAlign.right,
            ),
          ),
          SizedBox(height: 32),
          _buildSignButton(),
          SizedBox(height: 12),
          _buildSSOSignButton()
        ],
      ),
    );
  }

  Widget _buildSignButton() {
    return BlocBuilder<SignInCubit, SignInState>(
      buildWhen: (prev, current) {
        return prev.signInStatus != current.signInStatus;
      },
      builder: (context, state) {
        final isLoading = state.signInStatus == SignInStatus.LOADING;
        return Container(
          padding: EdgeInsets.symmetric(
              horizontal: AppDimens.formOuterHorizontalMargin),
          child: AppBlueDarkButton(
            title: S.of(context).login,
            onPressed: isLoading ? null : _signIn,
            isLoading: isLoading,
          ),
        );
      },
    );
  }

  Widget _buildSSOSignButton() {
    return GestureDetector(
      onTap: _ssoSigninPressHandle,
      child: Container(
        height: 48,
        margin: EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            Container(
              width: AppDimens.formIconSize * 2,
              margin: EdgeInsets.only(left: AppDimens.formInnerHorizontalMargin),
              child: Image.asset(AppImages.icLogoSSO),
            ),
            Expanded(
              child: Text(
                "Đăng nhập qua SSO Center",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w800,
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              width: AppDimens.formIconSize * 2,
              height: AppDimens.formIconSize,
              margin: EdgeInsets.only(right: AppDimens.formInnerHorizontalMargin),
            )
          ],
        ),
      ),
    );
  }

  void _ssoSigninPressHandle(){
    // ChooseAccDialog(
    //   context: context,
    //   selectedType: ForecastDurationType.SIX_MONTH,
    //   itemSelected: (ForecastDurationType selectedType ){
    //
    //   },
    // ).show();
    if(Platform.isAndroid)
      FlutterBridge.openSSOCenter();
  }

  void _signIn() {
    final username = _usernameController.text;
    final password = _passwordController.text;
    if (username.isEmpty) {
      _showMessage('Username is invalid');
      return;
    }
    if (password.isEmpty) {
      _showMessage('Password is invalid');
      return;
    }
    _cubit.signIn(username, password);
  }

  void _showMessage(String message) {
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState?.showSnackBar(SnackBar(content: Text(message)));
  }

  void initPlatformMethod() {
    FlutterBridge.platform.setMethodCallHandler(_handleMethod);
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case "message":
        debugPrint(call.arguments);
        return new Future.value("");
      case "_retrieveSSOToken":
        String token = call.arguments;
        if (token.trim().isNotEmpty) {
          showHome();
        }
    }
  }

  void showHome() {
    Application.router?.navigateTo(context, Routes.home);
  }
}
