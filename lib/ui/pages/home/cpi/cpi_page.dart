import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/appbar/shadow_banner.dart';
import 'package:flutter_base/ui/components/dialog/app_dialog.dart';
import 'package:flutter_base/ui/pages/home/cpi/loading/cpi_loading_page.dart';
import 'package:flutter_base/ui/pages/home/cpi/round_box/cpi_compare_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/cpi/round_box/cpi_forecast_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/cpi/round_box/cpi_statistical_round_box_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cpi_cubit.dart';

class CPIPage extends StatefulWidget {
  Function menuClick;

  CPIPage({required this.menuClick});

  @override
  _CPIPageState createState() => _CPIPageState();
}

class _CPIPageState extends State<CPIPage> {
  late CpiCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = context.read<CpiCubit>();
    _cubit.fetchCPI();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppColors.backgroundBlueLight,
        body: BlocBuilder<CpiCubit, CpiState>(
            buildWhen: (prev, current) =>
                prev.fetchCPIStatus != current.fetchCPIStatus,
            builder: (context, state) {
              if (state.fetchCPIStatus == LoadStatus.LOADING) {
                return _buildLoadingList();
              } else if (state.fetchCPIStatus == LoadStatus.FAILURE) {
                return _buildFailureList();
              }
              return _buildBodyWidget();
            }),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          backgroundColor: AppColors.backgroundBlueDark,
          expandedHeight: 200,
          floating: true,
          pinned: true,
          snap: false,
          stretch: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.menu),
            tooltip: 'Add new entry',
            onPressed: () {
              widget.menuClick();
            },
          ),
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            stretchModes: [StretchMode.zoomBackground],
            title: Text(
              S.of(context).cpi_title,
              style: AppTextStyle.whiteS15,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
            background: ShadowBanner(),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              /*Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Text(
                  S.of(context).cpi_title,
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),*/
              CPIForecastRoundBoxPage(
                topRightLargeText: "Dự báo các chỉ số kinh tế",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText: "Chỉ số CPI của tỉnh Đồng Nai năm 2017",
              ),
              CPIForcastStatRoundBoxPage(
                topRightLargeText: "Thống kê các chỉ số kinh tế",
                topRightSmallText: S.of(context).cpi_title,
              ),
              CPICompareRoundBoxPage(
                topRightLargeText: "So sánh các chỉ số kinh tế với cả nước",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText:
                    "Chỉ số CPI của tỉnh Đồng Nai so với cả nước năm 2017",
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildLoadingList() {
    return SafeArea(child: CPILoadingPage());
  }

  Widget _buildFailureList() {
    return SafeArea(child: Container());
  }

  Future<bool> _onBackPressed() async {
    AppDialog(
      context: context,
      title: 'Thoát khỏi ứng dụng!',
      description: 'Ấn đồng ý để thoát khỏi ứng dụng.',
      cancelText: 'Huỷ',
      okText: 'Đồng ý',
      onOkPressed: () {
        SystemNavigator.pop();
      },
      onCancelPressed: () {
        // do nothing
      },
    ).show();
    return Future<bool>.value(true);
  }
}
