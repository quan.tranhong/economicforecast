import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/appbar/shadow_banner.dart';
import 'package:flutter_base/ui/components/dialog/app_dialog.dart';
import 'package:flutter_base/ui/pages/home/cpi/loading/cpi_loading_page.dart';

import 'exp_forecast_chart/exp_forecast_round_box_page.dart';
import 'expenditure_cubit.dart';

class ExpPage extends StatefulWidget {
  Function menuClick;

  ExpPage({required this.menuClick});

  @override
  _ExpPageState createState() => _ExpPageState();
}

class _ExpPageState extends State<ExpPage> {
  late ExpCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ExpCubit>(context);
    _cubit.fetchRevex();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppColors.backgroundBlueLight,
        body: BlocBuilder<ExpCubit, ExpState>(
            buildWhen: (prev, current) =>
            prev.fetchRevexStatus != current.fetchRevexStatus,
            builder: (context, state) {
              if (state.fetchRevexStatus == LoadStatus.LOADING) {
                return _buildLoadingList();
              } else if (state.fetchRevexStatus == LoadStatus.FAILURE) {
                return _buildFailureList();
              }
              return _buildBodyWidget();
            }),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          backgroundColor: AppColors.backgroundBlueDark,
          expandedHeight: 200,
          floating: true,
          pinned: true,
          snap: false,
          stretch: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.menu),
            tooltip: 'Add new entry',
            onPressed: () {
              widget.menuClick();
            },
          ),
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            stretchModes: [StretchMode.zoomBackground],
            title: Text(
              S.of(context).export_title,
              style: AppTextStyle.whiteS15,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
            background: ShadowBanner(),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              /*Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Text(
                  S.of(context).cpi_title,
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),*/
              ExpForecastRoundBoxPage(
                topRightLargeText: "Dự báo các chỉ số kinh tế",
                topRightSmallText: S.of(context).export_title,
                bottomMidText: "Chỉ số Xuất khẩu của tỉnh Đồng Nai năm 2017",
              ),
              /*CPIForcastStatRoundBoxPage(
                topRightLargeText: "Thống kê các chỉ số kinh tế",
                topRightSmallText: S.of(context).cpi_title,
              ),
              CPICompareRoundBoxPage(
                topRightLargeText: "So sánh các chỉ số kinh tế với cả nước",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText:
                "Chỉ số CPI của tỉnh Đồng Nai so với cả nước năm 2017",
              ),*/
            ],
          ),
        )
      ],
    );
  }

  Widget _buildLoadingList() {
    return SafeArea(child: CPILoadingPage());
  }

  Widget _buildFailureList() {
    return SafeArea(child: Container());
  }

  Future<bool> _onBackPressed() async {
    AppDialog(
      context: context,
      title: 'Thoát khỏi ứng dụng!',
      description: 'Ấn đồng ý để thoát khỏi ứng dụng.',
      cancelText: 'Huỷ',
      okText: 'Đồng ý',
      onOkPressed: () {
        SystemNavigator.pop();
      },
      onCancelPressed: () {
        // do nothing
      },
    ).show();
    return Future<bool>.value(true);
  }
}
