import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/twice_chart_filter_param.dart';
import 'package:flutter_base/repositories/chart_repository.dart';
import 'package:flutter_base/utils/logger.dart';
import 'package:meta/meta.dart';

part 'xnk_state.dart';

class XnkCubit extends Cubit<XnkState> {
  ChartRepository? repository;

  XnkCubit({this.repository}) : super(XnkState());

  void fetchXNK() async {
    emit(state.copyWith(fetchXNKStatus: LoadStatus.LOADING));
    try {
      final xnkEntity = await repository?.fetchXNK();
      emit(state.copyWith(
          fetchXNKStatus: LoadStatus.SUCCESS, xnkEntity: xnkEntity));
    } catch (e) {
      emit(state.copyWith(fetchXNKStatus: LoadStatus.FAILURE));
      logger.e(e);
    }
  }

  /// quanth: statistical
  void filterStatisticalChart(
      {ChartPresenter? imChartPresenter,
      ChartPresenter? exChartPresenter,
      ChartPresenter? balChartPresenter}) async {
    emit(state.copyWith(
        statisticalFilterStatus: LoadStatus.LOADING,
        statLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
            statisticalFilterStatus: LoadStatus.SUCCESS,
            statisticalFilterParams: TwiceChartFilterParam(
                imChartPresenter: imChartPresenter,
                exChartPresenter: exChartPresenter,
                balChartPresenter: balChartPresenter),
            statLBSpots: [],
            statLBStatus: LoadStatus.SUCCESS),
      );
    } catch (error) {
      emit(state.copyWith(statisticalFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: statistical
  void filterRateChart(
      {ChartPresenter? imChartPresenter,
      ChartPresenter? exChartPresenter}) async {
    emit(state.copyWith(rateFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          rateFilterStatus: LoadStatus.SUCCESS,
          rateFilterParams: TwiceChartFilterParam(
            imChartPresenter: imChartPresenter,
            exChartPresenter: exChartPresenter,
          ),
        ),
      );
    } catch (error) {
      emit(state.copyWith(rateFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: statistical
  void filterForecastChart(
      {ChartPresenter? imChartPresenter,
      ChartPresenter? exChartPresenter}) async {
    emit(state.copyWith(
        forecastFilterStatus: LoadStatus.LOADING,
        forecastLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          forecastFilterStatus: LoadStatus.SUCCESS,
          forecastFilterParams: TwiceChartFilterParam(
            imChartPresenter: imChartPresenter,
            exChartPresenter: exChartPresenter,
          ),
          forecastLBSpots: [],
          forecastLBStatus: LoadStatus.SUCCESS,
          forecastType: ForecastDurationType.SIX_MONTH,
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initStatisticalChartPresenter(
      {ChartPresenter? imChartPresenter,
      ChartPresenter? exChartPresenter,
      ChartPresenter? balChartPresenter}) async {
    emit(state.copyWith(statisticalFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statisticalFilterStatus: LoadStatus.SUCCESS,
          statisticalFilterParams: TwiceChartFilterParam(
              imChartPresenter: imChartPresenter,
              exChartPresenter: exChartPresenter,
              balChartPresenter: balChartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(statisticalFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initRateChartPresenter({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
  }) async {
    emit(state.copyWith(rateFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          rateFilterStatus: LoadStatus.SUCCESS,
          rateFilterParams: TwiceChartFilterParam(
            imChartPresenter: imChartPresenter,
            exChartPresenter: exChartPresenter,
          ),
        ),
      );
    } catch (error) {
      emit(state.copyWith(rateFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initForecastChartPresenter({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
  }) async {
    emit(state.copyWith(forecastFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          forecastFilterStatus: LoadStatus.SUCCESS,
          forecastFilterParams: TwiceChartFilterParam(
            imChartPresenter: imChartPresenter,
            exChartPresenter: exChartPresenter,
          ),
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastFilterStatus: LoadStatus.FAILURE));
    }
  }

  void updateForecastType({ForecastDurationType? forecastType}) async {
    emit(
      state.copyWith(
        forecastType: forecastType,
      ),
    );
  }

  void updateStatisticalType({ForecastDurationType? statisticalType}) async {
    emit(
      state.copyWith(
        statisticalType: statisticalType,
      ),
    );
  }

  void updateForecastLBSpots({List<LineBarSpot>? forecastLBSpots}) async {
    emit(state.copyWith(forecastLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          forecastLBSpots: forecastLBSpots,
          forecastLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastLBStatus: LoadStatus.FAILURE));
    }
  }

  void updateStatLBSpots({List<BarTouchedSpot?>? statLBSpots}) async {
    emit(state.copyWith(statLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statLBSpots: statLBSpots,
          statLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(statLBStatus: LoadStatus.FAILURE));
    }
  }
}
