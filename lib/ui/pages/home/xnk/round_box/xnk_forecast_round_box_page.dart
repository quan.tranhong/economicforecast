import 'dart:collection';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/xnk/forecast/xnk_forecast_bottom_sheet_cubit.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/xnk/forecast/xnk_forecast_bottom_sheet_page.dart';
import 'package:flutter_base/ui/pages/chart/xnk/xnk_forecast_chart/xnk_forecast_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../xnk_cubit.dart';

class XNKForecastRoundBoxPage extends StatefulWidget {
  XNKDataEntity? xnk;
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  XNKForecastRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
    this.xnk,
  });

  @override
  _XNKForecastRoundBoxPageState createState() =>
      _XNKForecastRoundBoxPageState();
}

class _XNKForecastRoundBoxPageState extends State<XNKForecastRoundBoxPage> {
  late XnkCubit _cubit;
  List<ChartPresenter> _imChartPresenters = [];
  List<ChartPresenter> _exChartPresenters = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<XnkCubit>(context);
    createChartPresents(xnk: widget.xnk);
    _cubit.filterForecastChart(
        imChartPresenter: _imChartPresenters[0],
        exChartPresenter: _exChartPresenters[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<XnkCubit, XnkState>(
            buildWhen: (prev, current) =>
                prev.forecastType != current.forecastType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          BlocBuilder<XnkCubit, XnkState>(
            buildWhen: (prev, current) =>
            prev.forecastLBStatus != current.forecastLBStatus,
            builder: (context, state) {
              return _buildMidSmallHeader(
                  lineBarSpots: state.forecastLBSpots ?? []);
            },
          ),
          _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(text: widget.bottomMidText),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
                _cubit.state.forecastType ?? ForecastDurationType.SIX_MONTH,
          ),
          Container(
            width: 10,
          ),
          CircleButton(
            icon: Icon(
              Icons.settings,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _settingClickedListener,
          ),
        ],
      ),
    );
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader({List<LineBarSpot>? lineBarSpots}) {
    if (lineBarSpots?.isEmpty ?? false)
      return Container(
        height: 50,
      );
    List<Widget> list = [];
    for (int i = 0; i < (lineBarSpots?.length ?? 0); i++) {
      list.add(_buildMidSmallItem(lineBarSpot: lineBarSpots?[i]));
    }

    String time = "";
    String arr =
        widget.xnk?.years?[((lineBarSpots?[0].x ?? 2) - 1).toInt()] ?? "";
    String year = arr.split("-")[1];
    String month = arr.split("-")[0];
    time = "Tháng $month Năm $year";

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (lineBarSpots?.isNotEmpty ?? false)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Container(
              margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: list,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Text(
            "(tỷ lệ %)",
            style: AppTextStyle.greyS12,
            textAlign: TextAlign.start,
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _buildMidSmallItem({LineBarSpot? lineBarSpot}) {
    return Row(
      children: [
        Container(
          width: 20,
          height: 10,
          color: lineBarSpot?.bar.colors[0],
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          lineBarSpot?.y?.toString() ?? "",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: XNKForecastChartPage(
        onTouchDotListener: _onTouchDotListener,
        forecastType: ForecastDurationType.SIX_MONTH,
      ),
    );
  }

  Widget _buildFooter({String? text}) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Text(
          text ?? "",
          style: AppTextStyle.blueDarkS12Bold,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  void _onSettingClickListener() {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return XnkForeBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
  }

  Widget _buildBottomSheet() {
    return XNKForeBottomSheetPage(
      xnk: widget.xnk,
      selectItemListener: _selectItemListener,
      imSelectedPresenter: _cubit.state.forecastFilterParams?.imChartPresenter,
      exSelectedPresenter: _cubit.state.forecastFilterParams?.exChartPresenter,
    );
  }

  void _selectItemListener({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
  }) {
    _cubit.filterForecastChart(
      imChartPresenter: imChartPresenter,
      exChartPresenter: exChartPresenter,
    );
  }

  List<LineBarSpot> _lineBarSpots = [];

  void _onTouchDotListener({List<LineBarSpot>? lineBarSpots}) {
    try {
      /// quanth loaị bỏ các màu trùng nhau
      Map<Color, int> colorMap = HashMap<Color, int>();
      List<LineBarSpot> newlineBarSpots = [];

      for (int i = 0; i < (lineBarSpots?.length ?? 0); i++) {
        Color barColor = lineBarSpots?[i].bar.colors[0] ?? Colors.red;
        colorMap[barColor] = i;
      }

      colorMap.values.forEach((index) {
        newlineBarSpots.add(lineBarSpots![index]);
      });
      _cubit.updateForecastLBSpots(forecastLBSpots: newlineBarSpots);
    } catch (e, s) {
      print(s);
    }
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.xnkBlueDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Xuất khẩu",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.xnkRedDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Nhập khẩu",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void createChartPresents({XNKDataEntity? xnk}) {
    _imChartPresenters = [];
    for (int i = 0; i < (xnk?.nhapkhau?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: xnk?.nhapkhau?[i].rate,
        name: xnk?.nhapkhau?[i].name,
      );
      _imChartPresenters.add(chartPresenter);
    }
    _exChartPresenters = [];
    for (int i = 0; i < (xnk?.xuatkhau?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: xnk?.xuatkhau?[i].rate,
        name: xnk?.xuatkhau?[i].name,
      );
      _exChartPresenters.add(chartPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType: _cubit.state.forecastType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateForecastType(forecastType: type);
  }

  void _settingClickedListener() {
    _onSettingClickListener();
  }
}
