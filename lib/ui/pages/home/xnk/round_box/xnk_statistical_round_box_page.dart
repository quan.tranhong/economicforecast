import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/xnk/xnk_statistical_chart/xnk_statistical_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../chart/bottom_sheet/xnk/statistical/xnk_statistical_bottom_sheet_cubit.dart';
import '../../../chart/bottom_sheet/xnk/statistical/xnk_statistical_bottom_sheet_page.dart';
import '../xnk_cubit.dart';

class XNKStatisticalRoundBoxPage extends StatefulWidget {
  XNKDataEntity? xnk;
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  XNKStatisticalRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
    this.xnk,
  });

  @override
  _XNKStatisticalRoundBoxPageState createState() =>
      _XNKStatisticalRoundBoxPageState();
}

class _XNKStatisticalRoundBoxPageState
    extends State<XNKStatisticalRoundBoxPage> {
  late XnkCubit _cubit;
  List<ChartPresenter> _imChartPresenters = [];
  List<ChartPresenter> _exChartPresenters = [];
  List<ChartPresenter> _balChartPresenters = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<XnkCubit>(context);
    createChartPresents(xnk: widget.xnk);
    _cubit.filterStatisticalChart(
        imChartPresenter: _imChartPresenters[0],
        exChartPresenter: _exChartPresenters[0],
        balChartPresenter: _balChartPresenters[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 550,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<XnkCubit, XnkState>(
            buildWhen: (prev, current) =>
                prev.statisticalType != current.statisticalType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          BlocBuilder<XnkCubit, XnkState>(
            buildWhen: (prev, current) =>
            prev.statLBStatus != current.statLBStatus,
            builder: (context, state) {
              return _buildMidSmallHeader(barTouchedSpot: state.statLBSpots);
            },
          ),
          _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(text: widget.bottomMidText),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
            _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH,
          ),
          Container(
            width: 10,
          ),
          CircleButton(
            icon: Icon(
              Icons.settings,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _settingClickedListener,
          ),
        ],
      ),
    );
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader({List<BarTouchedSpot?>? barTouchedSpot}) {
    if (barTouchedSpot == null ||
        barTouchedSpot.isEmpty ||
        barTouchedSpot.contains(null))
      return Container(
        height: 50,
      );

    String time = "";
    String arr =
        widget.xnk?.years?[(barTouchedSpot[0]!.touchedBarGroup.x).toInt()] ?? "";
    String year = arr.split("-")[1];
    String month = arr.split("-")[0];
    time = "Tháng $month Năm $year";

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (barTouchedSpot != null)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Container(
              margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
              child: _buildMidSmallItem(barTouchedSpot: barTouchedSpot[0]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Row(
        children: [
          Text(
            "(đơn vị: 10 nghìn tỷ VND)",
            style: AppTextStyle.greyS9,
            textAlign: TextAlign.start,
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _buildMidSmallItem({BarTouchedSpot? barTouchedSpot}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 20,
          height: 10,
          color: barTouchedSpot?.touchedRodData.colors[0],
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          barTouchedSpot?.touchedRodData.y.toString() ?? "",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: XNKStatChartPage(
        onTouchDotListener: _onTouchDotListener,
      ),
    );
  }

  Widget _buildFooter({String? text}) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Text(
          text ?? "",
          style: AppTextStyle.blueDarkS12Bold,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  void _onSettingClickListener() {
    // if(item.priorityId.compareTo("")!=0){
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return XnkStatBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
    // }
  }

  Widget _buildBottomSheet() {
    return XNKStatisBottomSheetPage(
      xnk: widget.xnk,
      selectItemListener: _selectItemListener,
      imSelectedPresenter:
          _cubit.state.statisticalFilterParams?.imChartPresenter,
      exSelectedPresenter:
          _cubit.state.statisticalFilterParams?.exChartPresenter,
      balSelectedPresenter:
          _cubit.state.statisticalFilterParams?.balChartPresenter,
    );
  }

  void _selectItemListener({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
    ChartPresenter? balChartPresenter,
  }) {
    _cubit.filterStatisticalChart(
      imChartPresenter: imChartPresenter,
      exChartPresenter: exChartPresenter,
      balChartPresenter: balChartPresenter,
    );
  }

  BarTouchedSpot? _barTouchedSpot;

  void _onTouchDotListener({BarTouchedSpot? barTouchedSpot}) {
    try {
      _cubit.updateStatLBSpots(statLBSpots: [barTouchedSpot]);
    } catch (e, s) {
      print(s);
    }
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.xnkBlueDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Xuất khẩu",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.xnkRedDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Nhập khẩu",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.xnkGreenDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Cán cân TM",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void createChartPresents({XNKDataEntity? xnk}) {
    _imChartPresenters = [];
    for (int i = 0; i < (xnk?.nhapkhau?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: xnk?.nhapkhau?[i].val,
        name: xnk?.nhapkhau?[i].name,
      );
      _imChartPresenters.add(chartPresenter);
    }
    _exChartPresenters = [];
    for (int i = 0; i < (xnk?.xuatkhau?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: xnk?.xuatkhau?[i].val,
        name: xnk?.xuatkhau?[i].name,
      );
      _exChartPresenters.add(chartPresenter);
    }
    _balChartPresenters = [];
    for (int i = 0; i < (xnk?.xuatkhau?.length ?? 0); i++) {
      List<double>? balVals = [];
      for (int j = 0; j < (xnk?.xuatkhau?[i].val?.length ?? 0); j++) {
        double val = (xnk?.xuatkhau?[i].val?[j] ?? 0.0) -
            (xnk?.nhapkhau?[i].val?[j] ?? 0.0);
        balVals.add(val);
      }
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: balVals,
        name: xnk?.xuatkhau?[i].name,
      );
      _balChartPresenters.add(chartPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType:
      _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateStatisticalType(statisticalType: type);
  }

  void _settingClickedListener() {
    _onSettingClickListener();
  }
}
