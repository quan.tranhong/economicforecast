import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/appbar/shadow_banner.dart';
import 'package:flutter_base/ui/components/dialog/app_dialog.dart';
import 'package:flutter_base/ui/pages/home/cpi/loading/cpi_loading_page.dart';
import 'package:flutter_base/ui/pages/home/xnk/round_box/xnk_forecast_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/xnk/round_box/xnk_statistical_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/xnk/xnk_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class XNKPage extends StatefulWidget {
  Function menuClick;
  XNKPage({required this.menuClick});

  @override
  _XNKPageState createState() => _XNKPageState();
}

class _XNKPageState extends State<XNKPage> {
  late XnkCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<XnkCubit>(context);
    _cubit.fetchXNK();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppColors.backgroundBlueLight,
        body: BlocBuilder<XnkCubit, XnkState>(
            buildWhen: (prev, current) =>
            prev.fetchXNKStatus != current.fetchXNKStatus,
            builder: (context, state) {
              if (state.fetchXNKStatus == LoadStatus.LOADING) {
                return _buildLoadingList();
              } else if (state.fetchXNKStatus == LoadStatus.FAILURE) {
                return _buildFailureList();
              }
              return _buildBodyWidget();
            }),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          backgroundColor: AppColors.backgroundBlueDark,
          expandedHeight: 200,
          floating: true,
          pinned: true,
          snap: false,
          stretch: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.menu),
            tooltip: 'Add new entry',
            onPressed: () {
              widget.menuClick();
            },
          ),
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            stretchModes: [StretchMode.zoomBackground],
            title: Text(
              "Xuất Nhập khẩu",
              style: AppTextStyle.whiteS15,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
            background: ShadowBanner(),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              /*Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Text(
                  S.of(context).xnk_title,
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),*/
              XNKStatisticalRoundBoxPage(
                topRightLargeText: "Thống kê giá trị xuất nhập khẩu",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText: "Giá trị Xuất Nhập khẩu của tỉnh Đồng Nai năm 2021",
                xnk: _cubit.state.xnkEntity?.data,
              ),
              XNKForecastRoundBoxPage(
                topRightLargeText: "Thống kê tỷ lệ % xuất nhập khẩu",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText: "Tỷ lệ % Xuất Nhập khẩu của tỉnh Đồng Nai năm 2021",
                xnk: _cubit.state.xnkEntity?.data,
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildLoadingList() {
    return SafeArea(child: CPILoadingPage());
  }

  Widget _buildFailureList() {
    return SafeArea(child: Container());
  }

  Future<bool> _onBackPressed() async{
    AppDialog(
      context: context,
      title: 'Thoát khỏi ứng dụng!',
      description: 'Ấn đồng ý để thoát khỏi ứng dụng.',
      cancelText: 'Huỷ',
      okText: 'Đồng ý',
      onOkPressed: () {
        SystemNavigator.pop();
      },
      onCancelPressed: () {
        // do nothing
      },
    ).show();
    return Future<bool>.value(true);
  }
}