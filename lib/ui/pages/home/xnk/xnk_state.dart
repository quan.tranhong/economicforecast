part of 'xnk_cubit.dart';

@immutable
class XnkState extends Equatable {
  final XNKEntity? xnkEntity;
  final LoadStatus? fetchXNKStatus;

  /// quanth: statistical
  final LoadStatus? statisticalFilterStatus;
  TwiceChartFilterParam? statisticalFilterParams;
  ForecastDurationType? statisticalType;
  final LoadStatus? statLBStatus;
  List<BarTouchedSpot?>? statLBSpots;

  /// quanth: forecast
  final LoadStatus? forecastFilterStatus;
  TwiceChartFilterParam? forecastFilterParams;
  ForecastDurationType? forecastType;
  final LoadStatus? forecastLBStatus;
  List<LineBarSpot>? forecastLBSpots = [];

  /// quanth: rate
  final LoadStatus? rateFilterStatus;
  TwiceChartFilterParam? rateFilterParams;

  XnkState(
      {this.xnkEntity,
      this.fetchXNKStatus,
      this.statisticalFilterStatus,
      this.statisticalFilterParams,
      this.statisticalType,
      this.statLBStatus,
      this.statLBSpots,
      this.forecastFilterStatus,
      this.forecastFilterParams,
      this.forecastType,
      this.forecastLBStatus,
      this.forecastLBSpots,
      this.rateFilterStatus,
      this.rateFilterParams});

  XnkState copyWith({
    XNKEntity? xnkEntity,
    LoadStatus? fetchXNKStatus,
    LoadStatus? statisticalFilterStatus,
    TwiceChartFilterParam? statisticalFilterParams,
    ForecastDurationType? statisticalType,
    LoadStatus? statLBStatus,
    List<BarTouchedSpot?>? statLBSpots,
    LoadStatus? forecastFilterStatus,
    TwiceChartFilterParam? forecastFilterParams,
    ForecastDurationType? forecastType,
    LoadStatus? forecastLBStatus,
    List<LineBarSpot>? forecastLBSpots,
    LoadStatus? rateFilterStatus,
    TwiceChartFilterParam? rateFilterParams,
  }) {
    return new XnkState(
      xnkEntity: xnkEntity ?? this.xnkEntity,
      fetchXNKStatus: fetchXNKStatus ?? this.fetchXNKStatus,
      statisticalFilterStatus:
          statisticalFilterStatus ?? this.statisticalFilterStatus,
      statisticalFilterParams:
          statisticalFilterParams ?? this.statisticalFilterParams,
      statisticalType: statisticalType ?? this.statisticalType,
      statLBStatus: statLBStatus ?? this.statLBStatus,
      statLBSpots: statLBSpots ?? this.statLBSpots,
      forecastFilterStatus: forecastFilterStatus ?? this.forecastFilterStatus,
      forecastFilterParams: forecastFilterParams ?? this.forecastFilterParams,
      forecastType: forecastType ?? this.forecastType,
      forecastLBStatus: forecastLBStatus ?? this.forecastLBStatus,
      forecastLBSpots: forecastLBSpots ?? this.forecastLBSpots,
      rateFilterStatus: rateFilterStatus ?? this.rateFilterStatus,
      rateFilterParams: rateFilterParams ?? this.rateFilterParams,
    );
  }

  List<String>? get statisticalTimeLines {
    List<String> list = xnkEntity?.data?.years ?? [];
    if(list.isNotEmpty){
      switch(statisticalType){
        case ForecastDurationType.SIX_MONTH:
          return xnkEntity?.data?.years?.sublist(6,12);
        case ForecastDurationType.A_YEAR:
          return xnkEntity?.data?.years?.sublist(0,12);
        case ForecastDurationType.NINE_MONTH:
          return xnkEntity?.data?.years?.sublist(6,15);
        case ForecastDurationType.OVER_A_YEAR:
          return xnkEntity?.data?.years?.sublist(0,15);
        default:
          return xnkEntity?.data?.years?.sublist(6,12);
      }
    }
    return [];
  }

  List<String>? get forecastTimeLines {
    List<String> list = xnkEntity?.data?.years ?? [];
    if(list.isNotEmpty){
      switch(forecastType){
        case ForecastDurationType.SIX_MONTH:
          return xnkEntity?.data?.years?.sublist(6,12);
        case ForecastDurationType.A_YEAR:
          return xnkEntity?.data?.years?.sublist(0,12);
        case ForecastDurationType.NINE_MONTH:
          return xnkEntity?.data?.years?.sublist(6,15);
        case ForecastDurationType.OVER_A_YEAR:
          return xnkEntity?.data?.years?.sublist(0,15);
        default:
          return xnkEntity?.data?.years?.sublist(6,12);
      }
    }
    return [];
  }

  List<Object> get props => [
        this.xnkEntity ?? XNKEntity(),
        this.fetchXNKStatus ?? LoadStatus.LOADING,
        this.statisticalFilterParams ?? TwiceChartFilterParam(),
        this.statisticalFilterStatus ?? LoadStatus.LOADING,
        this.rateFilterParams ?? TwiceChartFilterParam(),
        this.rateFilterStatus ?? LoadStatus.LOADING,
        this.forecastFilterParams ?? TwiceChartFilterParam(),
        this.forecastFilterStatus ?? LoadStatus.LOADING,
        this.statisticalType ?? ForecastDurationType.SIX_MONTH,
        this.forecastType ?? ForecastDurationType.SIX_MONTH,
        this.forecastLBSpots ?? [],
        this.forecastLBStatus ?? LoadStatus.LOADING,
        this.statLBSpots ?? [],
        this.statLBStatus ?? LoadStatus.LOADING,
      ];
}
