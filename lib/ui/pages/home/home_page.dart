import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_images.dart';
import 'package:flutter_base/configs/app_config.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/page_type.dart';
import 'package:flutter_base/repositories/chart_repository.dart';
import 'package:flutter_base/router/application.dart';
import 'package:flutter_base/router/routers.dart';
import 'package:flutter_base/ui/components/appbar/custom_appbar.dart';
import 'package:flutter_base/ui/pages/home/cpi/cpi_page.dart';
import 'package:flutter_base/ui/pages/home/home_cubit.dart';
import 'package:flutter_base/ui/pages/home/imex/imex_page.dart';
import 'package:flutter_base/ui/pages/home/revenue/revenue_cubit.dart';
import 'package:flutter_base/ui/pages/home/unemployment/unemployment_cubit.dart';
import 'package:flutter_base/ui/pages/home/unemployment/unemployment_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cpi/cpi_cubit.dart';
import 'expenditure/expenditure_cubit.dart';
import 'expenditure/expenditure_page.dart';
import 'gdp/gdp_cubit.dart';
import 'gdp/gdp_page.dart';
import 'iip/iip_cubit.dart';
import 'iip/iip_page.dart';
import 'imex/imex_cubit.dart';
import 'revenue/revenue_page.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late HomeCubit _cubit;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void initPlatformMethod() {
    FlutterBridge.platform.setMethodCallHandler(_handleMethod);
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case "message":
      case "_retrieveSSOToken":
    }
  }

  @override
  void initState() {
    _cubit = context.read<HomeCubit>();
    super.initState();
    _cubit.listen((state) {
      if (state.signOutStatus == LoadStatus.SUCCESS) {
        _onSignOutSuccess();
      }
    });
    initPlatformMethod();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: AppColors.backgroundBlueLight,
      body: _buildPageView(),
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: ExactAssetImage(AppImages.icBackgroundGradient),
                    fit: BoxFit.fill),
              ),
              child: _buildHeaderItem(
                  name: "Trần Hồng Quân", image: AppImages.icPaul),
            ),
            /*DrawerHeader(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: ExactAssetImage(AppImages.icMapDN),
                    fit: BoxFit.fill),
              ),
              child: Container(
                height: 90,
                decoration: BoxDecoration(color: Colors.transparent),
                alignment: Alignment.center, // This is needed
              ),
            ),*/
            ListTile(
              title: _buildNavItem(
                  title: "Chỉ số CPI", image: AppImages.icCPIMenu),
              onTap: () {
                _cubit.changePage(PageType.CPI);
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            ListTile(
              title: _buildNavItem(
                  title: "Chỉ số IIP", image: AppImages.icIIPMenu),
              onTap: () {
                _cubit.changePage(PageType.IIP);
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            ListTile(
              title: _buildNavItem(
                  title: "Chỉ số GDP", image: AppImages.icGDPMenu),
              onTap: () {
                _cubit.changePage(PageType.GDP);
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            ListTile(
              title: _buildNavItem(
                  title: "Nhập khẩu", image: AppImages.icXNKMenu),
              onTap: () {
                _cubit.changePage(PageType.IMPORT);
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            ListTile(
              title: _buildNavItem(
                  title: "Xuất khẩu", image: AppImages.icXNKMenu),
              onTap: () {
                _cubit.changePage(PageType.EXPORT);
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            ListTile(
              title: _buildNavItem(
                  title: "Tỷ lệ thất nghiệp",
                  image: AppImages.icThatnghiepMenu),
              onTap: () {
                _cubit.changePage(PageType.UNEMPLOYMENT);
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            ListTile(
              title: _buildNavItem(
                  title: "Thu chi ngân sách", image: AppImages.icThuchiMenu),
              onTap: () {
                _cubit.changePage(PageType.IMPORT_EXPORT);
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
              },
            ),
            ListTile(
              title: _buildNavItem(
                  title: "Đăng xuất", image: AppImages.icLogoutMenu),
              onTap: () {
                FlutterBridge.logout();
                _cubit.signOut();
              },
            ),
          ],
        ),
      ),
      /*appBar: PreferredSize(
        preferredSize: Size.fromHeight(50.0), // here the desired height
        child: _buildAppBarWidget(title: S.of(context).economic_forecast),
      ),*/
    );
  }

  Widget _buildPageView() {
    return BlocBuilder<HomeCubit, HomeState>(
        buildWhen: (prev, current) => (prev.currentPage != current.currentPage),
        builder: (context, state) {
          switch (state.currentPage) {
            case PageType.CPI:
              return BlocProvider(
                create: (context) {
                  final chartRepository =
                      RepositoryProvider.of<ChartRepository>(context);
                  return CpiCubit(repository: chartRepository);
                },
                child: CPIPage(
                  menuClick: () {
                    _onMenuCLickListener();
                  },
                ),
              );
            case PageType.IIP:
              return BlocProvider(
                create: (context) {
                  final chartRepository =
                      RepositoryProvider.of<ChartRepository>(context);
                  return IipCubit(repository: chartRepository);
                },
                child: IIPPage(
                  menuClick: () {
                    _onMenuCLickListener();
                  },
                ),
              );
            case PageType.GDP:
              return BlocProvider(
                create: (context) {
                  final chartRepository =
                      RepositoryProvider.of<ChartRepository>(context);
                  return GdpCubit(repository: chartRepository);
                },
                child: GDPPage(
                  menuClick: () {
                    _onMenuCLickListener();
                  },
                ),
              );
            case PageType.IMPORT:
              return BlocProvider(
                create: (context) {
                  final chartRepository =
                      RepositoryProvider.of<ChartRepository>(context);
                  return RevCubit(repository: chartRepository);
                },
                child: RevPage(
                  menuClick: () {
                    _onMenuCLickListener();
                  },
                ),
              );
            case PageType.EXPORT:
              return BlocProvider(
                create: (context) {
                  final chartRepository =
                  RepositoryProvider.of<ChartRepository>(context);
                  return ExpCubit(repository: chartRepository);
                },
                child: ExpPage(
                  menuClick: () {
                    _onMenuCLickListener();
                  },
                ),
              );
            case PageType.UNEMPLOYMENT:
              return BlocProvider(
                create: (context) {
                  final chartRepository =
                      RepositoryProvider.of<ChartRepository>(context);
                  return UnemploymentCubit(repository: chartRepository);
                },
                child: UnemploymentPage(
                  menuClick: () {
                    _onMenuCLickListener();
                  },
                ),
              );
            case PageType.IMPORT_EXPORT:
              return BlocProvider(
                create: (context) {
                  final chartRepository =
                      RepositoryProvider.of<ChartRepository>(context);
                  return ImexCubit(repository: chartRepository);
                },
                child: ImexPage(
                  menuClick: () {
                    _onMenuCLickListener();
                  },
                ),
              );
          }
          return BlocProvider(
            create: (context) {
              final chartRepository =
                  RepositoryProvider.of<ChartRepository>(context);
              return CpiCubit(repository: chartRepository);
            },
            child: CPIPage(
              menuClick: () {
                _onMenuCLickListener();
              },
            ),
          );
        });
  }

  /// Build Appbar Widget
  Widget _buildAppBarWidget({String? title}) {
    return AppBar(
      flexibleSpace: Image(
        image: ExactAssetImage(AppImages.icBackgroundGradient),
        fit: BoxFit.cover,
      ),
      backgroundColor: Colors.transparent,
      shadowColor: Colors.white,
      elevation: 0.0,
      automaticallyImplyLeading: false,
      actions: [
        Container(),
      ],
      titleSpacing: 0,
      title: _buildTitle(title: title!),
    );
  }

  Widget _buildTitle({String? title}) {
    return CustomAppbar(
        title: title ?? "", onMenuCLickListener: _onMenuCLickListener);
  }

  Widget _buildNavItem({String? image, String? title}) {
    return Row(
      children: [
        Container(
          height: 30,
          decoration: BoxDecoration(color: Colors.transparent),
          alignment: Alignment.center, // This is needed
          child: Image.asset(
            image!,
            fit: BoxFit.contain,
            width: 30,
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          title ?? "",
          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: AppColors.backgroundBlueDark),
        )
      ],
    );
  }

  Widget _buildHeaderItem({String? image, String? name}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 90,
          decoration: BoxDecoration(color: Colors.transparent),
          alignment: Alignment.center, // This is needed
          child: Image.asset(
            image!,
            fit: BoxFit.contain,
            width: 90,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          name ?? "",
          style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: AppColors.backgroundBlueLight),
        )
      ],
    );
  }

  void _onMenuCLickListener() {
    _scaffoldKey.currentState?.openDrawer();
  }

  void _onSignOutSuccess() {
    Application.router?.navigateTo(context, Routes.root, clearStack: true);
  }
}
