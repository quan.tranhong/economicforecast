import 'dart:collection';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/entities/pie_chart_presenter.dart';
import 'package:flutter_base/models/enums/imex_type.dart';
import 'package:flutter_base/ui/pages/chart/import_export/imex_statistical_chart/ex_statistical_chart_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../imex_cubit.dart';

class ExStatisticalRoundBoxPage extends StatefulWidget {
  ImexDataEntity? imex;
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";
  ImexType type;

  ExStatisticalRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
    this.imex,
    required this.type,
  });

  @override
  _ExStatisticalRoundBoxPageState createState() =>
      _ExStatisticalRoundBoxPageState();
}

class _ExStatisticalRoundBoxPageState extends State<ExStatisticalRoundBoxPage> {
  late ImexCubit _cubit;
  List<PieChartPresenter> _pieChartPresenters = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ImexCubit>(context);
    createPieChartPresents(imex: widget.imex);
    _cubit.initEXChartPresenter(exChartPresenters: _pieChartPresenters);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildLargeHeader(text: widget.topRightLargeText),
          // _buildSmallHeader(text: widget.topRightSmallText),
          //_buildMidSmallHeader(lineBarSpots: _lineBarSpots),
          // _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(text: widget.bottomMidText),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          /*GestureDetector(
            onTap: () {},
            child: SizedBox.fromSize(
              size: Size.fromRadius(AppDimens.iconSettingSize),
              child: FittedBox(
                child: Icon(
                  Icons.settings,
                  color: AppColors.backgroundBlueDark,
                ),
              ),
            ),
          )*/
        ],
      ),
    );
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: ExStatChartPage(
        onTouchDotListener: _onTouchDotListener,
        type: widget.type,
      ),
    );
  }

  Widget _buildFooter({String? text}) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Text(
          text ?? "",
          style: AppTextStyle.blueDarkS12Bold,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  List<LineBarSpot> _lineBarSpots = [];

  void _onTouchDotListener({List<LineBarSpot>? lineBarSpots}) {
    try {
      setState(() {
        /// quanth loaị bỏ các màu trùng nhau
        Map<Color, int> colorMap = HashMap<Color, int>();
        List<LineBarSpot> newlineBarSpots = [];

        for (int i = 0; i < (lineBarSpots?.length ?? 0); i++) {
          Color barColor = lineBarSpots?[i].bar.colors[0] ?? Colors.red;
          colorMap[barColor] = i;
        }

        colorMap.values.forEach((index) {
          newlineBarSpots.add(lineBarSpots![index]);
        });

        _lineBarSpots.clear();
        _lineBarSpots.addAll(newlineBarSpots);
      });
    } catch (e, s) {
      print(s);
    }
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: Color(0xffBB5A25),
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Text(
                      "Tổng chi đầu tư phát triển",
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.blueDarkS12Bold,
                      maxLines: 2,
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(width: 50,),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    color: Color(0xff809F44),
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Text(
                      "Tổng chi thường xuyên",
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.blueDarkS12Bold,
                      maxLines: 2,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void createPieChartPresents({ImexDataEntity? imex}) {
    _pieChartPresenters = [];

    List<ImexValueEntity>? values =
        imex?.thuchiData?[widget.type.value].value ?? [];
    ImexValueEntity all = values[0];
    ImexValueEntity num1 = values[1];
    ImexValueEntity num2 = values[2];

    double totalAll = 0;
    double totalNum1 = 0;
    double totalNum2 = 0;

    for (int i = 0; i < (all.value?.length ?? 0); i++) {
      totalAll += (all.value?[i] ?? 0);
    }

    for (int i = 0; i < (num1.value?.length ?? 0); i++) {
      totalNum1 += (num1.value?[i] ?? 0);
    }

    for (int i = 0; i < (num2.value?.length ?? 0); i++) {
      totalNum2 += (num2.value?[i] ?? 0);
    }

    PieChartPresenter num1Present = PieChartPresenter(
        key: 0,
        name:
            "${double.parse((totalNum1 / totalAll * 100).toStringAsFixed(2))} %",
        value: double.parse((totalNum1 / totalAll * 100).toStringAsFixed(2)));

    PieChartPresenter num2Present = PieChartPresenter(
        key: 0,
        name:
            "${100 - double.parse((totalNum1 / totalAll * 100).toStringAsFixed(2))} %",
        value: 100 -
            double.parse((totalNum1 / totalAll * 100).toStringAsFixed(2)));

    _pieChartPresenters.add(num1Present);
    _pieChartPresenters.add(num2Present);
  }
}
