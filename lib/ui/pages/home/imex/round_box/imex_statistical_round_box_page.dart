import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/import_export/imex_statistical_chart/imex_statistical_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../imex_cubit.dart';

class ImExStatisticalRoundBoxPage extends StatefulWidget { /// quanth
  ImexDataEntity? imex;
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  ImExStatisticalRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
    this.imex,
  });

  @override
  _ImExStatisticalRoundBoxPageState createState() =>
      _ImExStatisticalRoundBoxPageState();
}

class _ImExStatisticalRoundBoxPageState
    extends State<ImExStatisticalRoundBoxPage> {
  late ImexCubit _cubit;
  List<ChartPresenter> _imChartPresenters = [];
  List<ChartPresenter> _exChartPresenters = [];
  List<ChartPresenter> _balChartPresenters = [];
  List<String> _timeline = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ImexCubit>(context);
    createChartPresents(imex: widget.imex);
    _cubit.filterStatisticalChart(
        imChartPresenter: _imChartPresenters[0],
        exChartPresenter: _exChartPresenters[0],
        balChartPresenter: _balChartPresenters[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 550,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<ImexCubit, ImexState>(
            buildWhen: (prev, current) =>
                prev.statisticalType != current.statisticalType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          BlocBuilder<ImexCubit, ImexState>(
            buildWhen: (prev, current) =>
                prev.statLBStatus != current.statLBStatus,
            builder: (context, state) {
              return _buildMidSmallHeader(barTouchedSpot: state.statLBSpots);
            },
          ),
          _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(text: widget.bottomMidText),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
                _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH,
          ),
        ],
      ),
    );
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader({List<BarTouchedSpot?>? barTouchedSpot}) {
    if (barTouchedSpot == null ||
        barTouchedSpot.isEmpty ||
        barTouchedSpot.contains(null))
      return Container(
        height: 50,
      );
    else {
      String time = "";
      String arr = _timeline[(barTouchedSpot[0]!.touchedBarGroup.x).toInt()];
      String year = arr.split("-")[1];
      String month = arr.split("-")[0];
      time = "Tháng $month Năm $year";

      return Center(
        child: Container(
          height: 50,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              (barTouchedSpot != null)
                  ? Text(
                      time,
                      style: AppTextStyle.blueDarkS12,
                    )
                  : Container(),
              Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: _buildMidSmallItem(barTouchedSpot: barTouchedSpot[0]),
              ),
            ],
          ),
        ),
      );
    }
  }

  Widget _buildDescriptionHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Row(
        children: [
          Text(
            "(đơn vị: tỷ VND)",
            style: AppTextStyle.greyS9,
            textAlign: TextAlign.start,
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _buildMidSmallItem({BarTouchedSpot? barTouchedSpot}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 20,
          height: 10,
          color: barTouchedSpot?.touchedRodData.colors[0],
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          barTouchedSpot?.touchedRodData.y.toString() ?? "",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: ImexStatChartPage(
        onTouchDotListener: _onTouchDotListener,
      ),
    );
  }

  Widget _buildFooter({String? text}) {
    return Center(
      child: Container(
        margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
        child: Text(
          text ?? "",
          style: AppTextStyle.blueDarkS12Bold,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  BarTouchedSpot? _barTouchedSpot;

  void _onTouchDotListener({BarTouchedSpot? barTouchedSpot}) {
    try {
      _cubit.updateStatLBSpots(statLBSpots: [barTouchedSpot]);
    } catch (e, s) {
      print(s);
    }
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.imexBlueDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Tổng thu NS",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.imexRedDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Tổng chi NS",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.imexGreenDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Cân đối NS",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void createChartPresents({ImexDataEntity? imex}) {
    _timeline = imex?.timeline ?? [];
    _imChartPresenters = [];
    List<ImexValueEntity>? imValue = imex?.thuchiData?[0].value ?? [];
    for (int i = 0; i < (imValue.length); i++) {
      ImexValueEntity imItem = imValue[i];
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: imItem.value,
        name: imItem.catName,
      );
      _imChartPresenters.add(chartPresenter);
    }
    _exChartPresenters = [];
    List<ImexValueEntity>? exValue = imex?.thuchiData?[1].value ?? [];
    for (int i = 0; i < (exValue.length); i++) {
      ImexValueEntity exItem = exValue[i];
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: exItem.value,
        name: exItem.catName,
      );
      _exChartPresenters.add(chartPresenter);
    }
    _balChartPresenters = [];
    for (int i = 0; i < (imValue.length); i++) {
      List<double>? balVals = [];
      for (int j = 0; j < (imValue[i].value?.length ?? 0); j++) {
        double val =
            (imValue[i].value?[j] ?? 0.0) - (exValue[i].value?[j] ?? 0.0);
        balVals.add(val);
      }
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: balVals,
        name: "Tổng thu chi",
      );
      _balChartPresenters.add(chartPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType:
          _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateStatisticalType(statisticalType: type);
  }
}
