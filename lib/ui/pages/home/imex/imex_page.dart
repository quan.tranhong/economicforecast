import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/imex_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/appbar/shadow_banner.dart';
import 'package:flutter_base/ui/components/dialog/app_dialog.dart';
import 'package:flutter_base/ui/pages/home/cpi/loading/cpi_loading_page.dart';
import 'package:flutter_base/ui/pages/home/imex/imex_cubit.dart';
import 'package:flutter_base/ui/pages/home/imex/round_box/ex_statistical_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/imex/round_box/im_statistical_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/imex/round_box/imex_statistical_round_box_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ImexPage extends StatefulWidget {
  Function menuClick;
  ImexPage({required this.menuClick});

  @override
  _ImexPageState createState() => _ImexPageState();
}

class _ImexPageState extends State<ImexPage> {
  late ImexCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ImexCubit>(context);
    _cubit.fetchImex();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppColors.backgroundBlueLight,
        body: BlocBuilder<ImexCubit, ImexState>(
            buildWhen: (prev, current) =>
            prev.fetchImexStatus != current.fetchImexStatus,
            builder: (context, state) {
              if (state.fetchImexStatus == LoadStatus.LOADING) {
                return _buildLoadingList();
              } else if (state.fetchImexStatus == LoadStatus.FAILURE) {
                return _buildFailureList();
              }
              return _buildBodyWidget();
            }),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          backgroundColor: AppColors.backgroundBlueDark,
          expandedHeight: 200,
          floating: true,
          pinned: true,
          snap: false,
          stretch: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.menu),
            tooltip: 'Add new entry',
            onPressed: () {
              widget.menuClick();
            },
          ),
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            stretchModes: [StretchMode.zoomBackground],
            title: Text(
              S.of(context).imex_title,
              style: AppTextStyle.whiteS15,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
            background: ShadowBanner(),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              /*Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Text(
                  S.of(context).imex_title,
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),*/
              ImExStatisticalRoundBoxPage(
                topRightLargeText: "Thống kê thu chi ngân sách tỉnh",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText: "Thu chi ngân sách của tỉnh Đồng Nai năm 2017",
                imex: _cubit.state.imexEntity?.data,
              ),
              ImStatisticalRoundBoxPage(
                topRightLargeText: "Thống kê thu ngân sách tỉnh",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText: "Thu ngân sách của tỉnh Đồng Nai năm 2017",
                imex: _cubit.state.imexEntity?.data,
                type: ImexType.IMPORT,
              ),
              ExStatisticalRoundBoxPage(
                topRightLargeText: "Thống kê chi ngân sách tỉnh",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText: "Chi ngân sách của tỉnh Đồng Nai năm 2017",
                imex: _cubit.state.imexEntity?.data,
                type: ImexType.EXPORT,
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildLoadingList() {
    return SafeArea(child: CPILoadingPage());
  }

  Widget _buildFailureList() {
    return SafeArea(child: Container());
  }

  Future<bool> _onBackPressed() async{
    AppDialog(
      context: context,
      title: 'Thoát khỏi ứng dụng!',
      description: 'Ấn đồng ý để thoát khỏi ứng dụng.',
      cancelText: 'Huỷ',
      okText: 'Đồng ý',
      onOkPressed: () {
        SystemNavigator.pop();
      },
      onCancelPressed: () {
        // do nothing
      },
    ).show();
    return Future<bool>.value(true);
  }
}