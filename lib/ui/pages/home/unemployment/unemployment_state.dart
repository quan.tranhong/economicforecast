part of 'unemployment_cubit.dart';

@immutable
class UnemploymentState extends Equatable {
  final UnemEntity? unemEntity;
  final LoadStatus? fetchUnemStatus;

  /// Line - City
  final LoadStatus? lineCityFilterStatus;
  GrotripChartFilterParam? lineCityFilterParam;
  ForecastDurationType? lineCityType;

  /// Line - Country
  final LoadStatus? lineCounFilterStatus;
  GrotripChartFilterParam? lineCounFilterParam;
  ForecastDurationType? lineCounType;

  /// Bar - City
  final LoadStatus? barCityFilterStatus;
  GrotripChartFilterParam? barCityFilterParam;
  ForecastDurationType? barCityType;
  final LoadStatus? statBCiLBStatus;
  List<BarTouchedSpot?>? statBCiLBSpots;

  /// Bar - Country
  final LoadStatus? barCounFilterStatus;
  GrotripChartFilterParam? barCounFilterParam;
  ForecastDurationType? barCounType;
  final LoadStatus? statBCoLBStatus;
  List<BarTouchedSpot?>? statBCoLBSpots;

  UnemploymentState(
      {this.unemEntity,
      this.fetchUnemStatus,
      this.lineCityFilterStatus,
      this.lineCityFilterParam,
      this.lineCityType,
      this.lineCounFilterStatus,
      this.lineCounFilterParam,
      this.lineCounType,
      this.barCityFilterStatus,
      this.barCityFilterParam,
      this.barCityType,
      this.statBCiLBStatus,
      this.statBCiLBSpots,
      this.barCounFilterStatus,
      this.barCounFilterParam,
      this.barCounType,
      this.statBCoLBStatus,
      this.statBCoLBSpots});

  UnemploymentState copyWith({
    UnemEntity? unemEntity,
    LoadStatus? fetchUnemStatus,
    LoadStatus? lineCityFilterStatus,
    GrotripChartFilterParam? lineCityFilterParam,
    ForecastDurationType? lineCityType,
    LoadStatus? lineCounFilterStatus,
    GrotripChartFilterParam? lineCounFilterParam,
    ForecastDurationType? lineCounType,
    LoadStatus? barCityFilterStatus,
    GrotripChartFilterParam? barCityFilterParam,
    ForecastDurationType? barCityType,
    LoadStatus? statBCiLBStatus,
    List<BarTouchedSpot?>? statBCiLBSpots,
    LoadStatus? barCounFilterStatus,
    GrotripChartFilterParam? barCounFilterParam,
    ForecastDurationType? barCounType,
    LoadStatus? statBCoLBStatus,
    List<BarTouchedSpot?>? statBCoLBSpots,
  }) {
    return new UnemploymentState(
      unemEntity: unemEntity ?? this.unemEntity,
      fetchUnemStatus: fetchUnemStatus ?? this.fetchUnemStatus,
      lineCityFilterStatus: lineCityFilterStatus ?? this.lineCityFilterStatus,
      lineCityFilterParam: lineCityFilterParam ?? this.lineCityFilterParam,
      lineCityType: lineCityType ?? this.lineCityType,
      lineCounFilterStatus: lineCounFilterStatus ?? this.lineCounFilterStatus,
      lineCounFilterParam: lineCounFilterParam ?? this.lineCounFilterParam,
      lineCounType: lineCounType ?? this.lineCounType,
      barCityFilterStatus: barCityFilterStatus ?? this.barCityFilterStatus,
      barCityFilterParam: barCityFilterParam ?? this.barCityFilterParam,
      barCityType: barCityType ?? this.barCityType,
      statBCiLBStatus: statBCiLBStatus ?? this.statBCiLBStatus,
      statBCiLBSpots: statBCiLBSpots ?? this.statBCiLBSpots,
      barCounFilterStatus: barCounFilterStatus ?? this.barCounFilterStatus,
      barCounFilterParam: barCounFilterParam ?? this.barCounFilterParam,
      barCounType: barCounType ?? this.barCounType,
      statBCoLBStatus: statBCoLBStatus ?? this.statBCoLBStatus,
      statBCoLBSpots: statBCoLBSpots ?? this.statBCoLBSpots,
    );
  }

  List<int>? get cityTimeLines {
    List<int> oldValue = unemEntity?.data?.timeline ?? [];
    if (oldValue.isNotEmpty) {
      switch (barCityType) {
        case ForecastDurationType.SIX_MONTH:
          return unemEntity?.data?.timeline?.sublist(
              oldValue.length <= 6 ? 0 : (oldValue.length - 6),
              oldValue.length);
        case ForecastDurationType.A_YEAR:
          return unemEntity?.data?.timeline?.sublist(
              oldValue.length <= 12 ? 0 : (oldValue.length - 12),
              oldValue.length);
        default:
          return unemEntity?.data?.timeline?.sublist(
              oldValue.length <= 6 ? 0 : (oldValue.length - 6),
              oldValue.length);
      }
    }
    return [];
  }

  List<int>? get counTimeLines {
    List<int> oldValue = unemEntity?.data?.timeline ?? [];
    if (oldValue.isNotEmpty) {
      switch (barCounType) {
        case ForecastDurationType.SIX_MONTH:
          return unemEntity?.data?.timeline?.sublist(
              oldValue.length <= 6 ? 0 : (oldValue.length - 6),
              oldValue.length);
        case ForecastDurationType.A_YEAR:
          return unemEntity?.data?.timeline?.sublist(
              oldValue.length <= 12 ? 0 : (oldValue.length - 12),
              oldValue.length);
        default:
          return unemEntity?.data?.timeline?.sublist(
              oldValue.length <= 6 ? 0 : (oldValue.length - 6),
              oldValue.length);
      }
    }
    return [];
  }

  @override
  List<Object?> get props => [
        this.unemEntity,
        this.fetchUnemStatus,
        this.lineCityFilterStatus,
        this.lineCityFilterParam,
        this.lineCounFilterStatus,
        this.lineCounFilterParam,
        this.barCityFilterStatus,
        this.barCityFilterParam,
        this.barCounFilterStatus,
        this.barCounFilterParam,
        this.lineCityType,
        this.lineCounType,
        this.barCityType,
        this.barCounType,
        this.statBCiLBStatus,
        this.statBCiLBSpots,
        this.statBCoLBStatus,
        this.statBCoLBSpots
      ];
}
