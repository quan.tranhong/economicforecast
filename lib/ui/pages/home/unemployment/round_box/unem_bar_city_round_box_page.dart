import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/chart_group_reg_presenter.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/unem_type.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/unem/statistical/unem_statistical_bottom_sheet_cubit.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/unem/statistical/unem_statistical_bottom_sheet_page.dart';
import 'package:flutter_base/ui/pages/chart/unemployment/unemployment_statistical_chart/unem_bar_city_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../unemployment_cubit.dart';
import '../unemployment_cubit.dart';

class UnemBarCityRoundBoxPage extends StatefulWidget {
  UnemDataEntity? unem;
  UnemType type;
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  UnemBarCityRoundBoxPage({
    required this.type,
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
    this.unem,
  });

  @override
  _UnemBarCityRoundBoxPageState createState() =>
      _UnemBarCityRoundBoxPageState();
}

class _UnemBarCityRoundBoxPageState extends State<UnemBarCityRoundBoxPage> {
  late UnemploymentCubit _cubit;
  List<ChartGroupRegPresenter> _chartGroupPresenters = [];
  List<int> _timeline = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<UnemploymentCubit>(context);
    createChartPresents(unem: widget.unem!);
    _cubit.initBarChartPresenter(
        chartGroupPresenter:
            _chartGroupPresenters[widget.type.value].presenters?[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<UnemploymentCubit, UnemploymentState>(
            buildWhen: (prev, current) =>
                prev.barCityType != current.barCityType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          BlocBuilder<UnemploymentCubit, UnemploymentState>(
            buildWhen: (prev, current) =>
            prev.statBCiLBStatus != current.statBCiLBStatus,
            builder: (context, state) {
              return _buildMidSmallHeader(barTouchedSpot: state.statBCiLBSpots);
            },
          ),
          _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
                _cubit.state.barCityType ?? ForecastDurationType.SIX_MONTH,
          ),
          Container(
            width: 10,
          ),
          CircleButton(
            icon: Icon(
              Icons.settings,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _settingClickedListener,
          ),
        ],
      ),
    );
  }

  void _onSettingClickListener() {
    // if(item.priorityId.compareTo("")!=0){
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return UnemStatBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
    // }
  }

  Widget _buildBottomSheet() {
    return UnemStatisBottomSheetPage(
      unem: widget.unem,
      type: widget.type,
      selectItemListener: _selectItemListener,
      chartGroupPresenter: _cubit.state.barCityFilterParam?.chartPresenter,
    );
  }

  void _selectItemListener({ChartGroupPresenter? chartGroupPresenter}) {
    _cubit.filterBarChart(chartGroupPresenter: chartGroupPresenter);
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader({List<BarTouchedSpot?>? barTouchedSpot}) {
    if (barTouchedSpot == null ||
        barTouchedSpot.isEmpty ||
        barTouchedSpot.contains(null))
      return Container(
        height: 50,
      );
    List<BarChartRodStackItem> rodStackItems =
        barTouchedSpot[0]!.touchedRodData.rodStackItems;
    if (rodStackItems.isEmpty)
      return Container(
        height: 50,
      );
    List<Widget> list = [];

    double a = rodStackItems[0].toY - rodStackItems[0].fromY;
    double b = rodStackItems[1].toY - rodStackItems[1].fromY;
    double c = rodStackItems[2].toY - rodStackItems[2].fromY;
    for (int i = 0; i < rodStackItems.length; i++) {
      switch (i) {
        case 0:
          list.add(_buildMidSmallItem(
              color: rodStackItems[i].color,
              value: double.parse(a.toStringAsFixed(2))));
          break;
        case 1:
          list.add(_buildMidSmallItem(
              color: rodStackItems[i].color,
              value: double.parse(b.toStringAsFixed(2))));
          break;
        case 2:
          list.add(_buildMidSmallItem(
              color: rodStackItems[i].color,
              value: double.parse(c.toStringAsFixed(2))));
          break;
      }
    }

    String time =
        "Năm ${_timeline[(barTouchedSpot[0]!.touchedBarGroup.x)].toString()}";

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (rodStackItems.isNotEmpty)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: list,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Text(
            "(tỷ lệ %)",
            style: AppTextStyle.greyS12,
            textAlign: TextAlign.start,
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _buildMidSmallItem({Color? color, double? value}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 20,
          height: 10,
          color: color ?? Colors.red,
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          value.toString(),
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: UnemBarCityChartPage(
        type: widget.type,
        onTouchDotListener: _onTouchDotListener,
      ),
    );
  }

  Widget _buildFooter() {
    return BlocBuilder<UnemploymentCubit, UnemploymentState>(
        buildWhen: (prev, current) =>
            prev.barCityFilterParam != current.barCityFilterParam,
        builder: (context, state) {
          if (state.barCityFilterParam == LoadStatus.LOADING) {
            return Container();
          } else if (state.barCityFilterParam == LoadStatus.FAILURE) {
            return Container();
          }
          String text =
              "Tỷ lệ thất nghiệp từ 15 tuổi trở lên tại tỉnh Đồng Nai";
          String name = state.barCityFilterParam?.chartPresenter?.name ?? "";

          switch (name) {
            case "Tong so":
              text = "Tỷ lệ thất nghiệp từ 15 tuổi trở lên tại tỉnh Đồng Nai";
              break;
            case "Thanh thi":
              text =
                  "Tỷ lệ thất nghiệp khu vực Thành thị từ 15 tuổi trở lên tại tỉnh Đồng Nai";
              break;
            case "Nong thon":
              text =
                  "Tỷ lệ thất nghiệp khu vực Nông thôn từ 15 tuổi trở lên tại tỉnh Đồng Nai";
              break;
          }
          return Center(
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: AppDimens.statusMarginHorizontal,
                  vertical: AppDimens.statusBarHeight),
              child: Text(
                text,
                style: AppTextStyle.blueDarkS12Bold,
                textAlign: TextAlign.center,
              ),
            ),
          );
        });
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.unemBlueDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Chung",
                    style: AppTextStyle.blueDarkS12Bold,
                    maxLines: 2,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.unemRedDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Nam", style: AppTextStyle.blueDarkS12Bold, maxLines: 2)
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.unemGreenDark,
                    width: 20,
                    height: 10,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text("Nữ", style: AppTextStyle.blueDarkS12Bold, maxLines: 2)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  BarTouchedSpot? _barTouchedSpot;

  void _onTouchDotListener({
    BarTouchedSpot? barTouchedSpot,
  }) {
    try {
      _cubit.updateStatBCiLBSpots(statLBSpots: [barTouchedSpot]);
    } catch (e, s) {
      print(s);
    }
  }

  void createChartPresents({UnemDataEntity? unem}) {
    _timeline = [];
    _chartGroupPresenters = [];
    _timeline.addAll(unem?.timeline ?? []);
    List<UnemDataItemEntity> dataItemEntities = unem?.unemployment ?? [];
    for (int i = 0; i < dataItemEntities.length; i++) {
      List<ChartGroupPresenter> chatGroupPresenters = [];
      UnemDataItemEntity dataItem = dataItemEntities[i];
      String ageName = dataItemEntities[i].indexName ?? "";
      List<UnemValueEntity> valueEntities = dataItem.value ?? [];
      for (int j = 0; j < valueEntities.length; j++) {
        UnemValueEntity valueItem = valueEntities[j];
        String regionName = valueItem.region ?? "";
        UnemValueItemEntity valueItemEntities =
            valueItem.value ?? UnemValueItemEntity();
        ChartPresenter allPresenter = ChartPresenter(
          key: 0,
          value: valueItemEntities.chung,
          name: "chung",
        );
        ChartPresenter femalePresenter = ChartPresenter(
          key: 1,
          value: valueItemEntities.nam,
          name: "nam",
        );
        ChartPresenter malePresenter = ChartPresenter(
          key: 2,
          value: valueItemEntities.nu,
          name: "nu",
        );
        ChartGroupPresenter groupPresenter = ChartGroupPresenter(
          key: j,
          name: regionName,
          presenters: [allPresenter, malePresenter, femalePresenter],
        );
        chatGroupPresenters.add(groupPresenter);
      }
      ChartGroupRegPresenter chartGroupRegPresenter = ChartGroupRegPresenter(
        key: i,
        name: ageName,
        presenters: chatGroupPresenters,
      );
      _chartGroupPresenters.add(chartGroupRegPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType: _cubit.state.barCityType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateBarCityType(barCityType: type);
  }

  void _settingClickedListener() {
    _onSettingClickListener();
  }
}
