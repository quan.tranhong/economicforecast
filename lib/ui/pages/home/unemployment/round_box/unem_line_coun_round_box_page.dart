import 'dart:collection';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/chart_group_reg_presenter.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/unem_type.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/unem/statistical/unem_statistical_bottom_sheet_cubit.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/unem/statistical/unem_statistical_bottom_sheet_page.dart';
import 'package:flutter_base/ui/pages/chart/unemployment/unemployment_statistical_chart/unem_line_coun_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../unemployment_cubit.dart';

class UnemLineCounRoundBoxPage extends StatefulWidget {
  UnemDataEntity? unem;
  UnemType type;
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  UnemLineCounRoundBoxPage({
    required this.type,
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
    this.unem,
  });

  @override
  _UnemLineCounRoundBoxPageState createState() =>
      _UnemLineCounRoundBoxPageState();
}

class _UnemLineCounRoundBoxPageState
    extends State<UnemLineCounRoundBoxPage> {
  late UnemploymentCubit _cubit;
  List<ChartGroupRegPresenter> _chartGroupPresenters = [];
  List<int> _timeline = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<UnemploymentCubit>(context);
    createChartPresents(unem: widget.unem);
    _cubit.initStatChartPresenter2(
        chartGroupPresenter: _chartGroupPresenters[widget.type.value].presenters?[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<UnemploymentCubit, UnemploymentState>(
            buildWhen: (prev, current) =>
            prev.lineCounType != current.lineCounType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          _buildMidSmallHeader(lineBarSpots: _lineBarSpots),
          _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
            _cubit.state.lineCounType ?? ForecastDurationType.SIX_MONTH,
          ),
          Container(
            width: 10,
          ),
          CircleButton(
            icon: Icon(
              Icons.settings,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _settingClickedListener,
          ),
        ],
      ),
    );
  }

  void _onSettingClickListener() {
    // if(item.priorityId.compareTo("")!=0){
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return UnemStatBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
    // }
  }

  Widget _buildBottomSheet() {
    return UnemStatisBottomSheetPage(
      unem: widget.unem,
      type: widget.type,
      selectItemListener: _selectItemListener,
      chartGroupPresenter: _cubit.state.lineCounFilterParam?.chartPresenter,
    );
  }

  void _selectItemListener({ChartGroupPresenter? chartGroupPresenter}) {
    _cubit.filterStatisticalChart2(chartGroupPresenter: chartGroupPresenter);
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader({List<LineBarSpot>? lineBarSpots}) {
    if (lineBarSpots?.isEmpty ?? false)
      return Container(
        height: 50,
      );
    List<Widget> list = [];
    for (int i = 0; i < (lineBarSpots?.length ?? 0); i++) {
      list.add(_buildMidSmallItem(lineBarSpot: lineBarSpots![i]));
    }

    String time = "";
    if (lineBarSpots?.isNotEmpty ?? false) {
      time = "Năm ${(_timeline[(lineBarSpots?[0].x ?? 1 - 1).toInt()]).toString()}";
    }

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (lineBarSpots?.isNotEmpty ?? false)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Container(
              margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: list,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Text(
            "(tỷ lệ %)",
            style: AppTextStyle.greyS12,
            textAlign: TextAlign.start,
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _buildMidSmallItem({LineBarSpot? lineBarSpot}) {
    return Row(
      children: [
        Container(
          width: 20,
          height: 10,
          color: lineBarSpot?.bar.colors[0],
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          lineBarSpot?.y?.toString() ?? "",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: UnemLineCounChartPage(
        unem: widget.unem,
        onTouchDotListener: _onTouchDotListener,
        type: widget.type,
      ),
    );
  }

  Widget _buildFooter() {
    return BlocBuilder<UnemploymentCubit, UnemploymentState>(
        buildWhen: (prev, current) =>
        prev.lineCounFilterParam != current.lineCounFilterParam,
        builder: (context, state) {
          if (state.lineCounFilterParam == LoadStatus.LOADING) {
            return Container();
          } else if (state.lineCounFilterParam == LoadStatus.FAILURE) {
            return Container();
          }
          String text = "Tỷ lệ thất nghiệp độ tuổi lao động tại tỉnh Đồng Nai";
          String name = state.lineCounFilterParam?.chartPresenter?.name ?? "";

          switch(name){
            case "Tong so":
              text = "Tỷ lệ thất nghiệp độ tuổi lao động tại tỉnh Đồng Nai";
              break;
            case "Thanh thi":
              text = "Tỷ lệ thất nghiệp khu vực Thành thị độ tuổi lao động tại tỉnh Đồng Nai";
              break;
            case "Nong thon":
              text = "Tỷ lệ thất nghiệp khu vực Nông thôn độ tuổi lao động tại tỉnh Đồng Nai";
              break;
          }
          return Center(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: AppDimens.statusMarginHorizontal, vertical: AppDimens.statusBarHeight),
              child: Text(
                text,
                style: AppTextStyle.blueDarkS12Bold,
                textAlign: TextAlign.center,
              ),
            ),
          );
        });
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.unemBlueDark,
                    width: 30,
                    height: 2,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Chung",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.unemRedDark,
                    width: 30,
                    height: 2,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Nam",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: AppColors.unemGreenDark,
                    width: 30,
                    height: 2,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Nữ",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<LineBarSpot> _lineBarSpots = [];

  void _onTouchDotListener({List<LineBarSpot>? lineBarSpots}) {
    try {
      setState(() {
        /// quanth loaị bỏ các màu trùng nhau
        Map<Color, int> colorMap = HashMap<Color, int>();
        List<LineBarSpot> newlineBarSpots = [];

        for (int i = 0; i < (lineBarSpots?.length ?? 0); i++) {
          Color barColor = lineBarSpots?[i].bar.colors[0] ?? Colors.red;
          colorMap[barColor] = i;
        }

        colorMap.values.forEach((index) {
          newlineBarSpots.add(lineBarSpots![index]);
        });

        _lineBarSpots.clear();
        _lineBarSpots.addAll(newlineBarSpots);
      });
    } catch (e, s) {
      print(s);
    }
  }

  void createChartPresents({UnemDataEntity? unem}) {
    _timeline = [];
    _chartGroupPresenters = [];
    _timeline.addAll(unem?.timeline ?? []);
    List<UnemDataItemEntity> dataItemEntities = unem?.unemployment ?? [];
    for (int i = 0; i < dataItemEntities.length; i++) {
      List<ChartGroupPresenter> chatGroupPresenters = [];
      UnemDataItemEntity dataItem = dataItemEntities[i];
      String ageName = dataItemEntities[i].indexName ?? "";
      List<UnemValueEntity> valueEntities = dataItem.value ?? [];
      for (int j = 0; j < valueEntities.length; j++) {
        UnemValueEntity valueItem = valueEntities[j];
        String regionName = valueItem.region ?? "";
        UnemValueItemEntity valueItemEntities =
            valueItem.value ?? UnemValueItemEntity();
        ChartPresenter allPresenter = ChartPresenter(
          key: 0,
          value: valueItemEntities.chung,
          name: "chung",
        );
        ChartPresenter femalePresenter = ChartPresenter(
          key: 1,
          value: valueItemEntities.nam,
          name: "nam",
        );
        ChartPresenter malePresenter = ChartPresenter(
          key: 2,
          value: valueItemEntities.nam,
          name: "nu",
        );
        ChartGroupPresenter groupPresenter = ChartGroupPresenter(
          key: j,
          name: regionName,
          presenters: [allPresenter, malePresenter, femalePresenter],
        );
        chatGroupPresenters.add(groupPresenter);
      }
      ChartGroupRegPresenter chartGroupRegPresenter = ChartGroupRegPresenter(
        key: i,
        name: ageName,
        presenters: chatGroupPresenters,
      );
      _chartGroupPresenters.add(chartGroupRegPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType:
      _cubit.state.lineCounType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateLineCounType(lineCounType: type);
  }

  void _settingClickedListener() {
    _onSettingClickListener();
  }
}
