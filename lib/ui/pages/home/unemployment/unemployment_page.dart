import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/unem_type.dart';
import 'package:flutter_base/ui/components/appbar/shadow_banner.dart';
import 'package:flutter_base/ui/components/dialog/app_dialog.dart';
import 'package:flutter_base/ui/pages/home/cpi/loading/cpi_loading_page.dart';
import 'package:flutter_base/ui/pages/home/unemployment/round_box/unem_bar_city_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/unemployment/round_box/unem_bar_coun_round_box_page.dart';
import 'package:flutter_base/ui/pages/home/unemployment/unemployment_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UnemploymentPage extends StatefulWidget {
  Function menuClick;
  UnemploymentPage({required this.menuClick});

  @override
  _UnemploymentPageState createState() => _UnemploymentPageState();
}

class _UnemploymentPageState extends State<UnemploymentPage> {
  late UnemploymentCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<UnemploymentCubit>(context);
    _cubit.fetchUnem();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppColors.backgroundBlueLight,
        body: BlocBuilder<UnemploymentCubit, UnemploymentState>(
            buildWhen: (prev, current) =>
            prev.fetchUnemStatus != current.fetchUnemStatus,
            builder: (context, state) {
              if (state.fetchUnemStatus == LoadStatus.LOADING) {
                return _buildLoadingList();
              } else if (state.fetchUnemStatus == LoadStatus.FAILURE) {
                return _buildFailureList();
              }
              return _buildBodyWidget();
            }),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          backgroundColor: AppColors.backgroundBlueDark,
          expandedHeight: 200,
          floating: true,
          pinned: true,
          snap: false,
          stretch: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.menu),
            tooltip: 'Add new entry',
            onPressed: () {
              widget.menuClick();
            },
          ),
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            stretchModes: [StretchMode.zoomBackground],
            title: Text(
              S.of(context).unemployment_title,
              style: AppTextStyle.whiteS15,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
            background: ShadowBanner(),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              /*Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Text(
                  S.of(context).unemployment_title,
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),*/
              /*UnemLineCityRoundBoxPage(
                topRightLargeText: "Thống kê tỷ lệ TN từ 15 tuổi trở lên",
                topRightSmallText: S.of(context).unemployment_title,
                bottomMidText: "Tỷ lệ thất nghiệp của tỉnh Đồng Nai năm 2017",
                unem: _cubit.state.unemEntity?.data,
                type: UnemType.UNDER_15,
              ),
              UnemLineCounRoundBoxPage(
                topRightLargeText: "Thống kê tỷ lệ TN trong độ tuổi lao động",
                topRightSmallText: S.of(context).unemployment_title,
                bottomMidText: "Tỷ lệ thất nghiệp của tỉnh Đồng Nai năm 2017",
                unem: _cubit.state.unemEntity?.data,
                type: UnemType.OVER_18,
              ),*/
              UnemBarCityRoundBoxPage(
                topRightLargeText: "Thống kê tỷ lệ thất nghiệp từ 15 tuổi trở lên",
                topRightSmallText: S.of(context).unemployment_title,
                bottomMidText: "Tỷ lệ TN ở Thành thị của tỉnh Đồng Nai theo độ tuổi năm 2017",
                unem: _cubit.state.unemEntity?.data,
                type: UnemType.UNDER_15,
              ),
              UnemBarCounRoundBoxPage(
                topRightLargeText: "Thống kê tỷ lệ TN trong độ tuổi lao động",
                topRightSmallText: S.of(context).unemployment_title,
                bottomMidText: "Tỷ lệ thất nghiệp ở Thành thị của tỉnh Đồng Nai theo độ tuổi năm 2017",
                unem: _cubit.state.unemEntity?.data,
                type: UnemType.OVER_18,
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildLoadingList() {
    return SafeArea(child: CPILoadingPage());
  }

  Widget _buildFailureList() {
    return SafeArea(child: Container());
  }

  Future<bool> _onBackPressed() async{
    AppDialog(
      context: context,
      title: 'Thoát khỏi ứng dụng!',
      description: 'Ấn đồng ý để thoát khỏi ứng dụng.',
      cancelText: 'Huỷ',
      okText: 'Đồng ý',
      onOkPressed: () {
        SystemNavigator.pop();
      },
      onCancelPressed: () {
        // do nothing
      },
    ).show();
    return Future<bool>.value(true);
  }
}