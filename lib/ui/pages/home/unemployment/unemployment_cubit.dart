import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/models/entities/chart_group_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/grotrip_chart_filter_param.dart';
import 'package:flutter_base/repositories/chart_repository.dart';
import 'package:flutter_base/utils/logger.dart';
import 'package:meta/meta.dart';

part 'unemployment_state.dart';

class UnemploymentCubit extends Cubit<UnemploymentState> {
  ChartRepository? repository;

  UnemploymentCubit({this.repository}) : super(UnemploymentState());

  void fetchUnem() async {
    emit(state.copyWith(fetchUnemStatus: LoadStatus.LOADING));
    try {
      final unemEntity = await repository?.fetchUnem();
      emit(state.copyWith(
          fetchUnemStatus: LoadStatus.SUCCESS, unemEntity: unemEntity));
    } catch (e) {
      emit(state.copyWith(fetchUnemStatus: LoadStatus.FAILURE));
      logger.e(e);
    }
  }

  /// quanth: statistical
  void filterStatisticalChart(
      {ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(lineCityFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          lineCityFilterStatus: LoadStatus.SUCCESS,
          lineCityFilterParam:
              GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(lineCityFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initLineCityPresenter({ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(lineCityFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          lineCityFilterStatus: LoadStatus.SUCCESS,
          lineCityFilterParam:
              GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(lineCityFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: statistical
  void filterStatisticalChart2(
      {ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(lineCounFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          lineCounFilterStatus: LoadStatus.SUCCESS,
          lineCounFilterParam:
              GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(lineCounFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initStatChartPresenter2(
      {ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(lineCounFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          lineCounFilterStatus: LoadStatus.SUCCESS,
          lineCounFilterParam:
              GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(lineCounFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initBarChartPresenter({ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(barCityFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          barCityFilterStatus: LoadStatus.SUCCESS,
          barCityFilterParam:
              GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(barCityFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: barchart
  void filterBarChart({ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(
        barCityFilterStatus: LoadStatus.LOADING,
        statBCiLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
            barCityFilterStatus: LoadStatus.SUCCESS,
            barCityFilterParam:
                GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
            statBCiLBSpots: [],
            statBCiLBStatus: LoadStatus.SUCCESS),
      );
    } catch (error) {
      emit(state.copyWith(barCityFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initBarChartPresenter2(
      {ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(barCounFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          barCounFilterStatus: LoadStatus.SUCCESS,
          barCounFilterParam:
              GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(barCounFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: barchart
  void filterBarChart2({ChartGroupPresenter? chartGroupPresenter}) async {
    emit(state.copyWith(barCounFilterStatus: LoadStatus.LOADING,
        statBCoLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          barCounFilterStatus: LoadStatus.SUCCESS,
          barCounFilterParam:
              GrotripChartFilterParam(chartPresenter: chartGroupPresenter),
            statBCoLBSpots: [],
            statBCoLBStatus: LoadStatus.SUCCESS
        ),
      );
    } catch (error) {
      emit(state.copyWith(barCounFilterStatus: LoadStatus.FAILURE));
    }
  }

  void updateLineCityType({ForecastDurationType? lineCityType}) async {
    emit(
      state.copyWith(
        lineCityType: lineCityType,
      ),
    );
  }

  void updateLineCounType({ForecastDurationType? lineCounType}) async {
    emit(
      state.copyWith(
        lineCounType: lineCounType,
      ),
    );
  }

  void updateBarCityType({ForecastDurationType? barCityType}) async {
    emit(
      state.copyWith(
        barCityType: barCityType,
      ),
    );
  }

  void updateBarCounType({ForecastDurationType? barCounType}) async {
    emit(
      state.copyWith(
        barCounType: barCounType,
      ),
    );
  }

  void updateStatBCiLBSpots({List<BarTouchedSpot?>? statLBSpots}) async {
    emit(state.copyWith(statBCiLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statBCiLBSpots: statLBSpots,
          statBCiLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(statBCiLBStatus: LoadStatus.FAILURE));
    }
  }

  void updateStatBCoLBSpots({List<BarTouchedSpot?>? statLBSpots}) async {
    emit(state.copyWith(statBCoLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statBCoLBSpots: statLBSpots,
          statBCoLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(statBCoLBStatus: LoadStatus.FAILURE));
    }
  }
}
