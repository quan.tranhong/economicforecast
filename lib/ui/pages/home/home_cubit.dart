import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/enums/page_type.dart';
import 'package:flutter_base/repositories/auth_repository.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  AuthRepository? repository;

  HomeCubit({this.repository}) : super(HomeState());

  void changePage(PageType type) {
    emit(state.copyWith(page: type));
  }

  void signOut() async {
    emit(state.copyWith(signOutStatus: LoadStatus.LOADING));
    await repository?.removeToken();
    await Future.delayed(Duration(seconds: 2));
    emit(state.copyWith(signOutStatus: LoadStatus.SUCCESS));
  }
}
