import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/circlebutton/circle_button.dart';
import 'package:flutter_base/ui/pages/chart/iip/iip_statistical_chart/iip_statistical_chart_page.dart';
import 'package:flutter_base/ui/pages/dialog/month_calendar_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../chart/bottom_sheet/iip/statistical/iip_statistical_bottom_sheet_cubit.dart';
import '../../../chart/bottom_sheet/iip/statistical/iip_statistical_bottom_sheet_page.dart';
import '../iip_cubit.dart';

class IIPForcastStatRoundBoxPage extends StatefulWidget {
  String? topRightLargeText = "";
  String? topRightSmallText = "";

  IIPForcastStatRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
  });

  @override
  _IIPForcastStatRoundBoxPageState createState() =>
      _IIPForcastStatRoundBoxPageState();
}

class _IIPForcastStatRoundBoxPageState
    extends State<IIPForcastStatRoundBoxPage> {
  late IipCubit _cubit;
  List<ChartPresenter> _chartPresenters = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<IipCubit>(context);
    createChartPresents(iips: _cubit.state.statisticalIips ?? []);
    _cubit.filterStatisticalChart(chartPresenter: _chartPresenters[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 550,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          BlocBuilder<IipCubit, IipState>(
            buildWhen: (prev, current) =>
                prev.statisticalType != current.statisticalType,
            builder: (context, state) {
              return _buildLargeHeader(text: widget.topRightLargeText);
            },
          ),
          // _buildSmallHeader(text: widget.topRightSmallText),
          BlocBuilder<IipCubit, IipState>(
            buildWhen: (prev, current) =>
                prev.statLBStatus != current.statLBStatus,
            builder: (context, state) {
              return _buildMidSmallHeader(barTouchedSpot: state.statLBSpots);
            },
          ),
          Expanded(child: _buildChart()),
          _buildFooter(),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          CircleButton(
            icon: Icon(
              Icons.calendar_today_rounded,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _calendarClickedListener,
            forecastType:
                _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH,
          ),
          Container(
            width: 10,
          ),
          CircleButton(
            icon: Icon(
              Icons.settings,
              color: AppColors.backgroundBlueDark,
            ),
            onTapListenter: _settingClickedListener,
          ),
        ],
      ),
    );
  }

  Widget _buildMidSmallHeader({List<BarTouchedSpot?>? barTouchedSpot}) {
    if (barTouchedSpot == null ||
        barTouchedSpot.isEmpty ||
        barTouchedSpot.contains(null))
      return Container(
        height: 50,
      );

    String time = "";
    String arr =
        _cubit.state.statisticalTimeLines?[(barTouchedSpot[0]!.touchedBarGroup.x).toInt()] ?? "";
    String year = arr.split("-")[0];
    String month = arr.split("-")[1];
    time = "Tháng $month Năm $year";

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (barTouchedSpot != null)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Container(
              margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
              child: _buildMidSmallItem(barTouchedSpot: barTouchedSpot[0]),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMidSmallItem({BarTouchedSpot? barTouchedSpot}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 20,
          height: 10,
          color: barTouchedSpot?.touchedRodData.colors[0],
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          barTouchedSpot?.touchedRodData.y.toString() ?? "",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: BlocBuilder<IipCubit, IipState>(
          buildWhen: (prev, current) =>
              prev.statisticalFilterStatus != current.statisticalFilterStatus ||
              prev.statisticalType != current.statisticalType,
          builder: (context, state) {
            if (state.statisticalFilterStatus == LoadStatus.LOADING) {
              return Container();
            } else if (state.statisticalFilterStatus == LoadStatus.FAILURE) {
              return Container();
            }
            return IIPStatChartPage(
              onTouchDotListener: _onTouchDotListener,
              timeline: _cubit.state.statisticalTimeLines ?? [],
              statisticalType:
                  state.statisticalType ?? ForecastDurationType.SIX_MONTH,
            );
          }),
    );
  }

  Widget _buildFooter() {
    return BlocBuilder<IipCubit, IipState>(
        buildWhen: (prev, current) =>
            prev.statisticalFilterStatus != current.statisticalFilterStatus,
        builder: (context, state) {
          if (state.statisticalFilterStatus == LoadStatus.LOADING) {
            return Container();
          } else if (state.statisticalFilterStatus == LoadStatus.FAILURE) {
            return Container();
          }
          String text =
              state.statisticalFilterParams?.chartPresenter?.name ?? "";
          return Center(
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: AppDimens.statusMarginHorizontal,
                  vertical: AppDimens.statusBarHeight),
              child: Text(
                "$text ${S.of(context).bottom_title}",
                style: AppTextStyle.blueDarkS12Bold,
                textAlign: TextAlign.center,
              ),
            ),
          );
        });
  }

  BarTouchedSpot? _barTouchedSpot;

  void _onTouchDotListener({BarTouchedSpot? barTouchedSpot}) {
    try {
      _cubit.updateStatLBSpots(statLBSpots: [barTouchedSpot]);
    } catch (e, s) {
      print(s);
    }
  }

  void _onSettingClickListener() {
    // if(item.priorityId.compareTo("")!=0){
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return IIPStatBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
    // }
  }

  Widget _buildBottomSheet() {
    return IIPStatisBottomSheetPage(
      iips: _cubit.state.statisticalIips,
      selectItemListener: _selectItemListener,
      selectedPresenter: _cubit.state.statisticalFilterParams?.chartPresenter,
    );
  }

  void _selectItemListener({ChartPresenter? chartPresenter}) {
    _cubit.filterStatisticalChart(chartPresenter: chartPresenter);
  }

  void createChartPresents({List<IipDataItemEntity>? iips}) {
    _chartPresenters = [];
    for (int i = 0; i < (iips?.length ?? 0); i++) {
      ChartPresenter chartPresenter = ChartPresenter(
        key: i,
        value: iips?[i].value,
        name: iips?[i].name,
      );
      _chartPresenters.add(chartPresenter);
    }
  }

  void _calendarClickedListener() {
    MonthCalendarDialog(
      context: context,
      selectedType:
          _cubit.state.statisticalType ?? ForecastDurationType.SIX_MONTH,
      itemSelected: _itemSelected,
      canShowAll: false,
    ).show();
  }

  void _itemSelected({required ForecastDurationType type}) {
    _cubit.updateStatisticalType(statisticalType: type);
  }

  void _settingClickedListener() {
    _onSettingClickListener();
  }
}
