import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/line_chart_filter_param.dart';
import 'package:flutter_base/repositories/chart_repository.dart';
import 'package:flutter_base/utils/logger.dart';
import 'package:flutter_base/utils/utils.dart';

part 'iip_state.dart';

class IipCubit extends Cubit<IipState> {
  ChartRepository? repository;

  IipCubit({this.repository}) : super(IipState());

  void fetchIIP() async {
    emit(state.copyWith(fetchCPIStatus: LoadStatus.LOADING));
    try {
      final iipEntity = await repository?.fetchIIP();
      final iipForecastEntity = await repository?.fetchForecastIIP();
      emit(state.copyWith(
          fetchCPIStatus: LoadStatus.SUCCESS, iipEntity: iipEntity, iipForecastEntity: iipForecastEntity));
    } catch (e) {
      emit(state.copyWith(fetchCPIStatus: LoadStatus.FAILURE));
      logger.e(e);
    }
  }

  /// quanth: forecast
  void filterForecastChart({List<ChartPresenter>? chartPresenters}) async {
    emit(state.copyWith(
        forecastFilterStatus: LoadStatus.LOADING,
        forecastLBStatus: LoadStatus.LOADING));
    try {
      if (state.forecastType == null) {
        emit(
          state.copyWith(
            forecastFilterStatus: LoadStatus.SUCCESS,
            forecastFilterParams:
            LineChartFilterParam(chartPresenters: chartPresenters),
            forecastLBSpots: [],
            forecastLBStatus: LoadStatus.SUCCESS,
            forecastType: ForecastDurationType.SIX_MONTH,
          ),
        );
      } else {
        /// quanth: nếu đang mở chế độ dự báo thì sẽ reset về ban đầu
        ForecastDurationType forecastType = ForecastDurationType.SIX_MONTH;
        if (state.forecastType == ForecastDurationType.NINE_MONTH) {
          forecastType = ForecastDurationType.SIX_MONTH;
        } else if (state.forecastType == ForecastDurationType.OVER_A_YEAR) {
          forecastType = ForecastDurationType.A_YEAR;
        } else {
          forecastType = state.forecastType!;
        }
        emit(
          state.copyWith(
            forecastFilterStatus: LoadStatus.SUCCESS,
            forecastFilterParams:
            LineChartFilterParam(chartPresenters: chartPresenters),
            forecastLBSpots: [],
            forecastLBStatus: LoadStatus.SUCCESS,
            forecastType: forecastType,
          ),
        );
      }

    } catch (error) {
      emit(state.copyWith(forecastFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: statistical
  void filterStatisticalChart({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(
      statisticalFilterStatus: LoadStatus.LOADING,
      statLBStatus: LoadStatus.LOADING,
    ));
    try {
      emit(
        state.copyWith(
          statisticalFilterStatus: LoadStatus.SUCCESS,
          statisticalFilterParams:
              LineChartFilterParam(chartPresenter: chartPresenter),
          statLBSpots: [],
          statLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(statisticalFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: statistical
  void filterCompareChart({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(
        compareFilterStatus: LoadStatus.LOADING,
        compareLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          compareFilterStatus: LoadStatus.SUCCESS,
          compareFilterParams:
              LineChartFilterParam(chartPresenter: chartPresenter),
          compareLBSpots: [],
          compareLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(compareFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initForecastChartPresenter({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(forecastFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          forecastFilterStatus: LoadStatus.SUCCESS,
          forecastFilterParams:
              LineChartFilterParam(chartPresenters: [chartPresenter!]),
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initStatisticalChartPresenter({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(statisticalFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statisticalFilterStatus: LoadStatus.SUCCESS,
          statisticalFilterParams:
              LineChartFilterParam(chartPresenter: chartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(statisticalFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initCompareChartPresenter({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(compareFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          compareFilterStatus: LoadStatus.SUCCESS,
          compareFilterParams:
              LineChartFilterParam(chartPresenter: chartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(compareFilterStatus: LoadStatus.FAILURE));
    }
  }

  void updateForecastType({ForecastDurationType? forecastType, ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(
      forecastLBStatus: LoadStatus.LOADING,
    ));
    try {
      LineChartFilterParam? filterParam = LineChartFilterParam(
          chartPresenters: state.forecastFilterParams?.chartPresenters,
          chartPresenter: state.forecastFilterParams?.chartPresenter,
          choosens: (chartPresenter == null) ? [] : [chartPresenter]);

      emit(
        state.copyWith(
          forecastType: forecastType,
          forecastFilterParams: filterParam,
          forecastLBSpots: [],
          forecastLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastLBStatus: LoadStatus.FAILURE));
    }
  }

  void updateStatisticalType({ForecastDurationType? statisticalType}) async {
    emit(
      state.copyWith(
        statisticalType: statisticalType,
      ),
    );
  }

  void updateCompareType({ForecastDurationType? compareType}) async {
    emit(
      state.copyWith(
        compareType: compareType,
      ),
    );
  }

  void updateForecastLBSpots({List<LineBarSpot>? forecastLBSpots}) async {
    emit(state.copyWith(forecastLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          forecastLBSpots: forecastLBSpots,
          forecastLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(forecastLBStatus: LoadStatus.FAILURE));
    }
  }

  void updateStatLBSpots({List<BarTouchedSpot?>? statLBSpots}) async {
    emit(state.copyWith(statLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          statLBSpots: statLBSpots,
          statLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(statLBStatus: LoadStatus.FAILURE));
    }
  }

  void updateCompareLBSpots({List<LineBarSpot>? compareLBSpots}) async {
    emit(state.copyWith(compareLBStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          compareLBSpots: compareLBSpots,
          compareLBStatus: LoadStatus.SUCCESS,
        ),
      );
    } catch (error) {
      emit(state.copyWith(compareLBStatus: LoadStatus.FAILURE));
    }
  }
}
