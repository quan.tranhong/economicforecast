import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/components/appbar/shadow_banner.dart';
import 'package:flutter_base/ui/components/dialog/app_dialog.dart';
import 'package:flutter_base/ui/pages/home/cpi/loading/cpi_loading_page.dart';
import 'package:flutter_base/ui/pages/home/gdp/round_box/gdp_combine_round_box_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'gdp_cubit.dart';

class GDPPage extends StatefulWidget {
  Function menuClick;
  GDPPage({required this.menuClick});

  @override
  _GDPPageState createState() => _GDPPageState();
}

class _GDPPageState extends State<GDPPage> {
  late GdpCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<GdpCubit>(context);
    _cubit.fetchGDP();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: AppColors.backgroundBlueLight,
        body: BlocBuilder<GdpCubit, GdpState>(
            buildWhen: (prev, current) =>
                prev.fetchGDPStatus != current.fetchGDPStatus,
            builder: (context, state) {
              if (state.fetchGDPStatus == LoadStatus.LOADING) {
                return _buildLoadingList();
              } else if (state.fetchGDPStatus == LoadStatus.FAILURE) {
                return _buildFailureList();
              }
              return _buildBodyWidget();
            }),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: [
        SliverAppBar(
          backgroundColor: AppColors.backgroundBlueDark,
          expandedHeight: 200,
          floating: true,
          pinned: true,
          snap: false,
          stretch: true,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(Icons.menu),
            tooltip: 'Add new entry',
            onPressed: () {
              widget.menuClick();
            },
          ),
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: false,
            stretchModes: [StretchMode.zoomBackground],
            title: Text(
              S.of(context).gdp_title,
              style: AppTextStyle.whiteS15,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.end,
            ),
            background: ShadowBanner(),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              /*Container(
                margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
                child: Text(
                  S.of(context).gdp_title,
                  style: AppTextStyle.blueDarkS15Bold,
                ),
              ),*/
              GDPCombineRoundBoxPage(
                topRightLargeText: "Thống kê chỉ số GDP",
                topRightSmallText: S.of(context).cpi_title,
                bottomMidText: "Chỉ số GDP của tỉnh Đồng Nai các năm gần đây",
                gdp: _cubit.state.gdpEntity!.data!,
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildLoadingList() {
    return SafeArea(child: CPILoadingPage());
  }

  Widget _buildFailureList() {
    return SafeArea(child: Container());
  }

  Future<bool> _onBackPressed() async{
    AppDialog(
      context: context,
      title: 'Thoát khỏi ứng dụng!',
      description: 'Ấn đồng ý để thoát khỏi ứng dụng.',
      cancelText: 'Huỷ',
      okText: 'Đồng ý',
      onOkPressed: () {
        SystemNavigator.pop();
      },
      onCancelPressed: () {
        // do nothing
      },
    ).show();
    return Future<bool>.value(true);
  }
}
