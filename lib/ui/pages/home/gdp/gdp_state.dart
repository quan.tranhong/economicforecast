part of 'gdp_cubit.dart';

class GdpState extends Equatable {
  final GDPEntity? gdpEntity;
  final LoadStatus? fetchGDPStatus;

  /// quanth: compare
  final LoadStatus? compareFilterStatus;
  LineChartFilterParam? compareFilterParams;

  /// quanth: combine
  List<ItemSyncChartPresenter>? lineBarSpots = [];
  final LoadStatus? lineBarSpotsStatus;
  ForecastDurationType? combineType;

  GdpState({
    this.gdpEntity,
    this.fetchGDPStatus,
    this.compareFilterStatus,
    this.compareFilterParams,
    this.lineBarSpots,
    this.lineBarSpotsStatus,
    this.combineType,
  });

  GdpState copyWith({
    GDPEntity? gdpEntity,
    LoadStatus? fetchGDPStatus,
    LoadStatus? compareFilterStatus,
    LineChartFilterParam? compareFilterParams,
    List<ItemSyncChartPresenter>? lineBarSpots,
    LoadStatus? lineBarSpotsStatus,
    ForecastDurationType? combineType,
  }) {
    return new GdpState(
      gdpEntity: gdpEntity ?? this.gdpEntity,
      fetchGDPStatus: fetchGDPStatus ?? this.fetchGDPStatus,
      compareFilterStatus: compareFilterStatus ?? this.compareFilterStatus,
      compareFilterParams: compareFilterParams ?? this.compareFilterParams,
      lineBarSpots: lineBarSpots ?? this.lineBarSpots,
      lineBarSpotsStatus: lineBarSpotsStatus ?? this.lineBarSpotsStatus,
      combineType: combineType ?? this.combineType,
    );
  }

  @override
  List<Object> get props => [
        this.gdpEntity ?? GDPEntity(),
        this.fetchGDPStatus ?? LoadStatus.LOADING,
        this.compareFilterStatus ?? LoadStatus.LOADING,
        this.compareFilterParams ?? LineChartFilterParam(),
        this.lineBarSpotsStatus ?? LoadStatus.LOADING,
        this.lineBarSpots ?? [],
        this.combineType ?? ForecastDurationType.SIX_MONTH,
      ];
}
