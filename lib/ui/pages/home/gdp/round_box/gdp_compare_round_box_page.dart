import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/app_dimens.dart';
import 'package:flutter_base/commons/app_shadow.dart';
import 'package:flutter_base/commons/app_text_styles.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/entities/item_sync_chart_presenter.dart';
import 'package:flutter_base/models/entities/sync_chart_presenter.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/gdp/compare/gdp_compare_bottom_sheet_cubit.dart';
import 'package:flutter_base/ui/pages/chart/bottom_sheet/gdp/compare/gdp_compare_bottom_sheet_page.dart';
import 'package:flutter_base/ui/pages/chart/gdp/gdp_compare_chart/gdp_compare_chart_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../gdp_cubit.dart';

class GDPCompareRoundBoxPage extends StatefulWidget {
  GDPDataEntity? gdp;
  String? topRightLargeText = "";
  String? topRightSmallText = "";
  String? bottomMidText = "";

  GDPCompareRoundBoxPage({
    this.topRightLargeText,
    this.topRightSmallText,
    this.bottomMidText,
    this.gdp,
  });

  @override
  _GDPCompareRoundBoxPageState createState() => _GDPCompareRoundBoxPageState();
}

class _GDPCompareRoundBoxPageState extends State<GDPCompareRoundBoxPage> {
  late GdpCubit _cubit;
  List<ChartPresenter> _chartPresenters = [];
  List<int>? _timeline = [];

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<GdpCubit>(context);
    createChartPresents(gdps: widget.gdp);
    _cubit.initCompareChartPresenter(chartPresenter: _chartPresenters[0]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 550,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: AppShadow.boxShadow),
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildLargeHeader(text: widget.topRightLargeText),
          // _buildSmallHeader(text: widget.topRightSmallText),
          _buildMidSmallHeader(lineBarSpots: _lineBarSpots),
          _buildDescriptionHeader(),
          Expanded(child: _buildChart()),
          _buildDescription(),
          _buildFooter(text: widget.bottomMidText),
        ],
      ),
    );
  }

  Widget _buildLargeHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Row(
        children: [
          Expanded(
            child: Text(
              text ?? "",
              style: AppTextStyle.blueDarkS15Bold,
            ),
          ),
          GestureDetector(
            onTap: () {
              _onSettingClickListener();
            },
            child: SizedBox.fromSize(
              size: Size.fromRadius(AppDimens.iconSettingSize),
              child: FittedBox(
                child: Icon(
                  Icons.settings,
                  color: AppColors.backgroundBlueDark,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSmallHeader({String? text}) {
    return Container(
      margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
      child: Text(
        text ?? "",
        style: AppTextStyle.greyS9,
      ),
    );
  }

  Widget _buildMidSmallHeader({List<LineBarSpot>? lineBarSpots}) {
    if (lineBarSpots == null || lineBarSpots.isEmpty)
      return Container(
        height: 50,
      );
    List<Widget> list = [];
    for (int i = 0; i < lineBarSpots.length; i++) {
      list.add(_buildMidSmallItem(lineBarSpot: lineBarSpots[i]));
    }

    String? time = "";
    if (lineBarSpots.isNotEmpty) {
      time =
          "Năm ${widget.gdp?.year?[(lineBarSpots[0].x - 1).toInt()].toString()}";
    }

    return Center(
      child: Container(
        height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (lineBarSpots.isNotEmpty)
                ? Text(
                    time,
                    style: AppTextStyle.blueDarkS12,
                  )
                : Container(),
            Container(
              margin: EdgeInsets.all(AppDimens.statusMarginHorizontal),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: list,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildDescriptionHeader() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Text(
            "(tỷ lệ %)",
            style: AppTextStyle.greyS12,
            textAlign: TextAlign.start,
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget _buildMidSmallItem({LineBarSpot? lineBarSpot}) {
    return Row(
      children: [
        Container(
          width: 20,
          height: 10,
          color: lineBarSpot?.bar.colors[0],
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          lineBarSpot?.y.toString() ?? "",
          style: AppTextStyle.blueDarkS12Bold,
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget _buildChart() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimens.statusMarginHorizontal),
      child: BlocBuilder<GdpCubit, GdpState>(
          buildWhen: (prev, current) =>
              prev.compareFilterParams != current.compareFilterParams,
          builder: (context, state) {
            if (state.compareFilterStatus == LoadStatus.LOADING) {
              return Container();
            } else if (state.compareFilterStatus == LoadStatus.FAILURE) {
              return Container();
            }
            return GDPCompareChartPage(
                chartPresenter:
                    _cubit.state.compareFilterParams!.chartPresenter!,
                timeline: _timeline,
                onTouchDotListener: _onTouchDotListener);
          }),
    );
  }

  void _onTouchDotListener({List<LineBarSpot>? lineBarSpots}) {
    try {
      setState(() {
        _lineBarSpots.clear();
        _lineBarSpots.addAll(lineBarSpots!);
      });
    } catch (e, s) {
      print(s);
    }
  }

  Widget _buildFooter({String? text}) {
    return BlocBuilder<GdpCubit, GdpState>(
        buildWhen: (prev, current) =>
            prev.compareFilterStatus != current.compareFilterStatus,
        builder: (context, state) {
          if (state.compareFilterStatus == LoadStatus.LOADING) {
            return Container();
          } else if (state.compareFilterStatus == LoadStatus.FAILURE) {
            return Container();
          }
          String text = state.compareFilterParams?.chartPresenter?.name ?? "";
          return Center(
            child: Container(
              margin: EdgeInsets.symmetric(
                  horizontal: AppDimens.statusMarginHorizontal,
                  vertical: AppDimens.statusBarHeight),
              child: Text(
                "$text ${S.of(context).bottom_title_compare}",
                style: AppTextStyle.blueDarkS12Bold,
                textAlign: TextAlign.center,
              ),
            ),
          );
        });
  }

  List<LineBarSpot> _lineBarSpots = [];

  void _onSettingClickListener() {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext bc) {
          return SingleChildScrollView(
            child: Wrap(children: <Widget>[
              BlocProvider(
                create: (context) {
                  return GdpCompareBottomSheetCubit();
                },
                child: _buildBottomSheet(),
              ),
            ]),
          );
        });
  }

  Widget _buildBottomSheet() {
    return GDPCompareBottomSheetPage(
      gdps: widget.gdp,
      selectedPresenter: _cubit.state.compareFilterParams?.chartPresenter,
      selectItemListener: _selectItemListener,
    );
  }

  void _selectItemListener({
    ChartPresenter? chartPresenter,
  }) {
    _cubit.filterCompareChart(
      chartPresenter: chartPresenter,
    );
  }

  Widget _buildDescription() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: Colors.red,
                    width: 30,
                    height: 2,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "tỉnh Đồng Nai",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    color: Colors.green,
                    width: 30,
                    height: 2,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Cả nước",
                    style: AppTextStyle.blueDarkS12Bold,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void createChartPresents({GDPDataEntity? gdps}) {
    _timeline = [];
    _timeline?.addAll(gdps?.year ?? []);

    _chartPresenters = [];
    ChartPresenter chartStatPresenter = ChartPresenter(
      key: 0,
      value: gdps?.values,
      name: "Giá trị",
    );
    _chartPresenters.add(chartStatPresenter);

    ChartPresenter chartRatePresenter = ChartPresenter(
      key: 1,
      value: gdps?.rates,
      name: "tỷ lệ",
    );

    _chartPresenters.add(chartRatePresenter);
  }
}
