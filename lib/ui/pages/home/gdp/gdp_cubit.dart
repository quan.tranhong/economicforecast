import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/entities/item_sync_chart_presenter.dart';
import 'package:flutter_base/models/entities/sync_chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/models/params/line_chart_filter_param.dart';
import 'package:flutter_base/models/params/sync_chart_filter_param.dart';
import 'package:flutter_base/repositories/chart_repository.dart';
import 'package:flutter_base/utils/logger.dart';

part 'gdp_state.dart';

class GdpCubit extends Cubit<GdpState> {
  ChartRepository? repository;

  GdpCubit({this.repository}) : super(GdpState());

  void fetchGDP() async {
    emit(state.copyWith(fetchGDPStatus: LoadStatus.LOADING));
    try {
      final gdpEntity = await repository?.fetchGDP();
      emit(state.copyWith(
          fetchGDPStatus: LoadStatus.SUCCESS, gdpEntity: gdpEntity));
    } catch (e) {
      emit(state.copyWith(fetchGDPStatus: LoadStatus.FAILURE));
      logger.e(e);
    }
  }

  /// quanth: compare
  void filterCompareChart({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(compareFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          compareFilterStatus: LoadStatus.SUCCESS,
          compareFilterParams: LineChartFilterParam(chartPresenter: chartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(compareFilterStatus: LoadStatus.FAILURE));
    }
  }

  void initCompareChartPresenter({ChartPresenter? chartPresenter}) async {
    emit(state.copyWith(compareFilterStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          compareFilterStatus: LoadStatus.SUCCESS,
          compareFilterParams: LineChartFilterParam(chartPresenter: chartPresenter),
        ),
      );
    } catch (error) {
      emit(state.copyWith(compareFilterStatus: LoadStatus.FAILURE));
    }
  }

  /// quanth: compare
  void updateLineBarSpots({List<ItemSyncChartPresenter>? lineBarSpots}) async {
    emit(state.copyWith(lineBarSpotsStatus: LoadStatus.LOADING));
    try {
      emit(
        state.copyWith(
          lineBarSpotsStatus: LoadStatus.SUCCESS,
          lineBarSpots: lineBarSpots,
        ),
      );
    } catch (error) {
      emit(state.copyWith(lineBarSpotsStatus: LoadStatus.FAILURE));
    }
  }

  void updateCombineType({ForecastDurationType? combineType}) async {
    emit(
      state.copyWith(
        combineType: combineType,
      ),
    );
  }

}
