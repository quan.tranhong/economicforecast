import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:flutter_base/models/entities/user_entity.dart';
import 'package:flutter_base/models/enums/load_status.dart';
import 'package:flutter_base/repositories/auth_repository.dart';
import 'package:flutter_base/utils/logger.dart';
import 'package:rxdart/rxdart.dart';

part 'splash_state.dart';

enum SplashNavigator {
  OPEN_HOME,
  OPEN_SIGN_IN,
}

class SplashCubit extends Cubit<SplashState> {
  AuthRepository? repository;

  final messageController = PublishSubject<String>();
  final navigatorController = PublishSubject<SplashNavigator>();

  SplashCubit({this.repository}) : super(SplashState());

  void checkLogin() async {
    TokenEntity? token = await repository?.getToken();
    if (token == null) {
      logger.d('emit LOGGED_OUT');
      emit(state.copyWith(loginState: LoginState.LOGGED_OUT));
      navigatorController.sink.add(SplashNavigator.OPEN_SIGN_IN);
    } else {
      logger.d('emit LOGGED_IN');
      emit(state.copyWith(loginState: LoginState.LOGGED_IN));
      navigatorController.sink.add(SplashNavigator.OPEN_HOME);
    }
  }

  void saveToken(String token) async {
    try {
      final result = TokenEntity(token: token, refreshToken: 'app_refresh_token');
      await repository?.saveToken(result);
      navigatorController.sink.add(SplashNavigator.OPEN_HOME);
    } catch (error) {
      logger.e(error);
      navigatorController.sink.add(SplashNavigator.OPEN_SIGN_IN);
    }
  }

  void getUser() {}

  void getRemoteConfig() {}

  @override
  Future<void> close() {
    messageController.close();
    navigatorController.close();
    return super.close();
  }
}
