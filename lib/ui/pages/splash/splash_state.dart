part of 'splash_cubit.dart';

enum LoginState { INITIAL, LOGGED_IN, LOGGED_OUT }

class SplashState extends Equatable {
  final LoginState? loginState;
  final LoadStatus? loadUserStatus;
  final UserEntity? user;
  final LoadStatus? loadServerConfigStatus;
  final RemoteConfigEntity? remoteConfig;

  SplashState({
    this.loginState = LoginState.INITIAL,
    this.loadUserStatus = LoadStatus.INITIAL,
    this.user,
    this.loadServerConfigStatus = LoadStatus.INITIAL,
    this.remoteConfig,
  });

  SplashState copyWith({
    LoginState? loginState,
    LoadStatus? loadUserStatus,
    UserEntity? user,
    LoadStatus? loadServerConfigStatus,
    RemoteConfigEntity? remoteConfig,
  }) {
    if ((loginState == null || identical(loginState, this.loginState)) &&
        (loadUserStatus == null ||
            identical(loadUserStatus, this.loadUserStatus)) &&
        (user == null || identical(user, this.user)) &&
        (loadServerConfigStatus == null ||
            identical(loadServerConfigStatus, this.loadServerConfigStatus)) &&
        (remoteConfig == null || identical(remoteConfig, this.remoteConfig))) {
      return this;
    }

    return new SplashState(
      loginState: loginState ?? this.loginState,
      loadUserStatus: loadUserStatus ?? this.loadUserStatus,
      user: user ?? this.user,
      loadServerConfigStatus:
          loadServerConfigStatus ?? this.loadServerConfigStatus,
      remoteConfig: remoteConfig ?? this.remoteConfig,
    );
  }

  @override
  List<Object> get props => [
        this.loginState ?? LoadStatus.LOADING,
        this.loadUserStatus ?? LoadStatus.LOADING,
        this.user ?? UserEntity(),
        this.loadServerConfigStatus ?? LoadStatus.LOADING,
        this.remoteConfig ?? RemoteConfigEntity(),
      ];
}
