import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_colors.dart';
import 'package:flutter_base/commons/screen_size.dart';

class StatusBarWidget extends StatelessWidget {
  final Color color;

  StatusBarWidget({Key? key, this.color = AppColors.main}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenSize.of(context).topPadding,
      color: color ?? AppColors.main,
    );
  }
}
