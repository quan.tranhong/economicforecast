import 'package:dio/dio.dart';
import 'package:flutter_base/configs/app_config.dart';
import 'package:flutter_base/models/entities/index.dart';
import 'package:retrofit/retrofit.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: AppConfig.baseUrl)
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  ///Setting
  @GET("/settings")
  Future<void> getSetting();

  ///Auth

  ///Fetch CPI
  @GET("/cpies")
  Future<CPIEntity> fetchCPI(
    // @Path("n_months") int nMonths,
    @Query("reverse") bool reverse,
  );

  /*///Fetch CPI
  @GET("/cpies/{n_months}")
  Future<CPIEntity> fetchCPI(
      // @Path("n_months") int nMonths,
      @Query("reverse") bool reverse,
  );*/

  ///Fetch forecast CPI
  @GET("/cpis/forecast/{n_months}")
  Future<CPIForecastEntity> fetchForecastCPI(
    @Path("n_months") int nMonths,
    @Query("alpha") double alpha,
  );

  ///Fetch IIP
  @GET("/iips/{n_months}")
  Future<IipEntity> fetchIIP(
    @Path("n_months") int nMonths,
    @Query("reverse") bool reverse,
  );

  ///Fetch forecast IIP
  @GET("/iips/forecast/{n_months}")
  Future<IIPForecastEntity> fetchForecastIIP(
    @Path("n_months") int nMonths,
    @Query("alpha") double alpha,
  );

  ///Fetch GDP
  @GET("/gdps/{n_years}")
  Future<GDPEntity> fetchGDP(
    @Path("n_years") int nYears,
    @Query("reverse") bool reverse,
  );

  ///Fetch XNK
  @GET("/xnk/{n_months}")
  Future<XNKEntity> fetchXNK(
    @Path("n_months") int nMonths,
    @Query("reverse") bool reverse,
  );

  ///Fetch ImEx
  @GET("/thuchi/{n_years}")
  Future<ImexEntity> fetchImEx(
    @Path("n_years") int nYears,
    @Query("reverse") bool reverse,
  );

  ///Fetch unemployment
  @GET("/unemployment/{n_years}")
  Future<UnemEntity> fetchUnemployment(
    @Path("n_years") int nYears,
    @Query("reverse") bool reverse,
  );

  ///Fetch revenue
  @GET("/import")
  Future<RevenueEntity> fetchRevenue();

  ///Fetch forecast revenue
  @GET("/import/forecast/{n_years}")
  Future<ImForecastEntity> fetchForecastRevenue(
      @Path("n_years") int nYears,
      @Query("alpha") double alpha,
  );

  ///Fetch expenditure
  @GET("/export")
  Future<ExpenditureEntity> fetchExpenditure();

  ///Fetch forecast expenditure
  @GET("/export/forecast/{n_years}")
  Future<ExForecastEntity> fetchForecastExpenditure(
      @Path("n_years") int nYears,
      @Query("alpha") double alpha,
  );
}
