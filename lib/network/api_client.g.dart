// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_client.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ApiClient implements ApiClient {
  _ApiClient(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://aic-group.bike/api/v1/dong-nai';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<void> getSetting() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    await _dio.fetch<void>(_setStreamType<void>(
        Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
            .compose(_dio.options, '/settings',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    return null;
  }

  @override
  Future<CPIEntity> fetchCPI(reverse) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'reverse': reverse};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CPIEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/cpies',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CPIEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CPIForecastEntity> fetchForecastCPI(nMonths, alpha) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'alpha': alpha};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CPIForecastEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/cpis/forecast/$nMonths',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CPIForecastEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<IipEntity> fetchIIP(nMonths, reverse) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'reverse': reverse};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<IipEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/iips/$nMonths',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = IipEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<IIPForecastEntity> fetchForecastIIP(nMonths, alpha) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'alpha': alpha};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<IIPForecastEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/iips/forecast/$nMonths',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = IIPForecastEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<GDPEntity> fetchGDP(nYears, reverse) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'reverse': reverse};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GDPEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/gdps/$nYears',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GDPEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<XNKEntity> fetchXNK(nMonths, reverse) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'reverse': reverse};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<XNKEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/xnk/$nMonths',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = XNKEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ImexEntity> fetchImEx(nYears, reverse) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'reverse': reverse};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ImexEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/thuchi/$nYears',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ImexEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<UnemEntity> fetchUnemployment(nYears, reverse) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'reverse': reverse};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<UnemEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/unemployment/$nYears',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = UnemEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<RevenueEntity> fetchRevenue() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<RevenueEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/import',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = RevenueEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ImForecastEntity> fetchForecastRevenue(nYears, alpha) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'alpha': alpha};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ImForecastEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/import/forecast/$nYears',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ImForecastEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ExpenditureEntity> fetchExpenditure() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ExpenditureEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/export',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ExpenditureEntity.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ExForecastEntity> fetchForecastExpenditure(nYears, alpha) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'alpha': alpha};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ExForecastEntity>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/export/forecast/$nYears',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ExForecastEntity.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
