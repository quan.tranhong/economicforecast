enum UnemType {
  UNDER_15,
  OVER_18,
}

extension UnemTypeExtension on UnemType {
  int get value {
    switch(this){
      case UnemType.UNDER_15:
        return 0;
      case UnemType.OVER_18:
        return 1;
      default:
        return 0;
    }
  }
}