enum ImexType {
  IMPORT,
  EXPORT,
}

extension ImexTypeExtension on ImexType {
  int get value {
    switch(this){
      case ImexType.IMPORT:
        return 0;
      case ImexType.EXPORT:
        return 1;
      default:
        return 0;
    }
  }
}