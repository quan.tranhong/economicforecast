import 'package:json_annotation/json_annotation.dart';

part 'cpi_data_item_entity.g.dart';

@JsonSerializable()
class CpiDataItemEntity {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'val')
  List<double>? val;

  CpiDataItemEntity({this.name, this.val});

  factory CpiDataItemEntity.fromJson(Map<String, dynamic> json) => _$CpiDataItemEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CpiDataItemEntityToJson(this);
}



