// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'revenue_data_detail_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RevenueDataDetailEntity _$RevenueDataDetailEntityFromJson(
    Map<String, dynamic> json) {
  return RevenueDataDetailEntity(
    value: (json['value'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    name: json['name'] as String?,
  );
}

Map<String, dynamic> _$RevenueDataDetailEntityToJson(
        RevenueDataDetailEntity instance) =>
    <String, dynamic>{
      'value': instance.value,
      'name': instance.name,
    };
