// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'im_forecast_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImForecastEntity _$ImForecastEntityFromJson(Map<String, dynamic> json) {
  return ImForecastEntity(
    data: json['data'] == null
        ? null
        : ImForecastDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ImForecastEntityToJson(ImForecastEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
