// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cpi_forecast_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CPIForecastEntity _$CPIForecastEntityFromJson(Map<String, dynamic> json) {
  return CPIForecastEntity(
    data: json['data'] == null
        ? null
        : CPIForecastDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CPIForecastEntityToJson(CPIForecastEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
