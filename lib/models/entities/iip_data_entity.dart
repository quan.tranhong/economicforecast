import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'iip_data_entity.g.dart';

@JsonSerializable()
class IipDataEntity {
  @JsonKey(name: 'subs')
  List<IipDataItemEntity>? subs;
  @JsonKey(name: 'timeline')
  List<String>? timeline;
  @JsonKey(name: 'iip')
  List<double>? iip;

  IipDataEntity({this.subs, this.iip, this.timeline});

  factory IipDataEntity.fromJson(Map<String, dynamic> json) => _$IipDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$IipDataEntityToJson(this);
}