import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'unem_value_item_entity.g.dart';

@JsonSerializable()
class UnemValueItemEntity {
  @JsonKey(name: 'chung')
  List<double>? chung;
  @JsonKey(name: 'nam')
  List<double>? nam;
  @JsonKey(name: 'nu')
  List<double>? nu;

  UnemValueItemEntity({this.chung, this.nam, this.nu});

  factory UnemValueItemEntity.fromJson(Map<String, dynamic> json)  => _$UnemValueItemEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UnemValueItemEntityToJson(this);

}