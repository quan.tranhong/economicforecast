// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unem_value_item_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnemValueItemEntity _$UnemValueItemEntityFromJson(Map<String, dynamic> json) {
  return UnemValueItemEntity(
    chung: (json['chung'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    nam: (json['nam'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    nu: (json['nu'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
  );
}

Map<String, dynamic> _$UnemValueItemEntityToJson(
        UnemValueItemEntity instance) =>
    <String, dynamic>{
      'chung': instance.chung,
      'nam': instance.nam,
      'nu': instance.nu,
    };
