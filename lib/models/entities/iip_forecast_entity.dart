import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'iip_forecast_entity.g.dart';

@JsonSerializable()
class IIPForecastEntity {
  @JsonKey(name: 'data')
  IIPForecastDataEntity? data;

  IIPForecastEntity({this.data});

  factory IIPForecastEntity.fromJson(Map<String, dynamic> json) => _$IIPForecastEntityFromJson(json);

  Map<String, dynamic> toJson() => _$IIPForecastEntityToJson(this);
}