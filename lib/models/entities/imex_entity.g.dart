// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'imex_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImexEntity _$ImexEntityFromJson(Map<String, dynamic> json) {
  return ImexEntity(
    data: json['data'] == null
        ? null
        : ImexDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ImexEntityToJson(ImexEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
