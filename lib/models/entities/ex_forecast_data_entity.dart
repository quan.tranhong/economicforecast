import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'ex_forecast_data_entity.g.dart';

@JsonSerializable()
class ExForecastDataEntity {
  @JsonKey(name: 'export')
  List<double>? export;
  @JsonKey(name: 'timeline')
  List<String>? timeline;
  @JsonKey(name: 'lower')
  List<double>? lower;
  @JsonKey(name: 'upper')
  List<double>? upper;

  ExForecastDataEntity({this.export, this.timeline, this.lower, this.upper});

  factory ExForecastDataEntity.fromJson(Map<String, dynamic> json) => _$ExForecastDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ExForecastDataEntityToJson(this);
}