import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'im_forecast_entity.g.dart';

@JsonSerializable()
class ImForecastEntity {
  @JsonKey(name: 'data')
  ImForecastDataEntity? data;

  ImForecastEntity({this.data});

  factory ImForecastEntity.fromJson(Map<String, dynamic> json) => _$ImForecastEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ImForecastEntityToJson(this);
}