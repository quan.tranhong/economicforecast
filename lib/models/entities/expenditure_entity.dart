import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'expenditure_entity.g.dart';

@JsonSerializable()
class ExpenditureEntity {
  @JsonKey(name: 'data')
  ExpenditureDataEntity? data;

  ExpenditureEntity({this.data});

  factory ExpenditureEntity.fromJson(Map<String, dynamic> json) => _$ExpenditureEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ExpenditureEntityToJson(this);
}