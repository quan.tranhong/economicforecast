// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'imex_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImexDataEntity _$ImexDataEntityFromJson(Map<String, dynamic> json) {
  return ImexDataEntity(
    thuchiData: (json['thuchi_data'] as List<dynamic>?)
        ?.map((e) => ImexDataItemEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$ImexDataEntityToJson(ImexDataEntity instance) =>
    <String, dynamic>{
      'thuchi_data': instance.thuchiData,
      'timeline': instance.timeline,
    };
