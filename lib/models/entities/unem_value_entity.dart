import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'unem_value_entity.g.dart';

@JsonSerializable()
class UnemValueEntity {
  @JsonKey(name: 'region')
  String? region;
  @JsonKey(name: 'value')
  UnemValueItemEntity? value;

  UnemValueEntity({this.region, this.value});

  factory UnemValueEntity.fromJson(Map<String, dynamic> json) => _$UnemValueEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UnemValueEntityToJson(this);

}