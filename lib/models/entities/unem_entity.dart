import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'unem_entity.g.dart';

@JsonSerializable()
class UnemEntity {
  @JsonKey(name: 'data')
  UnemDataEntity? data;

  UnemEntity({this.data});

  factory UnemEntity.fromJson(Map<String, dynamic> json) => _$UnemEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UnemEntityToJson(this);
}