import 'package:json_annotation/json_annotation.dart';

part 'iip_data_item_entity.g.dart';

@JsonSerializable()
class IipDataItemEntity {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'value')
  List<double>? value;

  IipDataItemEntity({this.name, this.value});

  factory IipDataItemEntity.fromJson(Map<String, dynamic> json) => _$IipDataItemEntityFromJson(json);

  Map<String, dynamic> toJson() => _$IipDataItemEntityToJson(this);
}



