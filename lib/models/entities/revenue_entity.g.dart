// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'revenue_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RevenueEntity _$RevenueEntityFromJson(Map<String, dynamic> json) {
  return RevenueEntity(
    data: json['data'] == null
        ? null
        : RevenueDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$RevenueEntityToJson(RevenueEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
