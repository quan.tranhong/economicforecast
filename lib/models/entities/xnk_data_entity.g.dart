// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'xnk_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

XNKDataEntity _$XNKDataEntityFromJson(Map<String, dynamic> json) {
  return XNKDataEntity(
    nhapkhau: (json['nhapkhau'] as List<dynamic>?)
        ?.map((e) => XNKDataItemEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    xuatkhau: (json['xuatkhau'] as List<dynamic>?)
        ?.map((e) => XNKDataItemEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    years: (json['years'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$XNKDataEntityToJson(XNKDataEntity instance) =>
    <String, dynamic>{
      'nhapkhau': instance.nhapkhau,
      'xuatkhau': instance.xuatkhau,
      'years': instance.years,
    };
