

import 'package:equatable/equatable.dart';

class PieChartPresenter extends Equatable {
  double? value;
  int? key;
  String? name;

  PieChartPresenter({this.key, this.value, this.name});

  @override
  List<Object> get props => [this.key ?? 0, this.value ?? 0, this.name ?? ""];

}