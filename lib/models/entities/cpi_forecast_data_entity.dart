import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'cpi_forecast_data_entity.g.dart';

@JsonSerializable()
class CPIForecastDataEntity {
  @JsonKey(name: 'cpi')
  List<double>? cpi;
  @JsonKey(name: 'timeline')
  List<String>? timeline;
  @JsonKey(name: 'lower')
  List<double>? lower;
  @JsonKey(name: 'upper')
  List<double>? upper;

  CPIForecastDataEntity({this.cpi, this.timeline});

  factory CPIForecastDataEntity.fromJson(Map<String, dynamic> json) => _$CPIForecastDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$CPIForecastDataEntityToJson(this);
}