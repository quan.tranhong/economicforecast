// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gdp_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GDPEntity _$GDPEntityFromJson(Map<String, dynamic> json) {
  return GDPEntity(
    data: json['data'] == null
        ? null
        : GDPDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$GDPEntityToJson(GDPEntity instance) => <String, dynamic>{
      'data': instance.data,
    };
