import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'unem_data_entity.g.dart';

@JsonSerializable()
class UnemDataEntity {
  @JsonKey(name: 'timeline')
  List<int>? timeline;
  @JsonKey(name: 'unemployment')
  List<UnemDataItemEntity>? unemployment;

  UnemDataEntity({this.timeline, this.unemployment});

  factory UnemDataEntity.fromJson(Map<String, dynamic> json) => _$UnemDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UnemDataEntityToJson(this);

}