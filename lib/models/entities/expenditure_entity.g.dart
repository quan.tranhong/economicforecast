// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'expenditure_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExpenditureEntity _$ExpenditureEntityFromJson(Map<String, dynamic> json) {
  return ExpenditureEntity(
    data: json['data'] == null
        ? null
        : ExpenditureDataEntity.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ExpenditureEntityToJson(ExpenditureEntity instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
