// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cpi_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CPIDataEntity _$CPIDataEntityFromJson(Map<String, dynamic> json) {
  return CPIDataEntity(
    cpi: (json['cpi'] as List<dynamic>?)
        ?.map((e) => CpiDataItemEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$CPIDataEntityToJson(CPIDataEntity instance) =>
    <String, dynamic>{
      'cpi': instance.cpi,
      'timeline': instance.timeline,
    };
