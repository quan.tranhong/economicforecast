import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'expenditure_data_detail_entity.g.dart';

@JsonSerializable()
class ExpenditureDataDetailEntity {
  @JsonKey(name: 'value')
  List<double>? value;
  @JsonKey(name: 'name')
  String? name;

  ExpenditureDataDetailEntity({this.value, this.name});

  factory ExpenditureDataDetailEntity.fromJson(Map<String, dynamic> json) => _$ExpenditureDataDetailEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ExpenditureDataDetailEntityToJson(this);
}