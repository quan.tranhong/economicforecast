// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unem_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnemDataEntity _$UnemDataEntityFromJson(Map<String, dynamic> json) {
  return UnemDataEntity(
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as int).toList(),
    unemployment: (json['unemployment'] as List<dynamic>?)
        ?.map((e) => UnemDataItemEntity.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UnemDataEntityToJson(UnemDataEntity instance) =>
    <String, dynamic>{
      'timeline': instance.timeline,
      'unemployment': instance.unemployment,
    };
