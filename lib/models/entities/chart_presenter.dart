

import 'package:equatable/equatable.dart';

class ChartPresenter extends Equatable {
  List<double>? value;
  int? key;
  String? name;

  ChartPresenter({this.key, this.value, this.name});

  @override
  List<Object> get props => [this.key ?? 0, this.value ?? 0, this.name ?? ""];

}