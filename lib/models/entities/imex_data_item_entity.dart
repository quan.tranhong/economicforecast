import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'imex_data_item_entity.g.dart';

@JsonSerializable()
class ImexDataItemEntity {
  @JsonKey(name: 'index_name')
  String? indexName;
  @JsonKey(name: 'value')
  List<ImexValueEntity>? value;

  ImexDataItemEntity({this.indexName, this.value});

  factory ImexDataItemEntity.fromJson(Map<String, dynamic> json) => _$ImexDataItemEntityFromJson(json);

  Map<String, dynamic> toJson() => _$ImexDataItemEntityToJson(this);

}