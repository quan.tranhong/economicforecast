// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ex_forecast_data_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ExForecastDataEntity _$ExForecastDataEntityFromJson(Map<String, dynamic> json) {
  return ExForecastDataEntity(
    export: (json['export'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    timeline:
        (json['timeline'] as List<dynamic>?)?.map((e) => e as String).toList(),
    lower: (json['lower'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
    upper: (json['upper'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
  );
}

Map<String, dynamic> _$ExForecastDataEntityToJson(
        ExForecastDataEntity instance) =>
    <String, dynamic>{
      'export': instance.export,
      'timeline': instance.timeline,
      'lower': instance.lower,
      'upper': instance.upper,
    };
