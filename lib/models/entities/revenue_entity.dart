import 'package:json_annotation/json_annotation.dart';

import 'index.dart';

part 'revenue_entity.g.dart';

@JsonSerializable()
class RevenueEntity {
  @JsonKey(name: 'data')
  RevenueDataEntity? data;

  RevenueEntity({this.data});

  factory RevenueEntity.fromJson(Map<String, dynamic> json) => _$RevenueEntityFromJson(json);

  Map<String, dynamic> toJson() => _$RevenueEntityToJson(this);
}