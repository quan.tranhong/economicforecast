// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'imex_value_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImexValueEntity _$ImexValueEntityFromJson(Map<String, dynamic> json) {
  return ImexValueEntity(
    catName: json['cat_name'] as String?,
    value: (json['value'] as List<dynamic>?)
        ?.map((e) => (e as num).toDouble())
        .toList(),
  );
}

Map<String, dynamic> _$ImexValueEntityToJson(ImexValueEntity instance) =>
    <String, dynamic>{
      'cat_name': instance.catName,
      'value': instance.value,
    };
