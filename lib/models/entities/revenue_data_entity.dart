import 'package:json_annotation/json_annotation.dart';
import 'index.dart';

part 'revenue_data_entity.g.dart';

@JsonSerializable()
class RevenueDataEntity {
  @JsonKey(name: 'details')
  List<RevenueDataDetailEntity>? details;
  @JsonKey(name: 'domestic_import')
  RevenueDataDetailEntity? domesticImport;
  @JsonKey(name: 'foreign_import')
  RevenueDataDetailEntity? foreignImport;
  @JsonKey(name: 'total_import')
  RevenueDataDetailEntity? totalImport;
  @JsonKey(name: 'timeline')
  List<String>? timeline;

  RevenueDataEntity({this.details, this.domesticImport, this.foreignImport, this.totalImport, this.timeline});

  factory RevenueDataEntity.fromJson(Map<String, dynamic> json) => _$RevenueDataEntityFromJson(json);

  Map<String, dynamic> toJson() => _$RevenueDataEntityToJson(this);
}