// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unem_value_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnemValueEntity _$UnemValueEntityFromJson(Map<String, dynamic> json) {
  return UnemValueEntity(
    region: json['region'] as String?,
    value: json['value'] == null
        ? null
        : UnemValueItemEntity.fromJson(json['value'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UnemValueEntityToJson(UnemValueEntity instance) =>
    <String, dynamic>{
      'region': instance.region,
      'value': instance.value,
    };
