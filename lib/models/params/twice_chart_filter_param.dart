import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';

// ignore: must_be_immutable
class TwiceChartFilterParam extends Equatable {
  ChartPresenter? imChartPresenter;
  ChartPresenter? exChartPresenter;
  ChartPresenter? balChartPresenter;

  TwiceChartFilterParam({
    this.imChartPresenter,
    this.exChartPresenter,
    this.balChartPresenter,
  });

  TwiceChartFilterParam copyWith({
    ChartPresenter? imChartPresenter,
    ChartPresenter? exChartPresenter,
    ChartPresenter? balChartPresenter,
  }) {
    return new TwiceChartFilterParam(
      imChartPresenter: imChartPresenter ?? this.imChartPresenter,
      exChartPresenter: exChartPresenter ?? this.exChartPresenter,
      balChartPresenter: balChartPresenter ?? this.balChartPresenter,
    );
  }

  @override
  List<Object> get props => [
        imChartPresenter ?? [],
        exChartPresenter ?? [],
        balChartPresenter ?? [],
      ];
}
