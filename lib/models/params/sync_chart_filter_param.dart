import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/entities/item_sync_chart_presenter.dart';
import 'package:flutter_base/models/entities/sync_chart_presenter.dart';

// ignore: must_be_immutable
class SyncChartFilterParam extends Equatable {
  SyncChartPresenter? chartPresenter;

  SyncChartFilterParam({
    SyncChartPresenter? chartPresenter
  }){
    this.chartPresenter = chartPresenter;
  }

  SyncChartFilterParam copyWith({
    SyncChartPresenter? chartPresenter,
  }) {
    return new SyncChartFilterParam(
      chartPresenter: chartPresenter ?? this.chartPresenter,
    );
  }

  @override
  List<Object> get props => [chartPresenter ?? SyncChartPresenter()];
}