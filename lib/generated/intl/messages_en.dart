// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "bottom_title": MessageLookupByLibrary.simpleMessage(
            "of Dong Nai province in 2017"),
        "bottom_title_compare": MessageLookupByLibrary.simpleMessage(
            "of Dong Nai province compared to the whole country in 2017"),
        "cpi_title":
            MessageLookupByLibrary.simpleMessage("Consumer Price Index (CPI)"),
        "economic_forecast":
            MessageLookupByLibrary.simpleMessage("Economic Forecast"),
        "export_title": MessageLookupByLibrary.simpleMessage("Exports"),
        "gdp_title": MessageLookupByLibrary.simpleMessage(
            "Gross Domestic Product (GDP)"),
        "iip_title": MessageLookupByLibrary.simpleMessage(
            "Index of Industrial Production (IIP)"),
        "imex_title": MessageLookupByLibrary.simpleMessage(
            "Budget revenue and expenditure"),
        "import_title": MessageLookupByLibrary.simpleMessage("Imports"),
        "loading": MessageLookupByLibrary.simpleMessage("Loading data..."),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "movies": MessageLookupByLibrary.simpleMessage("Movies"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "register":
            MessageLookupByLibrary.simpleMessage("Sign up for an account"),
        "unemployment_title":
            MessageLookupByLibrary.simpleMessage("Unemployment rate"),
        "username": MessageLookupByLibrary.simpleMessage("Username")
      };
}
