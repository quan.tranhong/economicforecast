part of 'app_cubit.dart';

class AppState extends Equatable {
  final TokenEntity? token;
  final UserEntity? user;

  AppState({
    this.token,
    this.user,
  });

  AppState copyWith({
    TokenEntity? token,
    UserEntity? user,
  }) {
    if ((token == null || identical(token, this.token)) &&
        (user == null || identical(user, this.user))) {
      return this;
    }

    return new AppState(
      token: token ?? this.token,
      user: user ?? this.user,
    );
  }

  @override
  List<Object> get props => [
        token ?? "",
        user ?? "",
      ];
}
