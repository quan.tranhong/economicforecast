import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppShadow {
  static final boxShadow = [
    BoxShadow(
      color: AppColors.shadowColor,
      blurRadius: 3,
      offset: Offset(0, 0),
    ),
  ];
  static final boxDarkShadow = [
    BoxShadow(
      color: Colors.grey,
      blurRadius: 3,
      offset: Offset(0, 0),
    ),
  ];
  static final boxLightShadow = [
    BoxShadow(
      color: Colors.grey,
      blurRadius: 1,
      offset: Offset(0, 0),
    ),
  ];

  static final boxLightE8Shadow = [
    BoxShadow(
      color: AppColors.whiteLightE8,
      blurRadius: 2,
      offset: Offset(0, 0),
    ),
  ];
}
