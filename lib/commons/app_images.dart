import 'package:flutter/cupertino.dart';

class AppImages {
  static final icBack = 'assets/images/ic_back.png';
  static final icWhiteBack = 'assets/images/ic_white_back.png';
  static final icBackgroundApp = 'assets/images/ic_background_app.png';
  static final icSplashLogo = 'assets/images/ic_splash_logo.png';
  static final icBackgroundGradient = 'assets/images/ic_background_gradient.png';
  static final icCPIMenu = 'assets/images/ic_cpi_menu.png';
  static final icIIPMenu = 'assets/images/ic_iip_menu.png';
  static final icXNKMenu = 'assets/images/ic_xnk_menu.png';
  static final icGDPMenu = 'assets/images/ic_gdp_menu.png';
  static final icThatnghiepMenu = 'assets/images/ic_thatnghiep_menu.png';
  static final icThuchiMenu = 'assets/images/ic_thuchi_menu.png';
  static final icLogoutMenu = 'assets/images/ic_logout_menu.png';
  static final icUser = 'assets/images/ic_user.png';
  static final icLine = 'assets/images/ic_line.png';
  static final icDash = 'assets/images/ic_dash.png';
  static final icPaul = 'assets/images/ic_paul_circle.png';
  static final icAYear = 'assets/images/ic_a_year.png';
  static final icSixMonth = 'assets/images/ic_six_month.png';
  static final icDN = 'assets/images/ic_dn.png';
  static final icSuccess = 'assets/images/ic_success.png';
  static final icErrorNetwork = 'assets/images/ic_error_network.png';
  static final icLogoSSO = 'assets/images/ic_logo_sso.png';
  static final ic6Month = 'assets/images/ic_6_month.png';
  static final icAllMonth = 'assets/images/ic_all_month.png';
  static final icMapDN = 'assets/images/ic_map_dn.png';

}
