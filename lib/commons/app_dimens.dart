class AppDimens {
  AppDimens._(); // this basically makes it so you can instantiate this class
  //TextFontSize
  static const double fontSmaller = 11.0;
  static const double fontMaxSmall = 10.0;
  static const double fontSmall = 12.0;
  static const double fontNormal = 14.0;
  static const double fontLarge = 16.0;
  static const double fontExtra = 18.0;

  //Common
  static const double appBarHeight = 48.0;
  //Signin
  static const double formInnerHorizontalMargin = 16.0;
  static const double formOuterHorizontalMargin = 20.0;
  static const double formIconSize = 20.0;
  static const double formHeight = 48.0;
  static const double formSigninButtonSpace = 30.0;
  //CPI
  static const double statusBarHeight = 20.0;
  static const double statusMarginHorizontal = 10.0;
  //RoundBorder
  static const double iconSettingSize = 10.0;

}
