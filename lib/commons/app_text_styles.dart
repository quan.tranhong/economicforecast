import 'dart:ui';

import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppTextStyle {
  ///Black
  static final black = TextStyle(color: Colors.black);

  //s12
  static final blackS12 = black.copyWith(fontSize: 12);
  static final blackS12Bold = blackS12.copyWith(fontWeight: FontWeight.bold);
  static final blackS12W800 = blackS12.copyWith(fontWeight: FontWeight.w800);

  //s14
  static final blackS14 = black.copyWith(fontSize: 14);
  static final blackS14Bold = blackS14.copyWith(fontWeight: FontWeight.bold);
  static final blackS14W800 = blackS14.copyWith(fontWeight: FontWeight.w800);

  //s16
  static final blackS16 = black.copyWith(fontSize: 14);
  static final blackS16Bold = blackS16.copyWith(fontWeight: FontWeight.bold);
  static final blackS16W800 = blackS16.copyWith(fontWeight: FontWeight.w800);

  //s18
  static final blackS18 = black.copyWith(fontSize: 14);
  static final blackS18Bold = blackS18.copyWith(fontWeight: FontWeight.bold);
  static final blackS18W800 = blackS18.copyWith(fontWeight: FontWeight.w800);

  ///White
  static final white = TextStyle(color: Colors.white);

  //s12
  static final whiteS12 = white.copyWith(fontSize: 12);
  static final whiteS12Bold = whiteS12.copyWith(fontWeight: FontWeight.bold);
  static final whiteS12W800 = whiteS12.copyWith(fontWeight: FontWeight.w800);
  static final whiteS12Drawer = whiteS12.copyWith(fontWeight: FontWeight.w800);

  //s14
  static final whiteS14 = white.copyWith(fontSize: 14);
  static final whiteS14Bold = whiteS14.copyWith(fontWeight: FontWeight.bold);
  static final whiteS14W800 = whiteS14.copyWith(fontWeight: FontWeight.w800);

  //s16
  static final whiteS15 = white.copyWith(fontSize: 15);

  //s16
  static final whiteS16 = white.copyWith(fontSize: 16);
  static final whiteS16Bold = whiteS16.copyWith(fontWeight: FontWeight.bold);
  static final whiteS16W800 = whiteS16.copyWith(fontWeight: FontWeight.w800);

  //s18
  static final whiteS18 = white.copyWith(fontSize: 18);
  static final whiteS18Bold = whiteS18.copyWith(fontWeight: FontWeight.bold);
  static final whiteS18W800 = whiteS18.copyWith(fontWeight: FontWeight.w800);

  ///Gray
  static final grey = TextStyle(color: Colors.grey);

  //s9
  static final greyS9 = grey.copyWith(fontSize: 9);

  //s12
  static final greyS12 = grey.copyWith(fontSize: 12);
  static final greyS12Bold = greyS12.copyWith(fontWeight: FontWeight.bold);
  static final greyS12W800 = greyS12.copyWith(fontWeight: FontWeight.w800);

  //s14
  static final greyS14 = grey.copyWith(fontSize: 14);
  static final greyS14Bold = greyS14.copyWith(fontWeight: FontWeight.bold);
  static final greyS14W800 = greyS14.copyWith(fontWeight: FontWeight.w800);

  //s16
  static final greyS16 = grey.copyWith(fontSize: 16);
  static final greyS16Bold = greyS16.copyWith(fontWeight: FontWeight.bold);
  static final greyS16W800 = greyS16.copyWith(fontWeight: FontWeight.w800);

  //s18
  static final greyS18 = grey.copyWith(fontSize: 18);
  static final greyS18Bold = greyS18.copyWith(fontWeight: FontWeight.bold);
  static final greyS18W800 = greyS18.copyWith(fontWeight: FontWeight.w800);

  ///Tint
  static final tint = TextStyle(color: AppColors.main);

  //s12
  static final tintS12 = tint.copyWith(fontSize: 12);
  static final tintS12Bold = tintS12.copyWith(fontWeight: FontWeight.bold);
  static final tintS12W800 = tintS12.copyWith(fontWeight: FontWeight.w800);

  //s14
  static final tintS14 = tint.copyWith(fontSize: 14);
  static final tintS14Bold = tintS14.copyWith(fontWeight: FontWeight.bold);
  static final tintS14W800 = tintS14.copyWith(fontWeight: FontWeight.w800);

  //s16
  static final tintS16 = tint.copyWith(fontSize: 16);
  static final tintS16Bold = tintS16.copyWith(fontWeight: FontWeight.bold);
  static final tintS16W800 = tintS16.copyWith(fontWeight: FontWeight.w800);

  //s18
  static final tintS18 = tint.copyWith(fontSize: 18);
  static final tintS18Bold = tintS18.copyWith(fontWeight: FontWeight.bold);
  static final tintS18W800 = tintS18.copyWith(fontWeight: FontWeight.w800);

  ///BlueDark
  static final blueDark = TextStyle(color: AppColors.backgroundBlueDark);

  //s12
  static final blueDarkS9 = blueDark.copyWith(fontSize: 9);
  static final blueDarkS9Bold = blueDarkS9.copyWith(fontWeight: FontWeight.bold);
  static final blueDarkS9W800 = blueDarkS9.copyWith(fontWeight: FontWeight.w800);

  //s12
  static final blueDarkS12 = blueDark.copyWith(fontSize: 12);
  static final blueDarkS12Bold = blueDarkS12.copyWith(fontWeight: FontWeight.bold);
  static final blueDarkS12W800 = blueDarkS12.copyWith(fontWeight: FontWeight.w800);

  //s15
  static final blueDarkS15 = blueDark.copyWith(fontSize: 15);
  static final blueDarkS15Bold = blueDarkS15.copyWith(fontWeight: FontWeight.bold);
  static final blueDarkS15W800 = blueDarkS15.copyWith(fontWeight: FontWeight.w800);

  //s17
  static final blueDarkS17 = blueDark.copyWith(fontSize: 17);
  static final blueDarkS17Bold = blueDarkS17.copyWith(fontWeight: FontWeight.bold);
  static final blueDarkS17W800 = blueDarkS17.copyWith(fontWeight: FontWeight.w800);

  ///Red
  static final red = TextStyle(color: Colors.red);

  //s12
  static final redS9 = red.copyWith(fontSize: 9);
  //s12
  static final redS12 = red.copyWith(fontSize: 12);
  static final redS12Bold = redS12.copyWith(fontWeight: FontWeight.bold);
  static final redS12W800 = redS12.copyWith(fontWeight: FontWeight.w800);

  ///greenDark
  static final greenDark = TextStyle(color: AppColors.backgroundGreenDark);
  //s12
  static final greenDarkS12 = greenDark.copyWith(fontSize: 12);
  static final greenDarkS12Bold = greenDarkS12.copyWith(fontWeight: FontWeight.bold);
  static final greenDarkS12W800 = greenDarkS12.copyWith(fontWeight: FontWeight.w800);

}
