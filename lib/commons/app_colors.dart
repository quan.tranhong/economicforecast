import 'dart:ui';

class AppColors {
  ///Common
  static const Color main = Color(0xFF00C8A0);
  static const Color background = Color(0xFFFFFFFF);

  ///Gradient
  static const Color gradientEnd = Color(0xFF00C8A0);
  static const Color gradientStart = Color(0xFF00AFA5);

  ///Shadow
  static const Color shadowColor = Color(0x25606060);

  ///Border
  static const Color borderColor = Color(0xFF606060);

  ///Text
  static const Color textTint = Color(0xFF00C8A0);
  static const Color textDart = Color(0xFF000000);
  static const Color textGray = Color(0xFF979ca8);

  ///Button
  static const Color buttonGreen = Color(0xFF00FF00);
  static const Color buttonRed = Color(0xFFFF0000);

  ///Other
  static const Color lightGray = Color(0x1A606060);
  static const Color gray = Color(0xFF606060);

  ///Background Blue
  static const Color backgroundBlue = Color(0xFF1873B9);
  static const Color backgroundBlueLight = Color(0xFFE5F0F6);
  static const Color backgroundBlueDark = Color(0xFF004481);
  static const Color backgroundGreenDark = Color(0xFF127716);
  static const Color backgroundGray = Color(0xFFA1A1A1);
  static const Color backgroundPink = Color(0xFF42a5f5);

  /// gdp screen
  static const Color gdpBlueDark = Color(0xff498399);
  static const Color gdpOrangeDark = Color(0xffBB5A25);

  /// xnk screen
  static const Color xnkBlueDark = Color(0xff3B5487);
  static const Color xnkGreenDark = Color(0xff75A29E);
  static const Color xnkRedDark = Color(0xffB35752);

  /// unem screen
  static const Color unemBlueDark = Color(0xff3B5487);
  static const Color unemGreenDark = Color(0xff75A29E);
  static const Color unemRedDark = Color(0xffB35752);

  /// imex screen
  static const Color imexBlueDark = Color(0xff3B5487);
  static const Color imexGreenDark = Color(0xff75A29E);
  static const Color imexRedDark = Color(0xffB35752);

  /// light
  static const Color whiteLight = Color(0xffF8F8F8);
  static const Color whiteLightE8 = Color(0xffE8E8E8);

  /// forecast Green
  static const Color greenForecastHex = Color(0xffD0E4D0);
  static Color greenForecast = backgroundGreenDark.withOpacity(0.2);

  ///Chart color
  static const List<Color> chartColors = [
    Color(0xff5B7FB7),
    Color(0xffB35752),
    Color(0xffA1BA65),
    Color(0xff5CBCAA),
    Color(0xff7B659D),
    Color(0xff65AAC2),
    Color(0xffF39248),
    Color(0xff7A6282),
    Color(0xff809F44),
    Color(0xff3B5487),
    Color(0xffDA986E),
    Color(0xff415F8E),
    Color(0xff8A3D39),
    Color(0xffA0CE63),
    Color(0xff5C4B78),
    Color(0xff498399),
    Color(0xff75A29E),
    Color(0xffBB5A25),
    Color(0xffDE8E38),
    Color(0xffDEA964),
    Color(0xffD18581),
    Color(0xff9E3674),
    Color(0xff47467F),
    Color(0xffD9D64A),
  ];
}
