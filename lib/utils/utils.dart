import 'package:flutter_base/models/entities/chart_presenter.dart';
import 'package:flutter_base/models/enums/forecast_duration_type.dart';

class Utils {
  static double? checkDouble(dynamic value) {
    if(value is double) return value;
    if(value is int) return value.toDouble();
    if(value is String) return double.tryParse(value);
    return null;
  }

  static int? checkInt(dynamic value) {
    if(value is int) return value;
    if(value is double) {
      return (value == double.infinity) ? 2021 : value.toInt();
    }
    if(value is String) return int.tryParse(value);
    return null;
  }

  static bool isEnableForecastMode(ForecastDurationType? type) {
    return type == ForecastDurationType.NINE_MONTH ||
        type == ForecastDurationType.OVER_A_YEAR ||
        type == ForecastDurationType.OVER_ALL_YEAR;
  }

  static bool isEnableAllMode(ForecastDurationType? type) {
    return type == ForecastDurationType.ALL_YEAR ||
        type == ForecastDurationType.OVER_ALL_YEAR;
  }

  static bool isEnableAllModeExceptOver(ForecastDurationType? type) {
    return type == ForecastDurationType.ALL_YEAR;
  }

  static double createDivRange(int length) {
    if(length <= 15) return 1;
    int a = length % 6;
    return ((length - a) / 6);
  }
}
